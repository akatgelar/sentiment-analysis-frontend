import {Component, OnInit, ViewChild} from '@angular/core';

declare var $: any;
declare var Morris: any;
import 'd3';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import { HttpClient } from '@angular/common/http';
import swal from 'sweetalert2';

@Component({
  moduleId: module.id,
  selector: 'app-sample-tweets',
  templateUrl: './sample-tweets.component.html',
  styleUrls: [  ],
  animations: [fadeInOutTranslate]
})
export class SampleTweetsComponent implements OnInit {
  private _apiURLGetSample2019GantiPresiden = '/api/getSample2019GantiPresiden';
  private _apiURLGetSample2019TetapJokowi = '/api/getSample2019TetapJokowi';
  private _apiURLGetSampleJokowi = '/api/getSampleJokowi';
  private _apiURLGetSamplePrabowo = '/api/getSamplePrabowo';
  private _dataGetSample2019GantiPresiden: DataResult;
  private _dataGetSample2019TetapJokowi: DataResult;
  private _dataGetSampleJokowi: DataResult;
  private _dataGetSamplePrabowo: DataResult;
  private _dataLoaded: boolean;
  private rows1: any[] = [];
  private rows2: any[] = [];
  private rows3: any[] = [];
  private rows4: any[] = [];
  private collapsed1: true;
  private collapsed2: true;
  private collapsed3: true;
  private collapsed4: true;

  @ViewChild('myTable') table: any;

  expanded: any = {};
  timeout: any;


  constructor(private httpClient: HttpClient) {
    this.collapsed1 = true;
    this.collapsed2 = true;
    this.collapsed3 = true;
    this.collapsed4 = true;
  }

  ngOnInit() {

    this.fetch1();
    this.fetch2();
    this.fetch3();
    this.fetch4();
  }

  onPage(event) {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      console.log('paged!', event);
    }, 100);
  }

  fetch1() {
    try {
      this.httpClient.get<DataResult>(this._apiURLGetSample2019GantiPresiden)
      .subscribe(result => {

        this._dataGetSample2019GantiPresiden = result as DataResult;

        for (let i = 0; i < this._dataGetSample2019GantiPresiden.data.sample_tweets.length; i++) {
          try {
            this._dataGetSample2019GantiPresiden.data.sample_tweets[i].screen_name =
              this._dataGetSample2019GantiPresiden.data.sample_tweets[i].user.screen_name;
          } catch (error) {

          }

          try {
            this._dataGetSample2019GantiPresiden.data.sample_tweets[i].link =
              this._dataGetSample2019GantiPresiden.data.sample_tweets[i].entities.urls[0].url;
            console.log(this._dataGetSample2019GantiPresiden.data.sample_tweets[i].user.screen_name);
            console.log(this._dataGetSample2019GantiPresiden.data.sample_tweets[i].entities);
          } catch (error) {

          }

          try {
            this._dataGetSample2019GantiPresiden.data.sample_tweets[i].location =
              this._dataGetSample2019GantiPresiden.data.sample_tweets[i].user.location;
          } catch (error) {

          }
        }

        this.rows1 = this._dataGetSample2019GantiPresiden.data.sample_tweets;

        this._dataLoaded = true;
      },
      error => {
        console.log(error);
      });
    } catch (error) {
      swal(
          'Infomation!',
          'Can\'t connect to server.',
          'error'
      );
    }
  }

  fetch2() {
    try {
      this.httpClient.get<DataResult>(this._apiURLGetSample2019TetapJokowi)
      .subscribe(result => {

        this._dataGetSample2019TetapJokowi = result as DataResult;

        for (let i = 0; i < this._dataGetSample2019TetapJokowi.data.sample_tweets.length; i++) {
          try {
            this._dataGetSample2019TetapJokowi.data.sample_tweets[i].screen_name =
              this._dataGetSample2019TetapJokowi.data.sample_tweets[i].user.screen_name;
          } catch (error) {

          }

          try {
            this._dataGetSample2019TetapJokowi.data.sample_tweets[i].link =
              this._dataGetSample2019TetapJokowi.data.sample_tweets[i].entities.urls[0].url;
          } catch (error) {

          }

          try {
            this._dataGetSample2019TetapJokowi.data.sample_tweets[i].location =
              this._dataGetSample2019TetapJokowi.data.sample_tweets[i].user.location;
          } catch (error) {

          }
        }

        this.rows2 = this._dataGetSample2019TetapJokowi.data.sample_tweets;

        this._dataLoaded = true;
      },
      error => {
        console.log(error);
      });
    } catch (error) {
      swal(
          'Infomation!',
          'Can\'t connect to server.',
          'error'
      );
    }
  }

  fetch3() {
    try {
      this.httpClient.get<DataResult>(this._apiURLGetSampleJokowi)
      .subscribe(result => {

        this._dataGetSampleJokowi = result as DataResult;

        for (let i = 0; i < this._dataGetSampleJokowi.data.sample_tweets.length; i++) {
          try {
            this._dataGetSampleJokowi.data.sample_tweets[i].screen_name =
              this._dataGetSampleJokowi.data.sample_tweets[i].user.screen_name;
          } catch (error) {

          }

          try {
            this._dataGetSampleJokowi.data.sample_tweets[i].link =
              this._dataGetSampleJokowi.data.sample_tweets[i].entities.urls[0].url;
          } catch (error) {

          }

          try {
            this._dataGetSampleJokowi.data.sample_tweets[i].location =
              this._dataGetSampleJokowi.data.sample_tweets[i].user.location;
          } catch (error) {

          }
        }
        this.rows3 = this._dataGetSampleJokowi.data.sample_tweets;

        this._dataLoaded = true;
      },
      error => {
        console.log(error);
      });
    } catch (error) {
      swal(
          'Infomation!',
          'Can\'t connect to server.',
          'error'
      );
    }
  }

  fetch4() {
    try {
      this.httpClient.get<DataResult>(this._apiURLGetSamplePrabowo)
      .subscribe(result => {

        this._dataGetSamplePrabowo = result as DataResult;

        for (let i = 0; i < this._dataGetSamplePrabowo.data.sample_tweets.length; i++) {
          try {
            this._dataGetSamplePrabowo.data.sample_tweets[i].screen_name =
              this._dataGetSamplePrabowo.data.sample_tweets[i].user.screen_name;
          } catch (error) {

          }

          try {
            this._dataGetSamplePrabowo.data.sample_tweets[i].link =
              this._dataGetSamplePrabowo.data.sample_tweets[i].entities.urls[0].url;
          } catch (error) {

          }

          try {
            this._dataGetSamplePrabowo.data.sample_tweets[i].location =
              this._dataGetSamplePrabowo.data.sample_tweets[i].user.location;
          } catch (error) {

          }
        }

        this.rows4 = this._dataGetSamplePrabowo.data.sample_tweets;

        this._dataLoaded = true;
      },
      error => {
        console.log(error);
      });
    } catch (error) {
      swal(
          'Infomation!',
          'Can\'t connect to server.',
          'error'
      );
    }
  }

  toggleExpandRow(row) {
    this.table.rowDetail.toggleExpandRow(row);
  }

  onDetailToggle(event) {}
}


export interface DataTopic {
  _id: string;
  created_at: string;
  id: string;
  mongo_created_at: Date;
  text: string;
  user: {
    name: string;
    screen_name: string;
    location?: string;
  };
  entities?: {
    urls?: {
      url?: string
    }[]
  }
  search_by: string;
  search_value: string;
  scoring_progress: boolean;
  scoring_point: number;
  scoring_sentiment: string;
  scoring_progress_date: Date;
  screen_name: string;
  link?: string;
  location?: string;
}

export interface DataTotal {
  sample_tweets: Array<DataTopic>;
}

export interface DataResult {
  code: number;
  success: boolean;
  message: string;
  data: DataTotal;
}
