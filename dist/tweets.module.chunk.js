webpackJsonp(["tweets.module"],{

/***/ "../../../../../src/app/data/tweets/tweets.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n\r\n  <!-- counter-card-1 start--> \r\n  <div class=\"col-md-12 col-xl-4\">\r\n    <div class=\"card social-widget-card\">\r\n      <div class=\"card-block-big bg-twitter\">\r\n        <h1 *ngIf=\"_dataLoaded\">{{numberWithCommas(_dataGetTotalTweet.data.total_tweet)}}</h1>\r\n        <span class=\"m-t-10\">Total Tweets</span>\r\n        <i class=\"icofont icofont-social-twitter\"></i>\r\n      </div>\r\n    </div>\r\n  </div>  \r\n  <div class=\"col-md-12 col-xl-3\">\r\n    <div class=\"card social-widget-card\">\r\n      <div class=\"card-block-big bg-linkein\">\r\n        <h1 *ngIf=\"_dataLoaded\">{{numberWithCommas(_dataGetTotalTweet.data.tweet_per_day)}}</h1>\r\n        <span class=\"m-t-10\">Rata-rata tweets per hari</span>\r\n        <i class=\"icofont icofont-social-twitter\"></i>\r\n      </div>\r\n    </div>\r\n  </div> \r\n  <div class=\"col-md-12 col-xl-5\">\r\n    <div class=\"card table-card widget-success-card\" style=\"margin-bottom: 15px;\">\r\n      <div class=\"row-table\">\r\n        <div class=\"col-sm-3 card-block-medium\">\r\n          <i class=\"icofont icofont-clock-time\"></i>\r\n          <i class=\"icofont icofont-simple-up\"></i>\r\n        </div> \r\n        <div class=\"col-sm-9\"> \r\n          <h6 class=\"ng-tns-c3-1\" *ngIf=\"_dataLoaded\">{{formattedDate(_dataGetTotalTweet.data.first_tweet)}}</h6>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"card table-card widget-success-card\">\r\n      <div class=\"row-table\">\r\n        <div class=\"col-sm-3 card-block-medium\">\r\n          <i class=\"icofont icofont-clock-time\"></i>\r\n          <i class=\"icofont icofont-simple-down\"></i>\r\n        </div>\r\n        <div class=\"col-sm-9\"> \r\n          <h6 class=\"ng-tns-c3-1\" *ngIf=\"_dataLoaded\">{{formattedDate(_dataGetTotalTweet.data.last_tweet)}}</h6>\r\n        </div>\r\n      </div>\r\n    </div> \r\n  </div> \r\n  <!-- counter-card-1 end-->\r\n\r\n\r\n  <!-- counter-card-2 start -->\r\n  <div class=\"col-md-6 col-xl-3\">\r\n    <div class=\"card client-blocks dark-hashtag1-border\">\r\n      <div class=\"card-block\">\r\n        <h5><b>#2019GantiPresiden</b></h5>\r\n        <ul>\r\n          <li>\r\n            <i class=\"icofont icofont-document-folder\" style=\"font-size: 60px;\"></i>\r\n          </li>\r\n          <li class=\"text-right\">\r\n            <h2  *ngIf=\"_dataLoaded\"><b>{{_dataGetTotalTweet.data.topics[0].percentage}}%</b></h2>\r\n            <h3  *ngIf=\"_dataLoaded\">{{numberWithCommas(_dataGetTotalTweet.data.topics[0].count_all)}}</h3>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-md-6 col-xl-3\">\r\n    <div class=\"card client-blocks dark-hashtag2-border\">\r\n      <div class=\"card-block\">\r\n        <h5><b>#2019TetapJokowi</b></h5>\r\n        <ul>\r\n          <li>\r\n            <i class=\"icofont icofont-document-folder\" style=\"font-size: 60px;\"></i>\r\n          </li>\r\n          <li class=\"text-right\">\r\n            <h2  *ngIf=\"_dataLoaded\"><b>{{_dataGetTotalTweet.data.topics[1].percentage}}%</b></h2>\r\n            <h3  *ngIf=\"_dataLoaded\">{{numberWithCommas(_dataGetTotalTweet.data.topics[1].count_all)}}</h3>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-md-6 col-xl-3\">\r\n    <div class=\"card client-blocks dark-keyword1-border\">\r\n      <div class=\"card-block\">\r\n        <h5><b>Jokowi</b></h5>\r\n        <ul>\r\n          <li>\r\n            <i class=\"icofont icofont-document-folder\" style=\"font-size: 60px;\"></i>\r\n          </li>\r\n          <li class=\"text-right\">\r\n            <h2  *ngIf=\"_dataLoaded\"><b>{{_dataGetTotalTweet.data.topics[2].percentage}}%</b></h2>\r\n            <h3  *ngIf=\"_dataLoaded\">{{numberWithCommas(_dataGetTotalTweet.data.topics[2].count_all)}}</h3>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-md-6 col-xl-3\">\r\n    <div class=\"card client-blocks dark-keyword2-border\">\r\n      <div class=\"card-block\">\r\n        <h5><b>Prabowo</b></h5>\r\n        <ul>\r\n          <li>\r\n            <i class=\"icofont icofont-document-folder\" style=\"font-size: 60px;\"></i>\r\n          </li>\r\n          <li class=\"text-right\">\r\n            <h2  *ngIf=\"_dataLoaded\"><b>{{_dataGetTotalTweet.data.topics[3].percentage}}%</b></h2>\r\n            <h3  *ngIf=\"_dataLoaded\">{{numberWithCommas(_dataGetTotalTweet.data.topics[3].count_all)}}</h3>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <!-- counter-card-2 end -->\r\n  \r\n\r\n\r\n  <!-- DISCRETE BAR CHART start -->\r\n  <div class=\"col-md-12 col-lg-12\">\r\n    <app-card [title]=\"'Total Tweets'\" [headerContent]=\"'Perbandingan Jumlah Tweet Masing Masing #hashtag atau keyword'\">\r\n      <nvd3 [options]=\"discreteBarOptions\" [data]=\"discreteBarData\"></nvd3>\r\n    </app-card>\r\n  </div>\r\n  <!-- DISCRETE BAR CHART Ends -->\r\n\r\n\r\n\r\n    \r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/data/tweets/tweets.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TweetsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_d3__ = __webpack_require__("../../../../d3/d3.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_d3___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_d3__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_elements_animation__ = __webpack_require__("../../../../../src/app/shared/elements/animation.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_sweetalert2__ = __webpack_require__("../../../../sweetalert2/dist/sweetalert2.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_sweetalert2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_sweetalert2__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var TweetsComponent = (function () {
    function TweetsComponent(httpClient) {
        this.httpClient = httpClient;
        this.messageError = '';
        this._apiURLGetTotalTweet = '/api/getTotalTweet';
    }
    TweetsComponent.prototype.ngOnInit = function () {
        this.getTotalTweet();
    };
    TweetsComponent.prototype.getTotalTweet = function () {
        var _this = this;
        try {
            this.httpClient.get(this._apiURLGetTotalTweet)
                .subscribe(function (result) {
                _this._dataGetTotalTweet = result;
                // this._dataGetTotalTweet.data.first_tweet = formattedDate(this._dataGetTotalTweet.data.first_tweet);
                // this._dataGetTotalTweet.data.last_tweet = formattedDate(this._dataGetTotalTweet.data.last_tweet);
                console.log(_this.formattedDate(_this._dataGetTotalTweet.data.first_tweet));
                console.log(_this.formattedDate(_this._dataGetTotalTweet.data.last_tweet));
                _this.discreteBarOptions = {
                    chart: {
                        type: 'discreteBarChart',
                        height: 400,
                        margin: {
                            top: 50,
                            right: 50,
                            bottom: 50,
                            left: 100
                        },
                        x: function (d) {
                            return d.label;
                        },
                        y: function (d) {
                            return d.value;
                        },
                        xAxis: {
                            showMaxMin: false,
                            tickPadding: 15
                        },
                        yAxis: {
                            tickFormat: function (d) {
                                return d3.format(',.0f')(d);
                            },
                            tickPadding: 10
                        },
                        valueFormat: d3.format(',.0f'),
                        staggerLabels: false,
                        showValues: true,
                        tooltips: true
                    }
                };
                _this.discreteBarData = [{
                        key: 'Cumulative Return',
                        values: [{
                                'label': '#' + _this._dataGetTotalTweet.data.topics[0].search_value,
                                'value': _this._dataGetTotalTweet.data.topics[0].count_all,
                                'color': '#00A14D'
                            }, {
                                'label': '#' + _this._dataGetTotalTweet.data.topics[1].search_value,
                                'value': _this._dataGetTotalTweet.data.topics[1].count_all,
                                'color': '#212467'
                            }, {
                                'label': _this._dataGetTotalTweet.data.topics[2].search_value,
                                'value': _this._dataGetTotalTweet.data.topics[2].count_all,
                                'color': '#DA201B'
                            }, {
                                'label': _this._dataGetTotalTweet.data.topics[3].search_value,
                                'value': _this._dataGetTotalTweet.data.topics[3].count_all,
                                'color': '#E3B402'
                            }]
                    }];
                _this._dataLoaded = true;
            }, function (error) {
                console.log(error);
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_4_sweetalert2___default()('Infomation!', 'Can\'t connect to server.', 'error');
        }
    };
    TweetsComponent.prototype.formattedDate = function (date) {
        __WEBPACK_IMPORTED_MODULE_5_moment__["locale"]('id');
        return __WEBPACK_IMPORTED_MODULE_5_moment__(date).format('dddd DD MMMM YYYY HH:mm:ss');
    };
    TweetsComponent.prototype.numberWithCommas = function (x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
    };
    return TweetsComponent;
}());
TweetsComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-tweets',
        template: __webpack_require__("../../../../../src/app/data/tweets/tweets.component.html"),
        styles: [__webpack_require__("../../../../c3/c3.min.css"), __webpack_require__("../../../../../src/assets/icon/SVG-animated/svg-weather.css"), __webpack_require__("../../../../../src/assets/css/chartist.css"), __webpack_require__("../../../../nvd3/build/nv.d3.css")],
        encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewEncapsulation"].None,
        animations: [__WEBPACK_IMPORTED_MODULE_2__shared_elements_animation__["a" /* fadeInOutTranslate */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */]) === "function" && _a || Object])
], TweetsComponent);

var _a;
//# sourceMappingURL=tweets.component.js.map

/***/ }),

/***/ "../../../../../src/app/data/tweets/tweets.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TweetsModule", function() { return TweetsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__tweets_component__ = __webpack_require__("../../../../../src/app/data/tweets/tweets.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__tweets_routing__ = __webpack_require__("../../../../../src/app/data/tweets/tweets.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var TweetsModule = (function () {
    function TweetsModule() {
    }
    return TweetsModule;
}());
TweetsModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["g" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_4__tweets_routing__["a" /* TweetsRoutes */]),
            __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__["a" /* SharedModule */],
            __WEBPACK_IMPORTED_MODULE_6__angular_common_http__["b" /* HttpClientModule */]
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_3__tweets_component__["a" /* TweetsComponent */]],
        providers: []
    })
], TweetsModule);

//# sourceMappingURL=tweets.module.js.map

/***/ }),

/***/ "../../../../../src/app/data/tweets/tweets.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TweetsRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__tweets_component__ = __webpack_require__("../../../../../src/app/data/tweets/tweets.component.ts");

var TweetsRoutes = [{
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__tweets_component__["a" /* TweetsComponent */],
        data: {
            breadcrumb: 'Data Tweets'
        }
    }];
//# sourceMappingURL=tweets.routing.js.map

/***/ })

});
//# sourceMappingURL=tweets.module.chunk.js.map