webpackJsonp(["progress.module"],{

/***/ "../../../../../src/app/data/progress/progress.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n\r\n  <!-- counter-card-1 start--> \r\n  <div class=\"col-md-12 col-xl-12\">\r\n    <app-card [title]=\"'Total Progress'\" [headerContent]=\"'Progress Pengambilan Sentiment Dari Seluruh Tweets'\"> \r\n      <div class=\"row\">\r\n        <div class=\"col-md-12 col-lg-5\">\r\n          <google-chart [data]=\"donutChartData\"></google-chart>\r\n        </div> \r\n        <div class=\"col-md-12 col-lg-7\">\r\n          <br/><br/><br/> \r\n          <label class=\"badge badge-lg bg-danger\"><i class=\"fa fa-circle-o\"></i></label>  \r\n          <h5 *ngIf=\"_dataLoaded\">&nbsp;&nbsp;Belum diproses {{_dataGetTotalProgress.data.percentage_unfinish}}% ({{numberWithCommas(_dataGetTotalProgress.data.count_unfinish)}} tweet) </h5>\r\n          <br>\r\n          <label class=\"badge badge-lg bg-success\"><i class=\"fa fa-circle-o\"></i></label>  \r\n          <h5 *ngIf=\"_dataLoaded\">&nbsp;&nbsp;Sudah diproses {{_dataGetTotalProgress.data.percentage_finish}}% ({{numberWithCommas(_dataGetTotalProgress.data.count_finish)}} tweet) </h5>\r\n        </div>\r\n      </div>\r\n    </app-card>\r\n  </div>   \r\n  <!-- counter-card-1 end-->\r\n\r\n\r\n  <!-- counter-card-2 start --> \r\n  <div class=\"col-md-12 col-lg-12\">\r\n    <app-card [title]=\"'Perbandingan Total Progress'\" [headerContent]=\"'Progress Pengambilan Sentiment Masing-Masing #hashtag atau keyword'\">\r\n      <div class=\"row\"> \r\n        <div class=\"col-md-12 col-lg-1\"></div>\r\n        <div class=\"col-md-12 col-lg-11\">\r\n          <h3>#2019GantiPresiden</h3> \r\n          <google-chart [data]=\"barChartData1\"></google-chart>\r\n          <br><br><br>\r\n        </div>\r\n\r\n        \r\n        <div class=\"col-md-12 col-lg-1\"></div>\r\n        <div class=\"col-md-12 col-lg-11\">\r\n          <h3>#2019TetapJokowi</h3> \r\n          <google-chart [data]=\"barChartData2\"></google-chart>\r\n          <br><br><br>\r\n        </div>\r\n\r\n        \r\n        <div class=\"col-md-12 col-lg-1\"></div>\r\n        <div class=\"col-md-12 col-lg-11\">\r\n          <h3>Jokowi</h3> \r\n          <google-chart [data]=\"barChartData3\"></google-chart>\r\n          <br><br><br>\r\n        </div>\r\n\r\n        \r\n        <div class=\"col-md-12 col-lg-1\"></div>\r\n        <div class=\"col-md-12 col-lg-11\">\r\n          <h3>Prabowo</h3> \r\n          <google-chart [data]=\"barChartData4\"></google-chart>\r\n          <br><br><br>\r\n        </div>\r\n      </div>\r\n    </app-card>\r\n  </div>\r\n  <!-- counter-card-2 end -->\r\n  \r\n \r\n\r\n\r\n    \r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/data/progress/progress.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProgressComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_d3__ = __webpack_require__("../../../../d3/d3.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_d3___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_d3__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_elements_animation__ = __webpack_require__("../../../../../src/app/shared/elements/animation.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_sweetalert2__ = __webpack_require__("../../../../sweetalert2/dist/sweetalert2.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_sweetalert2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_sweetalert2__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ProgressComponent = (function () {
    function ProgressComponent(httpClient) {
        this.httpClient = httpClient;
        this._apiURLGetTotalProgress = '/api/getTotalProgress';
    }
    ProgressComponent.prototype.ngOnInit = function () {
        this.getTotalProgress();
    };
    ProgressComponent.prototype.getTotalProgress = function () {
        var _this = this;
        try {
            this.httpClient.get(this._apiURLGetTotalProgress)
                .subscribe(function (result) {
                _this._dataGetTotalProgress = result;
                console.log(_this._dataGetTotalProgress.data.count_finish);
                _this.donutChartData = {
                    chartType: 'PieChart',
                    dataTable: [
                        ['Keterangan', 'Jumlah Persentase', { role: 'annotation' }],
                        ['Belum diproses', _this._dataGetTotalProgress.data.count_unfinish, _this._dataGetTotalProgress.data.count_unfinish],
                        ['Sudah diproses', _this._dataGetTotalProgress.data.count_finish, _this._dataGetTotalProgress.data.count_finish]
                    ],
                    options: {
                        height: 350,
                        is3D: true,
                        title: '',
                        pieHole: 0.3,
                        colors: ['#e74c3c', '#2ecc71'],
                        chartArea: {
                            left: 100,
                            height: 300,
                            width: 300,
                        },
                        legend: {
                            position: 'none',
                            maxLines: 1,
                            textStyle: {
                                fontSize: 25
                            }
                        },
                    },
                };
                _this.barChartData1 = {
                    chartType: 'BarChart',
                    dataTable: [
                        ['Keterangan', '', { role: 'style' }, { role: 'annotation' }],
                        ['Belum diproses', _this._dataGetTotalProgress.data.topics[0].count_unfinish, '#e74c3c',
                            _this._dataGetTotalProgress.data.topics[0].percentage_unfinish + '%'],
                        ['Sudah diproses', _this._dataGetTotalProgress.data.topics[0].count_finish, '#2ecc71',
                            _this._dataGetTotalProgress.data.topics[0].percentage_finish + '%']
                    ],
                    options: {
                        height: 150,
                        chartArea: {
                            left: 100,
                        },
                        title: '',
                        legend: { position: 'none' },
                    },
                };
                _this.barChartData2 = {
                    chartType: 'BarChart',
                    dataTable: [
                        ['Keterangan', '', { role: 'style' }, { role: 'annotation' }],
                        ['Belum diproses', _this._dataGetTotalProgress.data.topics[1].count_unfinish, '#e74c3c',
                            _this._dataGetTotalProgress.data.topics[1].percentage_unfinish + '%'],
                        ['Sudah diproses', _this._dataGetTotalProgress.data.topics[1].count_finish, '#2ecc71',
                            _this._dataGetTotalProgress.data.topics[1].percentage_finish + '%']
                    ],
                    options: {
                        height: 150,
                        chartArea: {
                            left: 100,
                        },
                        title: '',
                        legend: { position: 'none' },
                    },
                };
                _this.barChartData3 = {
                    chartType: 'BarChart',
                    dataTable: [
                        ['Keterangan', '', { role: 'style' }, { role: 'annotation' }],
                        ['Belum diproses', _this._dataGetTotalProgress.data.topics[2].count_unfinish, '#e74c3c',
                            _this._dataGetTotalProgress.data.topics[2].percentage_unfinish + '%'],
                        ['Sudah diproses', _this._dataGetTotalProgress.data.topics[2].count_finish, '#2ecc71',
                            _this._dataGetTotalProgress.data.topics[2].percentage_finish + '%']
                    ],
                    options: {
                        height: 150,
                        chartArea: {
                            left: 100,
                        },
                        title: '',
                        legend: { position: 'none' },
                    },
                };
                _this.barChartData4 = {
                    chartType: 'BarChart',
                    dataTable: [
                        ['Keterangan', '', { role: 'style' }, { role: 'annotation' }],
                        ['Belum diproses', _this._dataGetTotalProgress.data.topics[3].count_unfinish, '#e74c3c',
                            _this._dataGetTotalProgress.data.topics[3].percentage_unfinish + '%'],
                        ['Sudah diproses', _this._dataGetTotalProgress.data.topics[3].count_finish, '#2ecc71',
                            _this._dataGetTotalProgress.data.topics[3].percentage_finish + '%']
                    ],
                    options: {
                        height: 150,
                        chartArea: {
                            left: 100,
                        },
                        title: '',
                        legend: { position: 'none' },
                    },
                };
                _this._dataLoaded = true;
            }, function (error) {
                console.log(error);
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_4_sweetalert2___default()('Infomation!', 'Can\'t connect to server.', 'error');
        }
    };
    ProgressComponent.prototype.numberWithCommas = function (x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
    };
    return ProgressComponent;
}());
ProgressComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-progress',
        template: __webpack_require__("../../../../../src/app/data/progress/progress.component.html"),
        styles: [__webpack_require__("../../../../c3/c3.min.css"), __webpack_require__("../../../../../src/assets/icon/SVG-animated/svg-weather.css"), __webpack_require__("../../../../../src/assets/css/chartist.css"), __webpack_require__("../../../../nvd3/build/nv.d3.css")],
        encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewEncapsulation"].None,
        animations: [__WEBPACK_IMPORTED_MODULE_2__shared_elements_animation__["a" /* fadeInOutTranslate */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */]) === "function" && _a || Object])
], ProgressComponent);

var _a;
//# sourceMappingURL=progress.component.js.map

/***/ }),

/***/ "../../../../../src/app/data/progress/progress.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProgressModule", function() { return ProgressModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__progress_component__ = __webpack_require__("../../../../../src/app/data/progress/progress.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__progress_routing__ = __webpack_require__("../../../../../src/app/data/progress/progress.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var ProgressModule = (function () {
    function ProgressModule() {
    }
    return ProgressModule;
}());
ProgressModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["g" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_4__progress_routing__["a" /* ProgressRoutes */]),
            __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__["a" /* SharedModule */],
            __WEBPACK_IMPORTED_MODULE_6__angular_common_http__["b" /* HttpClientModule */]
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_3__progress_component__["a" /* ProgressComponent */]]
    })
], ProgressModule);

//# sourceMappingURL=progress.module.js.map

/***/ }),

/***/ "../../../../../src/app/data/progress/progress.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProgressRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__progress_component__ = __webpack_require__("../../../../../src/app/data/progress/progress.component.ts");

var ProgressRoutes = [{
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__progress_component__["a" /* ProgressComponent */],
        data: {
            breadcrumb: "Data Progress"
        }
    }];
//# sourceMappingURL=progress.routing.js.map

/***/ })

});
//# sourceMappingURL=progress.module.chunk.js.map