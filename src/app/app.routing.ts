import {Routes} from '@angular/router';

import {AdminLayoutComponent} from './layouts/admin/admin-layout.component';
import {AuthLayoutComponent} from './layouts/auth/auth-layout.component';

export const AppRoutes: Routes = [{
  path: '',
  component: AdminLayoutComponent,
  children: [
    {
      path: '',
      redirectTo: 'tweets',
      pathMatch: 'full'
    }, {
      path: 'tweets',
      loadChildren: './data/tweets/tweets.module#TweetsModule'
    }, {
      path: 'progress',
      loadChildren: './data/progress/progress.module#ProgressModule'
    }, {
      path: 'sentiment',
      loadChildren: './data/sentiment/sentiment.module#SentimentModule'
    }, {
      path: 'sample-tweets',
      loadChildren: './data/sample-tweets/sample-tweets.module#SampleTweetsModule'
    },
  ]
}, {
  path: '',
  component: AuthLayoutComponent,
  children: [
  // {
  //   path: 'auth/login',
  //   loadChildren: './auth/login/login.module#LoginModule'
  // },
  // {
  //   path: 'auth/forgot',
  //   loadChildren: './auth/forgot/forgot.module#ForgotModule'
  // },
  // {
  //   path: 'auth/registration',
  //   loadChildren: './auth/registration/registration.module#RegistrationModule'
  // },
  {
    path: 'error',
    loadChildren: './error/error.module#ErrorModule'
  },
  {
    path: 'maintenance/offline-ui',
    loadChildren: './maintenance/offline-ui/offline-ui.module#OfflineUiModule'
  },
]
}, {
  path: '**',
  redirectTo: 'error/404'
}];
