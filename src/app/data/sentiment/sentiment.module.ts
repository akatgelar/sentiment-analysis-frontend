import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {SentimentComponent} from './sentiment.component';
import {SentimentRoutes} from './sentiment.routing';
import {SharedModule} from '../../shared/shared.module';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(SentimentRoutes),
        SharedModule,
        HttpClientModule
    ],
    declarations: [SentimentComponent]
})

export class SentimentModule {}
