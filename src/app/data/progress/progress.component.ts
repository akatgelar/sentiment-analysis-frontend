import {Component, OnInit, ViewEncapsulation} from '@angular/core';

declare var $: any;
declare var Morris: any;
import 'd3';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import { HttpClient } from '@angular/common/http';
import swal from 'sweetalert2';

@Component({
  moduleId: module.id,
  selector: 'app-progress',
  templateUrl: './progress.component.html',
  styleUrls: [
    '../../../../node_modules/c3/c3.min.css',
    '../../../assets/icon/SVG-animated/svg-weather.css',
    '../../../assets/css/chartist.css',
    '../../../../node_modules/nvd3/build/nv.d3.css'
  ],
  encapsulation: ViewEncapsulation.None,
  animations: [fadeInOutTranslate]
})
export class ProgressComponent implements OnInit {
  private _apiURLGetTotalProgress = '/api/getTotalProgress';
  private _dataGetTotalProgress: DataResult;
  private _dataLoaded: boolean;
  private donutChartData: any;
  private barChartData1: any;
  private barChartData2: any;
  private barChartData3: any;
  private barChartData4: any;

  constructor(private httpClient: HttpClient) {
  }

  ngOnInit() {
    this.getTotalProgress();
  }

  getTotalProgress() {
    try {
      this.httpClient.get<DataResult>(this._apiURLGetTotalProgress)
      .subscribe(result => {

        this._dataGetTotalProgress = result as DataResult;
        console.log(this._dataGetTotalProgress.data.count_finish)

        this.donutChartData =  {
          chartType: 'PieChart',
          dataTable: [
            ['Keterangan', 'Jumlah Persentase', { role: 'annotation' } ],
            ['Belum diproses', this._dataGetTotalProgress.data.count_unfinish, this._dataGetTotalProgress.data.count_unfinish],
            ['Sudah diproses', this._dataGetTotalProgress.data.count_finish, this._dataGetTotalProgress.data.count_finish]
          ],
          options: {
            height: 350,
            is3D: true,
            title: '',
            pieHole: 0.3,
            colors: ['#e74c3c', '#2ecc71'],
            chartArea: {
              left: 100,
              height: 300,
              width: 300,
            },
            legend: {
              position: 'none',
              maxLines: 1,
              textStyle: {
                fontSize: 25
              }
            },
          },
        };

        this.barChartData1 =  {
          chartType: 'BarChart',
          dataTable: [
            ['Keterangan', '', { role: 'style' }, { role: 'annotation' } ],
            ['Belum diproses', this._dataGetTotalProgress.data.topics[0].count_unfinish, '#e74c3c',
              this._dataGetTotalProgress.data.topics[0].percentage_unfinish + '%'],
            ['Sudah diproses', this._dataGetTotalProgress.data.topics[0].count_finish, '#2ecc71',
              this._dataGetTotalProgress.data.topics[0].percentage_finish + '%']
          ],
          options: {
            height: 150,
            chartArea: {
              left: 100,
            },
            title: '',
            legend: { position: 'none' },
          },
        };

        this.barChartData2 =  {
          chartType: 'BarChart',
          dataTable: [
            ['Keterangan', '', { role: 'style' }, { role: 'annotation' } ],
            ['Belum diproses', this._dataGetTotalProgress.data.topics[1].count_unfinish, '#e74c3c',
              this._dataGetTotalProgress.data.topics[1].percentage_unfinish + '%'],
            ['Sudah diproses', this._dataGetTotalProgress.data.topics[1].count_finish, '#2ecc71',
              this._dataGetTotalProgress.data.topics[1].percentage_finish + '%']
          ],
          options: {
            height: 150,
            chartArea: {
              left: 100,
            },
            title: '',
            legend: { position: 'none' },
          },
        };

        this.barChartData3 =  {
          chartType: 'BarChart',
          dataTable: [
            ['Keterangan', '', { role: 'style' }, { role: 'annotation' } ],
            ['Belum diproses', this._dataGetTotalProgress.data.topics[2].count_unfinish, '#e74c3c',
              this._dataGetTotalProgress.data.topics[2].percentage_unfinish + '%'],
            ['Sudah diproses', this._dataGetTotalProgress.data.topics[2].count_finish, '#2ecc71',
              this._dataGetTotalProgress.data.topics[2].percentage_finish + '%']
          ],
          options: {
            height: 150,
            chartArea: {
              left: 100,
            },
            title: '',
            legend: { position: 'none' },
          },
        };

        this.barChartData4 =  {
          chartType: 'BarChart',
          dataTable: [
            ['Keterangan', '', { role: 'style' }, { role: 'annotation' } ],
            ['Belum diproses', this._dataGetTotalProgress.data.topics[3].count_unfinish, '#e74c3c',
              this._dataGetTotalProgress.data.topics[3].percentage_unfinish + '%'],
            ['Sudah diproses', this._dataGetTotalProgress.data.topics[3].count_finish, '#2ecc71',
              this._dataGetTotalProgress.data.topics[3].percentage_finish + '%']
          ],
          options: {
            height: 150,
            chartArea: {
              left: 100,
            },
            title: '',
            legend: { position: 'none' },
          },
        };

        this._dataLoaded = true;
      },
      error => {
        console.log(error);
      });

    } catch (error) {
      swal(
          'Infomation!',
          'Can\'t connect to server.',
          'error'
      );
    }
  }


  numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }

}



export interface DataTopic {
  search_by: string;
  search_value: string;
  count_all: number;
  count_finish: number;
  count_unfinish: number;
  percentage_finish: string;
  percentage_unfinish: string;
  percentage: number;
}

export interface DataTotal {
  count_all: number;
  count_finish: number;
  count_unfinish: number;
  percentage_finish: string;
  percentage_unfinish: string;
  topics: Array<DataTopic>;
}

export interface DataResult {
  code: number;
  success: boolean;
  message: string;
  data: DataTotal;
}

