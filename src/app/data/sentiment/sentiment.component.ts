import {Component, OnInit, ViewEncapsulation} from '@angular/core';

declare var $: any;
declare var Morris: any;
import 'd3';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import { HttpClient } from '@angular/common/http';
import swal from 'sweetalert2';

@Component({
  moduleId: module.id,
  selector: 'app-sentiment',
  templateUrl: './sentiment.component.html',
  styleUrls: [
    '../../../../node_modules/c3/c3.min.css',
    '../../../assets/icon/SVG-animated/svg-weather.css',
    '../../../assets/css/chartist.css',
    '../../../../node_modules/nvd3/build/nv.d3.css'
  ],
  encapsulation: ViewEncapsulation.None,
  animations: [fadeInOutTranslate]
})
export class SentimentComponent implements OnInit {
  private _apiURLGetTotalSentiment = '/api/getTotalSentiment';
  private _dataGetTotalSentiment: DataResult;
  private _dataLoaded: boolean;
  private donutChartData1: any;
  private donutChartData2: any;
  private donutChartData3: any;
  private donutChartData4: any;


  constructor(private httpClient: HttpClient) { }

  ngOnInit() {
    this.getTotalSentiment();
  }


  getTotalSentiment() {
    try {
      this.httpClient.get<DataResult>(this._apiURLGetTotalSentiment)
      .subscribe(result => {

        this._dataGetTotalSentiment = result as DataResult;
        console.log(this._dataGetTotalSentiment.data.sentiment_negatif)

        this.donutChartData1 =  {
          chartType: 'PieChart',
          dataTable: [
            ['Keterangan', 'Jumlah Persentase', { role: 'annotation' } ],
            ['Positif', this._dataGetTotalSentiment.data.topics[0].sentiment_positif,
              this.numberWithCommas(this._dataGetTotalSentiment.data.topics[0].sentiment_positif)],
            ['Netral', this._dataGetTotalSentiment.data.topics[0].sentiment_netral,
              this.numberWithCommas(this._dataGetTotalSentiment.data.topics[0].sentiment_netral)],
            ['Negatif', this._dataGetTotalSentiment.data.topics[0].sentiment_negatif,
              this.numberWithCommas(this._dataGetTotalSentiment.data.topics[0].sentiment_negatif)],
          ],
          options: {
            height: 300,
            title: '',
            pieHole: 0.3,
            colors: ['#2ecc71', '#f1c40f', '#e74c3c'],
            chartArea: {
              height: 250,
              width: 250,
              left: 50,
              right: 50,
            },
            legend: {
              position: 'none',
              maxLines: 1,
              textStyle: {
                fontSize: 25
              }
            },
          },
        };


        this.donutChartData2 =  {
          chartType: 'PieChart',
          dataTable: [
            ['Keterangan', 'Jumlah Persentase', { role: 'annotation' } ],
            ['Positif', this._dataGetTotalSentiment.data.topics[1].sentiment_positif,
              this.numberWithCommas(this._dataGetTotalSentiment.data.topics[1].sentiment_positif)],
            ['Netral', this._dataGetTotalSentiment.data.topics[1].sentiment_netral,
              this.numberWithCommas(this._dataGetTotalSentiment.data.topics[1].sentiment_netral)],
            ['Negatif', this._dataGetTotalSentiment.data.topics[1].sentiment_negatif,
              this.numberWithCommas(this._dataGetTotalSentiment.data.topics[1].sentiment_negatif)],
          ],
          options: {
            height: 300,
            title: '',
            pieHole: 0.3,
            colors: ['#2ecc71', '#f1c40f', '#e74c3c'],
            chartArea: {
              height: 250,
              width: 250,
              left: 50,
              right: 50,
            },
            legend: {
              position: 'none',
              maxLines: 1,
              textStyle: {
                fontSize: 25
              }
            },
          },
        };


        this.donutChartData3 =  {
          chartType: 'PieChart',
          dataTable: [
            ['Keterangan', 'Jumlah Persentase', { role: 'annotation' } ],
            ['Positif', this._dataGetTotalSentiment.data.topics[2].sentiment_positif,
              this.numberWithCommas(this._dataGetTotalSentiment.data.topics[2].sentiment_positif)],
            ['Netral', this._dataGetTotalSentiment.data.topics[2].sentiment_netral,
              this.numberWithCommas(this._dataGetTotalSentiment.data.topics[2].sentiment_netral)],
            ['Negatif', this._dataGetTotalSentiment.data.topics[2].sentiment_negatif,
              this.numberWithCommas(this._dataGetTotalSentiment.data.topics[2].sentiment_negatif)],
          ],
          options: {
            height: 300,
            title: '',
            pieHole: 0.3,
            colors: ['#2ecc71', '#f1c40f', '#e74c3c'],
            chartArea: {
              height: 250,
              width: 250,
              left: 50,
              right: 50,
            },
            legend: {
              position: 'none',
              maxLines: 1,
              textStyle: {
                fontSize: 25
              }
            },
          },
        };


        this.donutChartData4 =  {
          chartType: 'PieChart',
          dataTable: [
            ['Keterangan', 'Jumlah Persentase', { role: 'annotation' } ],
            ['Positif', this._dataGetTotalSentiment.data.topics[3].sentiment_positif,
              this.numberWithCommas(this._dataGetTotalSentiment.data.topics[3].sentiment_positif)],
            ['Netral', this._dataGetTotalSentiment.data.topics[3].sentiment_netral,
              this.numberWithCommas(this._dataGetTotalSentiment.data.topics[3].sentiment_netral)],
            ['Negatif', this._dataGetTotalSentiment.data.topics[3].sentiment_negatif,
              this.numberWithCommas(this._dataGetTotalSentiment.data.topics[3].sentiment_negatif)],
          ],
          options: {
            height: 300,
            title: '',
            pieHole: 0.3,
            colors: ['#2ecc71', '#f1c40f', '#e74c3c'],
            chartArea: {
              height: 250,
              width: 250,
              left: 50,
              right: 50,
            },
            legend: {
              position: 'none',
              maxLines: 1,
              textStyle: {
                fontSize: 25
              }
            },
          },
        };

        this._dataLoaded = true;
      },
      error => {
        console.log(error);
      });
    } catch (error) {
      swal(
          'Infomation!',
          'Can\'t connect to server.',
          'error'
      );
    }
  }


  numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }
}



export interface DataTopic {
  search_by: string;
  search_value: string;
  count_all: number;
  count_finish: number;
  sentiment_positif: number;
  sentiment_netral: number;
  sentiment_negatif: number;
  percentage_positif: string;
  percentage_netral: string;
  percentage_negatif: string;
}

export interface DataTotal {
  count_all: number;
  count_finish: number;
  count_unfinish: number;
  sentiment_positif: number;
  sentiment_netral: number;
  sentiment_negatif: number;
  percentage_positif: string;
  percentage_netral: string;
  percentage_negatif: string;
  topics: Array<DataTopic>;
}

export interface DataResult {
  code: number;
  success: boolean;
  message: string;
  data: DataTotal;
}

