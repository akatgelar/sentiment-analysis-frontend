import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

const MENUITEMS = [
  {
    label: 'Navigation',
    state: 'data',
    type: 'sub',
    main: [ 
         
          {
            state: 'tweets',
            name: 'Data Tweets',
            type: 'link',
            icon: 'fa fa-twitter' 
          },
          {
            state: 'progress',
            name: 'Data Progress',
            type: 'link',
            icon: 'fa fa-refresh'  
          },
          {
            state: 'sentiment',
            name: 'Sentiment',
            type: 'link',
            icon: 'fa fa-smile-o' 
          },
          {
            state: 'sample-tweets',
            name: 'Sample Tweets',
            type: 'link',
            icon: 'fa fa-list-ul' 
          }, 
        
    ],
  } 
];

@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }

  /*add(menu: Menu) {
    MENUITEMS.push(menu);
  }*/
}
