webpackJsonp(["sample-tweets.module"],{

/***/ "../../../../../src/app/data/sample-tweets/sample-tweets.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\"> \r\n\r\n\r\n  <!-- Tabination card start -->\r\n  <div class=\"col-md-12 col-xl-12\">\r\n    <div class=\"card card-border-primary\">\r\n      <div class=\"card-header\">\r\n        <h5>#2019GantiPresiden</h5> \r\n        <button class=\"btn btn-primary btn-sm f-right\" (click)=\"collapsed1 = !collapsed1\"> <i class=\"icofont icofont-eye-alt\"></i> Show / Hide </button>   \r\n      </div>\r\n      <div class=\"table-responsive\" [hidden]=\"collapsed1\">  \r\n        <ngx-datatable\r\n              #myTable\r\n              class=\"data-table expandable\"\r\n              [rows]='rows1'\r\n              [limit]=\"10\" \r\n              [columnMode]=\"force\"\r\n              [headerHeight]=\"50\"\r\n              [footerHeight]=\"50\"\r\n              [rowHeight]=\"'auto'\"\r\n              (page)=\"onPage($event)\">\r\n          <!-- Row Detail Template -->\r\n          <ngx-datatable-row-detail [rowHeight]=\"'auto'\" #myDetailRow (toggle)=\"onDetailToggle($event)\">\r\n            <ng-template let-row=\"row\" let-expanded=\"expanded\" ngx-datatable-row-detail-template let-link=\"row.link\">\r\n              <div class=\"dt-desc\">\r\n                <table class=\"table-bordered\">  \r\n                  <tr><td><b>Lokasi</b></td><td>{{row.location}}</td></tr>  \r\n                  <tr><td><b>Text</b></td><td>{{row.text}}</td></tr> \r\n                  <tr><td><b>Link</b></td><td><a href=\"{{link}}\" target=\"_blank\">{{row.link}}</a></td></tr>\r\n                </table>\r\n              </div> \r\n            </ng-template>\r\n          </ngx-datatable-row-detail>\r\n          <ngx-datatable-column\r\n            [width]=\"50\"\r\n            [resizeable]=\"false\"\r\n            [sortable]=\"false\"\r\n            [draggable]=\"false\"\r\n            [canAutoResize]=\"false\">\r\n            <ng-template let-row=\"row\" let-expanded=\"expanded\" ngx-datatable-cell-template>\r\n              <a\r\n                href=\"javascript:;\"\r\n                [class.datatable-icon-right]=\"!expanded\"\r\n                [class.datatable-icon-down]=\"expanded\"\r\n                title=\"Expand/Collapse Row\"\r\n                (click)=\"toggleExpandRow(row)\">\r\n              </a>\r\n            </ng-template>\r\n          </ngx-datatable-column>\r\n          <ngx-datatable-column name=\"Index\" [width]=\"70\" >\r\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\r\n              <strong> {{rows1.indexOf(row)+1}}</strong>\r\n            </ng-template>\r\n          </ngx-datatable-column>\r\n          <ngx-datatable-column name=\"Name\" [width]=\"180\" >\r\n            <ng-template let-row=\"row\" ngx-datatable-cell-template let-screen_name=\"row['screen_name']\">\r\n              <a href=\"https://twitter.com/{{screen_name}}\" target=\"_blank\"> @{{row['screen_name']}}</a> \r\n            </ng-template> \r\n          </ngx-datatable-column> \r\n          <ngx-datatable-column name=\"Datetime\" [width]=\"300\">\r\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\r\n              {{row['created_at']}}\r\n            </ng-template>\r\n          </ngx-datatable-column>\r\n          <ngx-datatable-column name=\"Point\" [width]=\"200\" >\r\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\r\n              <strong>{{row['scoring_point']}}</strong>\r\n            </ng-template>\r\n          </ngx-datatable-column>\r\n          <ngx-datatable-column name=\"Sentiment\" [width]=\"100\"> \r\n            <ng-template let-row=\"row\" ngx-datatable-cell-template style=\"text-align: center\">\r\n              <span class=\"label label-success\" *ngIf=\"row['scoring_sentiment'] === 'positif'\">{{row['scoring_sentiment']}}</span>\r\n              <span class=\"label label-danger\" *ngIf=\"row['scoring_sentiment'] === 'negatif'\">{{row['scoring_sentiment']}}</span>\r\n              <span class=\"label label-warning\" *ngIf=\"row['scoring_sentiment'] === 'netral'\">{{row['scoring_sentiment']}}</span>  \r\n            </ng-template>\r\n          </ngx-datatable-column>   \r\n        </ngx-datatable>  \r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n\r\n  <!-- Tabination card start -->\r\n  <div class=\"col-md-12 col-xl-12\">\r\n    <div class=\"card card-border-primary\">\r\n      <div class=\"card-header\">\r\n        <h5>#2019TetapJokowi</h5> \r\n        <button class=\"btn btn-primary btn-sm f-right\" (click)=\"collapsed2 = !collapsed2\"> <i class=\"icofont icofont-eye-alt\"></i> Show / Hide </button>   \r\n      </div>\r\n      <div class=\"table-responsive\" [hidden]=\"collapsed2\">  \r\n        <ngx-datatable\r\n              #myTable\r\n              class=\"data-table expandable\"\r\n              [rows]='rows2'\r\n              [limit]=\"10\" \r\n              [columnMode]=\"force\"\r\n              [headerHeight]=\"50\"\r\n              [footerHeight]=\"50\"\r\n              [rowHeight]=\"'auto'\"\r\n              (page)=\"onPage($event)\">\r\n          <!-- Row Detail Template -->\r\n          <ngx-datatable-row-detail [rowHeight]=\"'auto'\" #myDetailRow (toggle)=\"onDetailToggle($event)\">\r\n            <ng-template let-row=\"row\" let-expanded=\"expanded\" ngx-datatable-row-detail-template let-link=\"row.link\">\r\n              <div class=\"dt-desc\">\r\n                <table class=\"table-bordered\">  \r\n                  <tr><td><b>Lokasi</b></td><td>{{row.location}}</td></tr>  \r\n                  <tr><td><b>Text</b></td><td>{{row.text}}</td></tr> \r\n                  <tr><td><b>Link</b></td><td><a href=\"{{link}}\" target=\"_blank\">{{row.link}}</a></td></tr>\r\n                </table>\r\n              </div> \r\n            </ng-template>\r\n          </ngx-datatable-row-detail>\r\n          <ngx-datatable-column\r\n            [width]=\"50\"\r\n            [resizeable]=\"false\"\r\n            [sortable]=\"false\"\r\n            [draggable]=\"false\"\r\n            [canAutoResize]=\"false\">\r\n            <ng-template let-row=\"row\" let-expanded=\"expanded\" ngx-datatable-cell-template>\r\n              <a\r\n                href=\"javascript:;\"\r\n                [class.datatable-icon-right]=\"!expanded\"\r\n                [class.datatable-icon-down]=\"expanded\"\r\n                title=\"Expand/Collapse Row\"\r\n                (click)=\"toggleExpandRow(row)\">\r\n              </a>\r\n            </ng-template>\r\n          </ngx-datatable-column>\r\n          <ngx-datatable-column name=\"Index\" [width]=\"70\" >\r\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\r\n              <strong> {{rows2.indexOf(row)+1}}</strong>\r\n            </ng-template>\r\n          </ngx-datatable-column>\r\n          <ngx-datatable-column name=\"Name\" [width]=\"180\" >\r\n            <ng-template let-row=\"row\" ngx-datatable-cell-template let-screen_name=\"row['screen_name']\">\r\n              <a href=\"https://twitter.com/{{screen_name}}\" target=\"_blank\"> @{{row['screen_name']}}</a> \r\n            </ng-template> \r\n          </ngx-datatable-column> \r\n          <ngx-datatable-column name=\"Datetime\" [width]=\"300\">\r\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\r\n              {{row['created_at']}}\r\n            </ng-template>\r\n          </ngx-datatable-column>\r\n          <ngx-datatable-column name=\"Point\" [width]=\"200\" >\r\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\r\n              <strong>{{row['scoring_point']}}</strong>\r\n            </ng-template>\r\n          </ngx-datatable-column>\r\n          <ngx-datatable-column name=\"Sentiment\" [width]=\"100\"> \r\n            <ng-template let-row=\"row\" ngx-datatable-cell-template style=\"text-align: center\">\r\n              <span class=\"label label-success\" *ngIf=\"row['scoring_sentiment'] === 'positif'\">{{row['scoring_sentiment']}}</span>\r\n              <span class=\"label label-danger\" *ngIf=\"row['scoring_sentiment'] === 'negatif'\">{{row['scoring_sentiment']}}</span>\r\n              <span class=\"label label-warning\" *ngIf=\"row['scoring_sentiment'] === 'netral'\">{{row['scoring_sentiment']}}</span>  \r\n            </ng-template>\r\n          </ngx-datatable-column>   \r\n        </ngx-datatable>  \r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <!-- Tabination card start -->\r\n  <div class=\"col-md-12 col-xl-12\">\r\n    <div class=\"card card-border-primary\">\r\n      <div class=\"card-header\">\r\n        <h5>Jokowi</h5> \r\n        <button class=\"btn btn-primary btn-sm f-right\" (click)=\"collapsed3 = !collapsed3\"> <i class=\"icofont icofont-eye-alt\"></i> Show / Hide </button>   \r\n      </div>\r\n      <div class=\"table-responsive\" [hidden]=\"collapsed3\">  \r\n        <ngx-datatable\r\n              #myTable\r\n              class=\"data-table expandable\"\r\n              [rows]='rows3'\r\n              [limit]=\"10\" \r\n              [columnMode]=\"force\"\r\n              [headerHeight]=\"50\"\r\n              [footerHeight]=\"50\"\r\n              [rowHeight]=\"'auto'\"\r\n              (page)=\"onPage($event)\">\r\n          <!-- Row Detail Template -->\r\n          <ngx-datatable-row-detail [rowHeight]=\"'auto'\" #myDetailRow (toggle)=\"onDetailToggle($event)\">\r\n            <ng-template let-row=\"row\" let-expanded=\"expanded\" ngx-datatable-row-detail-template let-link=\"row.link\">\r\n              <div class=\"dt-desc\">\r\n                <table class=\"table-bordered\">  \r\n                  <tr><td><b>Lokasi</b></td><td>{{row.location}}</td></tr>  \r\n                  <tr><td><b>Text</b></td><td>{{row.text}}</td></tr> \r\n                  <tr><td><b>Link</b></td><td><a href=\"{{link}}\" target=\"_blank\">{{row.link}}</a></td></tr>\r\n                </table>\r\n              </div> \r\n            </ng-template>\r\n          </ngx-datatable-row-detail>\r\n          <ngx-datatable-column\r\n            [width]=\"50\"\r\n            [resizeable]=\"false\"\r\n            [sortable]=\"false\"\r\n            [draggable]=\"false\"\r\n            [canAutoResize]=\"false\">\r\n            <ng-template let-row=\"row\" let-expanded=\"expanded\" ngx-datatable-cell-template>\r\n              <a\r\n                href=\"javascript:;\"\r\n                [class.datatable-icon-right]=\"!expanded\"\r\n                [class.datatable-icon-down]=\"expanded\"\r\n                title=\"Expand/Collapse Row\"\r\n                (click)=\"toggleExpandRow(row)\">\r\n              </a>\r\n            </ng-template>\r\n          </ngx-datatable-column>\r\n          <ngx-datatable-column name=\"Index\" [width]=\"70\" >\r\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\r\n              <strong> {{rows3.indexOf(row)+1}}</strong>\r\n            </ng-template>\r\n          </ngx-datatable-column>\r\n          <ngx-datatable-column name=\"Name\" [width]=\"180\" >\r\n            <ng-template let-row=\"row\" ngx-datatable-cell-template let-screen_name=\"row['screen_name']\">\r\n              <a href=\"https://twitter.com/{{screen_name}}\" target=\"_blank\"> @{{row['screen_name']}}</a> \r\n            </ng-template> \r\n          </ngx-datatable-column> \r\n          <ngx-datatable-column name=\"Datetime\" [width]=\"300\">\r\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\r\n              {{row['created_at']}}\r\n            </ng-template>\r\n          </ngx-datatable-column>\r\n          <ngx-datatable-column name=\"Point\" [width]=\"200\" >\r\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\r\n              <strong>{{row['scoring_point']}}</strong>\r\n            </ng-template>\r\n          </ngx-datatable-column>\r\n          <ngx-datatable-column name=\"Sentiment\" [width]=\"100\"> \r\n            <ng-template let-row=\"row\" ngx-datatable-cell-template style=\"text-align: center\">\r\n              <span class=\"label label-success\" *ngIf=\"row['scoring_sentiment'] === 'positif'\">{{row['scoring_sentiment']}}</span>\r\n              <span class=\"label label-danger\" *ngIf=\"row['scoring_sentiment'] === 'negatif'\">{{row['scoring_sentiment']}}</span>\r\n              <span class=\"label label-warning\" *ngIf=\"row['scoring_sentiment'] === 'netral'\">{{row['scoring_sentiment']}}</span>  \r\n            </ng-template>\r\n          </ngx-datatable-column>   \r\n        </ngx-datatable>  \r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <!-- Tabination card start -->\r\n  <div class=\"col-md-12 col-xl-12\">\r\n    <div class=\"card card-border-primary\">\r\n      <div class=\"card-header\">\r\n        <h5>Prabowo</h5> \r\n        <button class=\"btn btn-primary btn-sm f-right\" (click)=\"collapsed4 = !collapsed4\"> <i class=\"icofont icofont-eye-alt\"></i> Show / Hide </button>   \r\n      </div>\r\n      <div class=\"table-responsive\" [hidden]=\"collapsed4\">  \r\n        <ngx-datatable\r\n              #myTable\r\n              class=\"data-table expandable\"\r\n              [rows]='rows4'\r\n              [limit]=\"10\" \r\n              [columnMode]=\"force\"\r\n              [headerHeight]=\"50\"\r\n              [footerHeight]=\"50\"\r\n              [rowHeight]=\"'auto'\"\r\n              (page)=\"onPage($event)\">\r\n          <!-- Row Detail Template -->\r\n          <ngx-datatable-row-detail [rowHeight]=\"'auto'\" #myDetailRow (toggle)=\"onDetailToggle($event)\">\r\n            <ng-template let-row=\"row\" let-expanded=\"expanded\" ngx-datatable-row-detail-template let-link=\"row.link\">\r\n              <div class=\"dt-desc\">\r\n                <table class=\"table-bordered\">  \r\n                  <tr><td><b>Lokasi</b></td><td>{{row.location}}</td></tr>  \r\n                  <tr><td><b>Text</b></td><td>{{row.text}}</td></tr> \r\n                  <tr><td><b>Link</b></td><td><a href=\"{{link}}\" target=\"_blank\">{{row.link}}</a></td></tr>\r\n                </table>\r\n              </div> \r\n            </ng-template>\r\n          </ngx-datatable-row-detail>\r\n          <ngx-datatable-column\r\n            [width]=\"50\"\r\n            [resizeable]=\"false\"\r\n            [sortable]=\"false\"\r\n            [draggable]=\"false\"\r\n            [canAutoResize]=\"false\">\r\n            <ng-template let-row=\"row\" let-expanded=\"expanded\" ngx-datatable-cell-template>\r\n              <a\r\n                href=\"javascript:;\"\r\n                [class.datatable-icon-right]=\"!expanded\"\r\n                [class.datatable-icon-down]=\"expanded\"\r\n                title=\"Expand/Collapse Row\"\r\n                (click)=\"toggleExpandRow(row)\">\r\n              </a>\r\n            </ng-template>\r\n          </ngx-datatable-column>\r\n          <ngx-datatable-column name=\"Index\" [width]=\"70\" >\r\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\r\n              <strong> {{rows4.indexOf(row)+1}}</strong>\r\n            </ng-template>\r\n          </ngx-datatable-column>\r\n          <ngx-datatable-column name=\"Name\" [width]=\"180\" >\r\n            <ng-template let-row=\"row\" ngx-datatable-cell-template let-screen_name=\"row['screen_name']\">\r\n              <a href=\"https://twitter.com/{{screen_name}}\" target=\"_blank\"> @{{row['screen_name']}}</a> \r\n            </ng-template> \r\n          </ngx-datatable-column> \r\n          <ngx-datatable-column name=\"Datetime\" [width]=\"300\">\r\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\r\n              {{row['created_at']}}\r\n            </ng-template>\r\n          </ngx-datatable-column>\r\n          <ngx-datatable-column name=\"Point\" [width]=\"200\" >\r\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\r\n              <strong>{{row['scoring_point']}}</strong>\r\n            </ng-template>\r\n          </ngx-datatable-column>\r\n          <ngx-datatable-column name=\"Sentiment\" [width]=\"100\"> \r\n            <ng-template let-row=\"row\" ngx-datatable-cell-template style=\"text-align: center\">\r\n              <span class=\"label label-success\" *ngIf=\"row['scoring_sentiment'] === 'positif'\">{{row['scoring_sentiment']}}</span>\r\n              <span class=\"label label-danger\" *ngIf=\"row['scoring_sentiment'] === 'negatif'\">{{row['scoring_sentiment']}}</span>\r\n              <span class=\"label label-warning\" *ngIf=\"row['scoring_sentiment'] === 'netral'\">{{row['scoring_sentiment']}}</span>  \r\n            </ng-template>\r\n          </ngx-datatable-column>   \r\n        </ngx-datatable>  \r\n      </div>\r\n    </div>\r\n  </div>\r\n"

/***/ }),

/***/ "../../../../../src/app/data/sample-tweets/sample-tweets.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SampleTweetsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_d3__ = __webpack_require__("../../../../d3/d3.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_d3___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_d3__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_elements_animation__ = __webpack_require__("../../../../../src/app/shared/elements/animation.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_sweetalert2__ = __webpack_require__("../../../../sweetalert2/dist/sweetalert2.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_sweetalert2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_sweetalert2__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SampleTweetsComponent = (function () {
    function SampleTweetsComponent(httpClient) {
        this.httpClient = httpClient;
        this._apiURLGetSample2019GantiPresiden = '/api/getSample2019GantiPresiden';
        this._apiURLGetSample2019TetapJokowi = '/api/getSample2019TetapJokowi';
        this._apiURLGetSampleJokowi = '/api/getSampleJokowi';
        this._apiURLGetSamplePrabowo = '/api/getSamplePrabowo';
        this.rows1 = [];
        this.rows2 = [];
        this.rows3 = [];
        this.rows4 = [];
        this.expanded = {};
        this.collapsed1 = true;
        this.collapsed2 = true;
        this.collapsed3 = true;
        this.collapsed4 = true;
    }
    SampleTweetsComponent.prototype.ngOnInit = function () {
        this.fetch1();
        this.fetch2();
        this.fetch3();
        this.fetch4();
    };
    SampleTweetsComponent.prototype.onPage = function (event) {
        clearTimeout(this.timeout);
        this.timeout = setTimeout(function () {
            console.log('paged!', event);
        }, 100);
    };
    SampleTweetsComponent.prototype.fetch1 = function () {
        var _this = this;
        try {
            this.httpClient.get(this._apiURLGetSample2019GantiPresiden)
                .subscribe(function (result) {
                _this._dataGetSample2019GantiPresiden = result;
                for (var i = 0; i < _this._dataGetSample2019GantiPresiden.data.sample_tweets.length; i++) {
                    try {
                        _this._dataGetSample2019GantiPresiden.data.sample_tweets[i].screen_name =
                            _this._dataGetSample2019GantiPresiden.data.sample_tweets[i].user.screen_name;
                    }
                    catch (error) {
                    }
                    try {
                        _this._dataGetSample2019GantiPresiden.data.sample_tweets[i].link =
                            _this._dataGetSample2019GantiPresiden.data.sample_tweets[i].entities.urls[0].url;
                        console.log(_this._dataGetSample2019GantiPresiden.data.sample_tweets[i].user.screen_name);
                        console.log(_this._dataGetSample2019GantiPresiden.data.sample_tweets[i].entities);
                    }
                    catch (error) {
                    }
                    try {
                        _this._dataGetSample2019GantiPresiden.data.sample_tweets[i].location =
                            _this._dataGetSample2019GantiPresiden.data.sample_tweets[i].user.location;
                    }
                    catch (error) {
                    }
                }
                _this.rows1 = _this._dataGetSample2019GantiPresiden.data.sample_tweets;
                _this._dataLoaded = true;
            }, function (error) {
                console.log(error);
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_4_sweetalert2___default()('Infomation!', 'Can\'t connect to server.', 'error');
        }
    };
    SampleTweetsComponent.prototype.fetch2 = function () {
        var _this = this;
        try {
            this.httpClient.get(this._apiURLGetSample2019TetapJokowi)
                .subscribe(function (result) {
                _this._dataGetSample2019TetapJokowi = result;
                for (var i = 0; i < _this._dataGetSample2019TetapJokowi.data.sample_tweets.length; i++) {
                    try {
                        _this._dataGetSample2019TetapJokowi.data.sample_tweets[i].screen_name =
                            _this._dataGetSample2019TetapJokowi.data.sample_tweets[i].user.screen_name;
                    }
                    catch (error) {
                    }
                    try {
                        _this._dataGetSample2019TetapJokowi.data.sample_tweets[i].link =
                            _this._dataGetSample2019TetapJokowi.data.sample_tweets[i].entities.urls[0].url;
                    }
                    catch (error) {
                    }
                    try {
                        _this._dataGetSample2019TetapJokowi.data.sample_tweets[i].location =
                            _this._dataGetSample2019TetapJokowi.data.sample_tweets[i].user.location;
                    }
                    catch (error) {
                    }
                }
                _this.rows2 = _this._dataGetSample2019TetapJokowi.data.sample_tweets;
                _this._dataLoaded = true;
            }, function (error) {
                console.log(error);
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_4_sweetalert2___default()('Infomation!', 'Can\'t connect to server.', 'error');
        }
    };
    SampleTweetsComponent.prototype.fetch3 = function () {
        var _this = this;
        try {
            this.httpClient.get(this._apiURLGetSampleJokowi)
                .subscribe(function (result) {
                _this._dataGetSampleJokowi = result;
                for (var i = 0; i < _this._dataGetSampleJokowi.data.sample_tweets.length; i++) {
                    try {
                        _this._dataGetSampleJokowi.data.sample_tweets[i].screen_name =
                            _this._dataGetSampleJokowi.data.sample_tweets[i].user.screen_name;
                    }
                    catch (error) {
                    }
                    try {
                        _this._dataGetSampleJokowi.data.sample_tweets[i].link =
                            _this._dataGetSampleJokowi.data.sample_tweets[i].entities.urls[0].url;
                    }
                    catch (error) {
                    }
                    try {
                        _this._dataGetSampleJokowi.data.sample_tweets[i].location =
                            _this._dataGetSampleJokowi.data.sample_tweets[i].user.location;
                    }
                    catch (error) {
                    }
                }
                _this.rows3 = _this._dataGetSampleJokowi.data.sample_tweets;
                _this._dataLoaded = true;
            }, function (error) {
                console.log(error);
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_4_sweetalert2___default()('Infomation!', 'Can\'t connect to server.', 'error');
        }
    };
    SampleTweetsComponent.prototype.fetch4 = function () {
        var _this = this;
        try {
            this.httpClient.get(this._apiURLGetSamplePrabowo)
                .subscribe(function (result) {
                _this._dataGetSamplePrabowo = result;
                for (var i = 0; i < _this._dataGetSamplePrabowo.data.sample_tweets.length; i++) {
                    try {
                        _this._dataGetSamplePrabowo.data.sample_tweets[i].screen_name =
                            _this._dataGetSamplePrabowo.data.sample_tweets[i].user.screen_name;
                    }
                    catch (error) {
                    }
                    try {
                        _this._dataGetSamplePrabowo.data.sample_tweets[i].link =
                            _this._dataGetSamplePrabowo.data.sample_tweets[i].entities.urls[0].url;
                    }
                    catch (error) {
                    }
                    try {
                        _this._dataGetSamplePrabowo.data.sample_tweets[i].location =
                            _this._dataGetSamplePrabowo.data.sample_tweets[i].user.location;
                    }
                    catch (error) {
                    }
                }
                _this.rows4 = _this._dataGetSamplePrabowo.data.sample_tweets;
                _this._dataLoaded = true;
            }, function (error) {
                console.log(error);
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_4_sweetalert2___default()('Infomation!', 'Can\'t connect to server.', 'error');
        }
    };
    SampleTweetsComponent.prototype.toggleExpandRow = function (row) {
        this.table.rowDetail.toggleExpandRow(row);
    };
    SampleTweetsComponent.prototype.onDetailToggle = function (event) { };
    return SampleTweetsComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('myTable'),
    __metadata("design:type", Object)
], SampleTweetsComponent.prototype, "table", void 0);
SampleTweetsComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-sample-tweets',
        template: __webpack_require__("../../../../../src/app/data/sample-tweets/sample-tweets.component.html"),
        styleUrls: [],
        animations: [__WEBPACK_IMPORTED_MODULE_2__shared_elements_animation__["a" /* fadeInOutTranslate */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */]) === "function" && _a || Object])
], SampleTweetsComponent);

var _a;
//# sourceMappingURL=sample-tweets.component.js.map

/***/ }),

/***/ "../../../../../src/app/data/sample-tweets/sample-tweets.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SampleTweetsModule", function() { return SampleTweetsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__sample_tweets_component__ = __webpack_require__("../../../../../src/app/data/sample-tweets/sample-tweets.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__sample_tweets_routing__ = __webpack_require__("../../../../../src/app/data/sample-tweets/sample-tweets.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var SampleTweetsModule = (function () {
    function SampleTweetsModule() {
    }
    return SampleTweetsModule;
}());
SampleTweetsModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["g" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_4__sample_tweets_routing__["a" /* SampleTweetsRoutes */]),
            __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__["a" /* SharedModule */],
            __WEBPACK_IMPORTED_MODULE_6__angular_common_http__["b" /* HttpClientModule */]
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_3__sample_tweets_component__["a" /* SampleTweetsComponent */]]
    })
], SampleTweetsModule);

//# sourceMappingURL=sample-tweets.module.js.map

/***/ }),

/***/ "../../../../../src/app/data/sample-tweets/sample-tweets.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SampleTweetsRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__sample_tweets_component__ = __webpack_require__("../../../../../src/app/data/sample-tweets/sample-tweets.component.ts");

var SampleTweetsRoutes = [{
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__sample_tweets_component__["a" /* SampleTweetsComponent */],
        data: {
            breadcrumb: "Sample Tweets"
        }
    }];
//# sourceMappingURL=sample-tweets.routing.js.map

/***/ })

});
//# sourceMappingURL=sample-tweets.module.chunk.js.map