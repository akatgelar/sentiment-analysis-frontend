import { Routes } from '@angular/router';
import { SentimentComponent } from "./sentiment.component";

export const SentimentRoutes: Routes = [{
    path: '',
    component: SentimentComponent,
    data: {
        breadcrumb: "Sentiment"
    }
}];
