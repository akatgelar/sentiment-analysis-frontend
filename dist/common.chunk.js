webpackJsonp(["common"],{

/***/ "../../../../../src/app/shared/elements/animation.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return fadeInOutTranslate; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_animations__ = __webpack_require__("../../../animations/@angular/animations.es5.js");

var fadeInOutTranslate = Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["k" /* trigger */])('fadeInOutTranslate', [
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["j" /* transition */])(':enter', [
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["i" /* style */])({ opacity: 0 }),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["e" /* animate */])('400ms ease-in-out', Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["i" /* style */])({ opacity: 1 }))
    ]),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["j" /* transition */])(':leave', [
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["i" /* style */])({ transform: 'translate(0)' }),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["e" /* animate */])('400ms ease-in-out', Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["i" /* style */])({ opacity: 0 }))
    ])
]);
//# sourceMappingURL=animation.js.map

/***/ }),

/***/ "../../../../../src/assets/css/chartist.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".ct-double-octave:after, .ct-major-eleventh:after, .ct-major-second:after, .ct-major-seventh:after, .ct-major-sixth:after, .ct-major-tenth:after, .ct-major-third:after, .ct-major-twelfth:after, .ct-minor-second:after, .ct-minor-seventh:after, .ct-minor-sixth:after, .ct-minor-third:after, .ct-octave:after, .ct-perfect-fifth:after, .ct-perfect-fourth:after, .ct-square:after {\r\n  content: \"\";\r\n  clear: both\r\n}\r\n\r\n.ct-label {\r\n  fill: rgba(0, 0, 0, .4);\r\n  color: rgba(0, 0, 0, .4);\r\n  font-size: .75rem;\r\n  line-height: 1\r\n}\r\n\r\n.ct-grid-background, .ct-line {\r\n  fill: none\r\n}\r\n\r\n.ct-chart-bar .ct-label, .ct-chart-line .ct-label {\r\n  display: block;\r\n  display: -webkit-box;\r\n  display: -ms-flexbox;\r\n  display: flex\r\n}\r\n\r\n.ct-chart-donut .ct-label, .ct-chart-pie .ct-label {\r\n  dominant-baseline: central\r\n}\r\n\r\n.ct-label.ct-horizontal.ct-start {\r\n  -webkit-box-align: flex-end;\r\n  -ms-flex-align: flex-end;\r\n  align-items: flex-end;\r\n  -webkit-box-pack: flex-start;\r\n  -ms-flex-pack: flex-start;\r\n  justify-content: flex-start;\r\n  text-align: left;\r\n  text-anchor: start\r\n}\r\n\r\n.ct-label.ct-horizontal.ct-end {\r\n  -webkit-box-align: flex-start;\r\n  -ms-flex-align: flex-start;\r\n  align-items: flex-start;\r\n  -webkit-box-pack: flex-start;\r\n  -ms-flex-pack: flex-start;\r\n  justify-content: flex-start;\r\n  text-align: left;\r\n  text-anchor: start\r\n}\r\n\r\n.ct-label.ct-vertical.ct-start {\r\n  -webkit-box-align: flex-end;\r\n  -ms-flex-align: flex-end;\r\n  align-items: flex-end;\r\n  -webkit-box-pack: flex-end;\r\n  -ms-flex-pack: flex-end;\r\n  justify-content: flex-end;\r\n  text-align: right;\r\n  text-anchor: end\r\n}\r\n\r\n.ct-label.ct-vertical.ct-end {\r\n  -webkit-box-align: flex-end;\r\n  -ms-flex-align: flex-end;\r\n  align-items: flex-end;\r\n  -webkit-box-pack: flex-start;\r\n  -ms-flex-pack: flex-start;\r\n  justify-content: flex-start;\r\n  text-align: left;\r\n  text-anchor: start\r\n}\r\n\r\n.ct-chart-bar .ct-label.ct-horizontal.ct-start {\r\n  -webkit-box-align: flex-end;\r\n  -ms-flex-align: flex-end;\r\n  align-items: flex-end;\r\n  -webkit-box-pack: center;\r\n  -ms-flex-pack: center;\r\n  justify-content: center;\r\n  text-align: center;\r\n  text-anchor: start\r\n}\r\n\r\n.ct-chart-bar .ct-label.ct-horizontal.ct-end {\r\n  -webkit-box-align: flex-start;\r\n  -ms-flex-align: flex-start;\r\n  align-items: flex-start;\r\n  -webkit-box-pack: center;\r\n  -ms-flex-pack: center;\r\n  justify-content: center;\r\n  text-align: center;\r\n  text-anchor: start\r\n}\r\n\r\n.ct-chart-bar.ct-horizontal-bars .ct-label.ct-horizontal.ct-start {\r\n  -webkit-box-align: flex-end;\r\n  -ms-flex-align: flex-end;\r\n  align-items: flex-end;\r\n  -webkit-box-pack: flex-start;\r\n  -ms-flex-pack: flex-start;\r\n  justify-content: flex-start;\r\n  text-align: left;\r\n  text-anchor: start\r\n}\r\n\r\n.ct-chart-bar.ct-horizontal-bars .ct-label.ct-horizontal.ct-end {\r\n  -webkit-box-align: flex-start;\r\n  -ms-flex-align: flex-start;\r\n  align-items: flex-start;\r\n  -webkit-box-pack: flex-start;\r\n  -ms-flex-pack: flex-start;\r\n  justify-content: flex-start;\r\n  text-align: left;\r\n  text-anchor: start\r\n}\r\n\r\n.ct-chart-bar.ct-horizontal-bars .ct-label.ct-vertical.ct-start {\r\n  -webkit-box-align: center;\r\n  -ms-flex-align: center;\r\n  align-items: center;\r\n  -webkit-box-pack: flex-end;\r\n  -ms-flex-pack: flex-end;\r\n  justify-content: flex-end;\r\n  text-align: right;\r\n  text-anchor: end\r\n}\r\n\r\n.ct-chart-bar.ct-horizontal-bars .ct-label.ct-vertical.ct-end {\r\n  -webkit-box-align: center;\r\n  -ms-flex-align: center;\r\n  align-items: center;\r\n  -webkit-box-pack: flex-start;\r\n  -ms-flex-pack: flex-start;\r\n  justify-content: flex-start;\r\n  text-align: left;\r\n  text-anchor: end\r\n}\r\n\r\n.ct-grid {\r\n  stroke: rgba(0, 0, 0, .2);\r\n  stroke-width: 1px;\r\n  stroke-dasharray: 2px\r\n}\r\n\r\n.ct-point {\r\n  stroke-width: 10px;\r\n  stroke-linecap: round\r\n}\r\n\r\n.ct-line {\r\n  stroke-width: 4px\r\n}\r\n\r\n.ct-area {\r\n  stroke: none;\r\n  fill-opacity: .1\r\n}\r\n\r\n.ct-bar {\r\n  fill: none;\r\n  stroke-width: 10px\r\n}\r\n\r\n.ct-slice-donut {\r\n  fill: none;\r\n  stroke-width: 60px\r\n}\r\n\r\n.ct-series-a .ct-bar, .ct-series-a .ct-line, .ct-series-a .ct-point, .ct-series-a .ct-slice-donut {\r\n  stroke: #1ce3bb\r\n}\r\n\r\n.ct-series-a .ct-area, .ct-series-a .ct-slice-donut-solid, .ct-series-a .ct-slice-pie {\r\n  fill: #1ce3bb\r\n}\r\n\r\n.ct-series-b .ct-bar, .ct-series-b .ct-line, .ct-series-b .ct-point, .ct-series-b .ct-slice-donut {\r\n  stroke: rgba(255, 157, 136, 0.62);\r\n}\r\n\r\n.ct-series-b .ct-area, .ct-series-b .ct-slice-donut-solid, .ct-series-b .ct-slice-pie {\r\n  fill: rgba(255, 157, 136, 0.62);\r\n}\r\n\r\n.ct-series-c .ct-bar, .ct-series-c .ct-line, .ct-series-c .ct-point, .ct-series-c .ct-slice-donut {\r\n  stroke: rgba(79, 84, 103, 0.45)\r\n}\r\n\r\n.ct-series-c .ct-area, .ct-series-c .ct-slice-donut-solid, .ct-series-c .ct-slice-pie {\r\n  fill: rgba(79, 84, 103, 0.45)\r\n}\r\n\r\n.ct-series-d .ct-bar, .ct-series-d .ct-line, .ct-series-d .ct-point, .ct-series-d .ct-slice-donut {\r\n  stroke: rgba(129, 142, 219, 0.61) !important;\r\n}\r\n\r\n.ct-series-d .ct-area, .ct-series-d .ct-slice-donut-solid, .ct-series-d .ct-slice-pie {\r\n  fill: rgba(129, 142, 219, 0.61) !important;\r\n}\r\n\r\n.ct-series-e .ct-bar, .ct-series-e .ct-line, .ct-series-e .ct-point, .ct-series-e .ct-slice-donut {\r\n  stroke: #453d3f\r\n}\r\n\r\n.ct-series-e .ct-area, .ct-series-e .ct-slice-donut-solid, .ct-series-e .ct-slice-pie {\r\n  fill: #453d3f\r\n}\r\n\r\n.ct-series-f .ct-bar, .ct-series-f .ct-line, .ct-series-f .ct-point, .ct-series-f .ct-slice-donut {\r\n  stroke: #59922b\r\n}\r\n\r\n.ct-series-f .ct-area, .ct-series-f .ct-slice-donut-solid, .ct-series-f .ct-slice-pie {\r\n  fill: #59922b\r\n}\r\n\r\n.ct-series-g .ct-bar, .ct-series-g .ct-line, .ct-series-g .ct-point, .ct-series-g .ct-slice-donut {\r\n  stroke: #0544d3\r\n}\r\n\r\n.ct-series-g .ct-area, .ct-series-g .ct-slice-donut-solid, .ct-series-g .ct-slice-pie {\r\n  fill: #0544d3\r\n}\r\n\r\n.ct-series-h .ct-bar, .ct-series-h .ct-line, .ct-series-h .ct-point, .ct-series-h .ct-slice-donut {\r\n  stroke: #6b0392\r\n}\r\n\r\n.ct-series-h .ct-area, .ct-series-h .ct-slice-donut-solid, .ct-series-h .ct-slice-pie {\r\n  fill: #6b0392\r\n}\r\n\r\n.ct-series-i .ct-bar, .ct-series-i .ct-line, .ct-series-i .ct-point, .ct-series-i .ct-slice-donut {\r\n  stroke: #f05b4f\r\n}\r\n\r\n.ct-series-i .ct-area, .ct-series-i .ct-slice-donut-solid, .ct-series-i .ct-slice-pie {\r\n  fill: #f05b4f\r\n}\r\n\r\n.ct-series-j .ct-bar, .ct-series-j .ct-line, .ct-series-j .ct-point, .ct-series-j .ct-slice-donut {\r\n  stroke: #dda458\r\n}\r\n\r\n.ct-series-j .ct-area, .ct-series-j .ct-slice-donut-solid, .ct-series-j .ct-slice-pie {\r\n  fill: #dda458\r\n}\r\n\r\n.ct-series-k .ct-bar, .ct-series-k .ct-line, .ct-series-k .ct-point, .ct-series-k .ct-slice-donut {\r\n  stroke: #eacf7d\r\n}\r\n\r\n.ct-series-k .ct-area, .ct-series-k .ct-slice-donut-solid, .ct-series-k .ct-slice-pie {\r\n  fill: #eacf7d\r\n}\r\n\r\n.ct-series-l .ct-bar, .ct-series-l .ct-line, .ct-series-l .ct-point, .ct-series-l .ct-slice-donut {\r\n  stroke: #86797d\r\n}\r\n\r\n.ct-series-l .ct-area, .ct-series-l .ct-slice-donut-solid, .ct-series-l .ct-slice-pie {\r\n  fill: #86797d\r\n}\r\n\r\n.ct-series-m .ct-bar, .ct-series-m .ct-line, .ct-series-m .ct-point, .ct-series-m .ct-slice-donut {\r\n  stroke: #b2c326\r\n}\r\n\r\n.ct-series-m .ct-area, .ct-series-m .ct-slice-donut-solid, .ct-series-m .ct-slice-pie {\r\n  fill: #b2c326\r\n}\r\n\r\n.ct-series-n .ct-bar, .ct-series-n .ct-line, .ct-series-n .ct-point, .ct-series-n .ct-slice-donut {\r\n  stroke: #6188e2\r\n}\r\n\r\n.ct-series-n .ct-area, .ct-series-n .ct-slice-donut-solid, .ct-series-n .ct-slice-pie {\r\n  fill: #6188e2\r\n}\r\n\r\n.ct-series-o .ct-bar, .ct-series-o .ct-line, .ct-series-o .ct-point, .ct-series-o .ct-slice-donut {\r\n  stroke: #a748ca\r\n}\r\n\r\n.ct-series-o .ct-area, .ct-series-o .ct-slice-donut-solid, .ct-series-o .ct-slice-pie {\r\n  fill: #a748ca\r\n}\r\n\r\n.ct-square {\r\n  display: block;\r\n  position: relative;\r\n  width: 100%\r\n}\r\n\r\n.ct-square:before {\r\n  display: block;\r\n  float: left;\r\n  content: \"\";\r\n  width: 0;\r\n  height: 0;\r\n  padding-bottom: 100%\r\n}\r\n\r\n.ct-square:after {\r\n  display: table\r\n}\r\n\r\n.ct-square > svg {\r\n  display: block;\r\n  position: absolute;\r\n  top: 0;\r\n  left: 0\r\n}\r\n\r\n.ct-minor-second {\r\n  display: block;\r\n  position: relative;\r\n  width: 100%\r\n}\r\n\r\n.ct-minor-second:before {\r\n  display: block;\r\n  float: left;\r\n  content: \"\";\r\n  width: 0;\r\n  height: 0;\r\n  padding-bottom: 93.75%\r\n}\r\n\r\n.ct-minor-second:after {\r\n  display: table\r\n}\r\n\r\n.ct-minor-second > svg {\r\n  display: block;\r\n  position: absolute;\r\n  top: 0;\r\n  left: 0\r\n}\r\n\r\n.ct-major-second {\r\n  display: block;\r\n  position: relative;\r\n  width: 100%\r\n}\r\n\r\n.ct-major-second:before {\r\n  display: block;\r\n  float: left;\r\n  content: \"\";\r\n  width: 0;\r\n  height: 0;\r\n  padding-bottom: 88.8888888889%\r\n}\r\n\r\n.ct-major-second:after {\r\n  display: table\r\n}\r\n\r\n.ct-major-second > svg {\r\n  display: block;\r\n  position: absolute;\r\n  top: 0;\r\n  left: 0\r\n}\r\n\r\n.ct-minor-third {\r\n  display: block;\r\n  position: relative;\r\n  width: 100%\r\n}\r\n\r\n.ct-minor-third:before {\r\n  display: block;\r\n  float: left;\r\n  content: \"\";\r\n  width: 0;\r\n  height: 0;\r\n  padding-bottom: 83.3333333333%\r\n}\r\n\r\n.ct-minor-third:after {\r\n  display: table\r\n}\r\n\r\n.ct-minor-third > svg {\r\n  display: block;\r\n  position: absolute;\r\n  top: 0;\r\n  left: 0\r\n}\r\n\r\n.ct-major-third {\r\n  display: block;\r\n  position: relative;\r\n  width: 100%\r\n}\r\n\r\n.ct-major-third:before {\r\n  display: block;\r\n  float: left;\r\n  content: \"\";\r\n  width: 0;\r\n  height: 0;\r\n  padding-bottom: 80%\r\n}\r\n\r\n.ct-major-third:after {\r\n  display: table\r\n}\r\n\r\n.ct-major-third > svg {\r\n  display: block;\r\n  position: absolute;\r\n  top: 0;\r\n  left: 0\r\n}\r\n\r\n.ct-perfect-fourth {\r\n  display: block;\r\n  position: relative;\r\n  width: 100%\r\n}\r\n\r\n.ct-perfect-fourth:before {\r\n  display: block;\r\n  float: left;\r\n  content: \"\";\r\n  width: 0;\r\n  height: 0;\r\n  padding-bottom: 75%\r\n}\r\n\r\n.ct-perfect-fourth:after {\r\n  display: table\r\n}\r\n\r\n.ct-perfect-fourth > svg {\r\n  display: block;\r\n  position: absolute;\r\n  top: 0;\r\n  left: 0\r\n}\r\n\r\n.ct-perfect-fifth {\r\n  display: block;\r\n  position: relative;\r\n  width: 100%\r\n}\r\n\r\n.ct-perfect-fifth:before {\r\n  display: block;\r\n  float: left;\r\n  content: \"\";\r\n  width: 0;\r\n  height: 0;\r\n  padding-bottom: 66.6666666667%\r\n}\r\n\r\n.ct-perfect-fifth:after {\r\n  display: table\r\n}\r\n\r\n.ct-perfect-fifth > svg {\r\n  display: block;\r\n  position: absolute;\r\n  top: 0;\r\n  left: 0\r\n}\r\n\r\n.ct-minor-sixth {\r\n  display: block;\r\n  position: relative;\r\n  width: 100%\r\n}\r\n\r\n.ct-minor-sixth:before {\r\n  display: block;\r\n  float: left;\r\n  content: \"\";\r\n  width: 0;\r\n  height: 0;\r\n  padding-bottom: 62.5%\r\n}\r\n\r\n.ct-minor-sixth:after {\r\n  display: table\r\n}\r\n\r\n.ct-minor-sixth > svg {\r\n  display: block;\r\n  position: absolute;\r\n  top: 0;\r\n  left: 0\r\n}\r\n\r\n.ct-golden-section {\r\n  display: block;\r\n  position: relative;\r\n  width: 100%\r\n}\r\n\r\n.ct-golden-section:before {\r\n  display: block;\r\n  float: left;\r\n  content: \"\";\r\n  width: 0;\r\n  height: 0;\r\n  padding-bottom: 61.804697157%\r\n}\r\n\r\n.ct-golden-section:after {\r\n  content: \"\";\r\n  display: table;\r\n  clear: both\r\n}\r\n\r\n.ct-golden-section > svg {\r\n  display: block;\r\n  position: absolute;\r\n  top: 0;\r\n  left: 0\r\n}\r\n\r\n.ct-major-sixth {\r\n  display: block;\r\n  position: relative;\r\n  width: 100%\r\n}\r\n\r\n.ct-major-sixth:before {\r\n  display: block;\r\n  float: left;\r\n  content: \"\";\r\n  width: 0;\r\n  height: 0;\r\n  padding-bottom: 60%\r\n}\r\n\r\n.ct-major-sixth:after {\r\n  display: table\r\n}\r\n\r\n.ct-major-sixth > svg {\r\n  display: block;\r\n  position: absolute;\r\n  top: 0;\r\n  left: 0\r\n}\r\n\r\n.ct-minor-seventh {\r\n  display: block;\r\n  position: relative;\r\n  width: 100%\r\n}\r\n\r\n.ct-minor-seventh:before {\r\n  display: block;\r\n  float: left;\r\n  content: \"\";\r\n  width: 0;\r\n  height: 0;\r\n  padding-bottom: 56.25%\r\n}\r\n\r\n.ct-minor-seventh:after {\r\n  display: table\r\n}\r\n\r\n.ct-minor-seventh > svg {\r\n  display: block;\r\n  position: absolute;\r\n  top: 0;\r\n  left: 0\r\n}\r\n\r\n.ct-major-seventh {\r\n  display: block;\r\n  position: relative;\r\n  width: 100%\r\n}\r\n\r\n.ct-major-seventh:before {\r\n  display: block;\r\n  float: left;\r\n  content: \"\";\r\n  width: 0;\r\n  height: 0;\r\n  padding-bottom: 53.3333333333%\r\n}\r\n\r\n.ct-major-seventh:after {\r\n  display: table\r\n}\r\n\r\n.ct-major-seventh > svg {\r\n  display: block;\r\n  position: absolute;\r\n  top: 0;\r\n  left: 0\r\n}\r\n\r\n.ct-octave {\r\n  display: block;\r\n  position: relative;\r\n  width: 100%\r\n}\r\n\r\n.ct-octave:before {\r\n  display: block;\r\n  float: left;\r\n  content: \"\";\r\n  width: 0;\r\n  height: 0;\r\n  padding-bottom: 50%\r\n}\r\n\r\n.ct-octave:after {\r\n  display: table\r\n}\r\n\r\n.ct-octave > svg {\r\n  display: block;\r\n  position: absolute;\r\n  top: 0;\r\n  left: 0\r\n}\r\n\r\n.ct-major-tenth {\r\n  display: block;\r\n  position: relative;\r\n  width: 100%\r\n}\r\n\r\n.ct-major-tenth:before {\r\n  display: block;\r\n  float: left;\r\n  content: \"\";\r\n  width: 0;\r\n  height: 0;\r\n  padding-bottom: 40%\r\n}\r\n\r\n.ct-major-tenth:after {\r\n  display: table\r\n}\r\n\r\n.ct-major-tenth > svg {\r\n  display: block;\r\n  position: absolute;\r\n  top: 0;\r\n  left: 0\r\n}\r\n\r\n.ct-major-eleventh {\r\n  display: block;\r\n  position: relative;\r\n  width: 100%\r\n}\r\n\r\n.ct-major-eleventh:before {\r\n  display: block;\r\n  float: left;\r\n  content: \"\";\r\n  width: 0;\r\n  height: 0;\r\n  padding-bottom: 37.5%\r\n}\r\n\r\n.ct-major-eleventh:after {\r\n  display: table\r\n}\r\n\r\n.ct-major-eleventh > svg {\r\n  display: block;\r\n  position: absolute;\r\n  top: 0;\r\n  left: 0\r\n}\r\n\r\n.ct-major-twelfth {\r\n  display: block;\r\n  position: relative;\r\n  width: 100%\r\n}\r\n\r\n.ct-major-twelfth:before {\r\n  display: block;\r\n  float: left;\r\n  content: \"\";\r\n  width: 0;\r\n  height: 0;\r\n  padding-bottom: 33.3333333333%\r\n}\r\n\r\n.ct-major-twelfth:after {\r\n  display: table\r\n}\r\n\r\n.ct-major-twelfth > svg {\r\n  display: block;\r\n  position: absolute;\r\n  top: 0;\r\n  left: 0\r\n}\r\n\r\n.ct-double-octave {\r\n  display: block;\r\n  position: relative;\r\n  width: 100%\r\n}\r\n\r\n.ct-double-octave:before {\r\n  display: block;\r\n  float: left;\r\n  content: \"\";\r\n  width: 0;\r\n  height: 0;\r\n  padding-bottom: 25%\r\n}\r\n\r\n.ct-double-octave:after {\r\n  display: table\r\n}\r\n\r\n.ct-double-octave > svg {\r\n  display: block;\r\n  position: absolute;\r\n  top: 0;\r\n  left: 0\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/icon/SVG-animated/svg-weather.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*https://codepen.io/noahblon/pen/lxukH*/\r\n\r\n/*General classes*/\r\n.climacon_component-stroke {\r\n  fill: #1abc9c;\r\n  stroke-width: 0%;\r\n  stroke: black;\r\n}\r\n\r\n.climacon_component-fill {\r\n  fill: #28E1BD;\r\n  stroke-width: 0%;\r\n  stroke: black;\r\n}\r\n.climacon {\r\n  display: inline-block;\r\n  width: 50px;\r\n  height: 50px;\r\n  shape-rendering: geometricPrecision;\r\n  vertical-align: middle;  \r\n}\r\n\r\ng, path, circle, rect {\r\n  -webkit-animation-iteration-count: infinite;\r\n          animation-iteration-count: infinite;\r\n  -webkit-animation-timing-function: linear;\r\n          animation-timing-function: linear;\r\n  -webkit-transform-origin: 50% 50%;\r\n          transform-origin: 50% 50%;\r\n  -webkit-animation-duration: 18s;\r\n          animation-duration: 18s;\r\n  -webkit-animation-direction: normal;\r\n          animation-direction: normal;\r\n}\r\n\r\n\r\n\r\n/* SUN */\r\n.climacon_componentWrap-sun {\r\n  -webkit-animation-name: rotate;\r\n          animation-name: rotate;\r\n}\r\n\r\n.climacon_componentWrap_sunSpoke .climacon_component-stroke_sunSpoke {\r\n  -webkit-animation-name: scale;\r\n          animation-name: scale;\r\n  -webkit-animation-direction: alternate;\r\n          animation-direction: alternate;\r\n  -webkit-animation-duration: 4.5s;\r\n          animation-duration: 4.5s;\r\n}\r\n\r\n.climacon_componentWrap_sunSpoke .climacon_component-stroke_sunSpoke:nth-child(even) {\r\n  -webkit-animation-delay: 4.5s;\r\n          animation-delay: 4.5s;\r\n}\r\n\r\n/* MOON */\r\n.climacon_componentWrap-moon {\r\n  -webkit-animation-name: partialRotate;\r\n          animation-name: partialRotate;\r\n  -webkit-animation-duration: 18s;\r\n          animation-duration: 18s;\r\n  -webkit-animation-direction: alternate;\r\n          animation-direction: alternate;\r\n}\r\n\r\n/* WIND */\r\n.climacon_componentWrap-wind {\r\n  -webkit-animation-name: translateWind;\r\n          animation-name: translateWind;\r\n  -webkit-animation-duration: 6s;\r\n          animation-duration: 6s;\r\n  -webkit-animation-direction: alternate;\r\n          animation-direction: alternate;\r\n  -webkit-animation-timing-function: ease-in-out;\r\n          animation-timing-function: ease-in-out;\r\n}\r\n\r\n/* SNOWFLAKE */\r\n.climacon_componentWrap-snowflake {\r\n  -webkit-animation-name: rotate;\r\n          animation-name: rotate;\r\n  -webkit-animation-duration: 54s;\r\n          animation-duration: 54s;\r\n  -webkit-animation-direction: normal;\r\n          animation-direction: normal;\r\n}\r\n\r\n/* CLOUD SUN */\r\n.climacon_componentWrap-sun_cloud {\r\n  -webkit-animation-name: behindCloudMove, rotate;\r\n          animation-name: behindCloudMove, rotate;\r\n  -webkit-animation-iteration-count: 1, infinite;\r\n          animation-iteration-count: 1, infinite;\r\n  -webkit-animation-timing-function: ease-out, linear;\r\n          animation-timing-function: ease-out, linear;\r\n  -webkit-animation-delay: 0, 4.5s;\r\n          animation-delay: 0, 4.5s;\r\n  -webkit-animation-duration: 4.5s, 18s;\r\n          animation-duration: 4.5s, 18s;\r\n}\r\n\r\n.climacon_componentWrap-sun_cloud .climacon_componentWrap_sunSpoke .climacon_component-stroke_sunSpoke {\r\n  fill-opacity: 0;\r\n  -webkit-animation-name: fillOpacity, scale;\r\n          animation-name: fillOpacity, scale;\r\n  -webkit-animation-iteration-count: 1, infinite;\r\n          animation-iteration-count: 1, infinite;\r\n  -webkit-animation-delay: 4.5s, 0;\r\n          animation-delay: 4.5s, 0;\r\n  -webkit-animation-fill-mode: both;\r\n          animation-fill-mode: both;\r\n}\r\n\r\n.climacon_componentWrap-sun_cloud .climacon_componentWrap_sunSpoke .climacon_component-stroke_sunSpoke:nth-child(even) {\r\n  -webkit-animation-delay: 4.5s;\r\n          animation-delay: 4.5s;\r\n}\r\n\r\n/* CLOUD MOON */\r\n.climacon_componentWrap-moon_cloud {\r\n  -webkit-animation-name: behindCloudMove, partialRotate;\r\n          animation-name: behindCloudMove, partialRotate;\r\n  -webkit-animation-iteration-count: 1, infinite;\r\n          animation-iteration-count: 1, infinite;\r\n  -webkit-animation-timing-function: ease-out, linear;\r\n          animation-timing-function: ease-out, linear;\r\n  -webkit-animation-delay: 0, 4.5s;\r\n          animation-delay: 0, 4.5s;\r\n  -webkit-animation-duration: 4.5s, 18s;\r\n          animation-duration: 4.5s, 18s;\r\n}\r\n\r\n/* DRIZZLE */\r\n.climacon_component-stroke_drizzle {\r\n  fill-opacity: 0;\r\n  -webkit-animation-name: drizzleFall, fillOpacity2;\r\n          animation-name: drizzleFall, fillOpacity2;\r\n  -webkit-animation-timing-function: ease-in;\r\n          animation-timing-function: ease-in;\r\n  -webkit-animation-duration: 1.5s;\r\n          animation-duration: 1.5s;\r\n}\r\n\r\n.climacon_component-stroke_drizzle:nth-child(1) {\r\n  -webkit-animation-delay: 0s;\r\n          animation-delay: 0s;\r\n}\r\n\r\n.climacon_component-stroke_drizzle:nth-child(2) {\r\n  -webkit-animation-delay: 0.9s;\r\n          animation-delay: 0.9s;\r\n}\r\n\r\n.climacon_component-stroke_drizzle:nth-child(3) {\r\n  -webkit-animation-delay: 1.8s;\r\n          animation-delay: 1.8s;\r\n}\r\n\r\n/* RAIN */\r\n.climacon_component-stroke_rain {\r\n  fill-opacity: 0;\r\n  -webkit-animation-name: rainFall, fillOpacity2;\r\n          animation-name: rainFall, fillOpacity2;\r\n  -webkit-animation-timing-function: ease-in;\r\n          animation-timing-function: ease-in;\r\n  -webkit-animation-duration: 3s;\r\n          animation-duration: 3s;\r\n}\r\n\r\n.climacon_component-stroke_rain:nth-child(n+4) {\r\n  -webkit-animation-delay: 1.5s;\r\n          animation-delay: 1.5s;\r\n}\r\n\r\n.climacon_component-stroke_rain_alt:nth-child(2) {\r\n  -webkit-animation-delay: 1.5s;\r\n          animation-delay: 1.5s;\r\n}\r\n\r\n/* HAIL */\r\n/* HAIL ALT */\r\n.climacon_component-stroke_hailAlt {\r\n  fill-opacity: 1;\r\n  -webkit-animation-timing-function: ease-in;\r\n          animation-timing-function: ease-in;\r\n  -webkit-animation-duration: 1s;\r\n          animation-duration: 1s;\r\n}\r\n\r\n.climacon_component-stroke_hailAlt-left {\r\n  -webkit-animation-name: hailLeft, fillOpacity2;\r\n          animation-name: hailLeft, fillOpacity2;\r\n}\r\n\r\n.climacon_component-stroke_hailAlt-middle {\r\n  -webkit-animation-name: hailMiddle, fillOpacity2;\r\n          animation-name: hailMiddle, fillOpacity2;\r\n}\r\n\r\n.climacon_component-stroke_hailAlt-middle:nth-child(2) {\r\n  -webkit-animation-name: hailMiddle2, fillOpacity2;\r\n          animation-name: hailMiddle2, fillOpacity2;\r\n}\r\n\r\n.climacon_component-stroke_hailAlt-right {\r\n  -webkit-animation-name: hailRight, fillOpacity2;\r\n          animation-name: hailRight, fillOpacity2;\r\n}\r\n\r\n.climacon_component-stroke_hailAlt:nth-child(1) {\r\n  -webkit-animation-delay: 0s;\r\n          animation-delay: 0s;\r\n}\r\n\r\n.climacon_component-stroke_hailAlt:nth-child(2) {\r\n  -webkit-animation-delay: 0.16667s;\r\n          animation-delay: 0.16667s;\r\n}\r\n\r\n.climacon_component-stroke_hailAlt:nth-child(3) {\r\n  -webkit-animation-delay: 0.33333s;\r\n          animation-delay: 0.33333s;\r\n}\r\n\r\n.climacon_component-stroke_hailAlt:nth-child(4) {\r\n  -webkit-animation-delay: 0.5s;\r\n          animation-delay: 0.5s;\r\n}\r\n\r\n.climacon_component-stroke_hailAlt:nth-child(5) {\r\n  -webkit-animation-delay: 0.66667s;\r\n          animation-delay: 0.66667s;\r\n}\r\n\r\n.climacon_component-stroke_hailAlt:nth-child(6) {\r\n  -webkit-animation-delay: 0.83333s;\r\n          animation-delay: 0.83333s;\r\n}\r\n\r\n/* SNOW */\r\n.climacon_component-stroke_snow {\r\n  fill-opacity: 0;\r\n  -webkit-animation-name: snowFall, fillOpacity2;\r\n          animation-name: snowFall, fillOpacity2;\r\n  -webkit-animation-timing-function: ease-in-out;\r\n          animation-timing-function: ease-in-out;\r\n  -webkit-animation-duration: 9s;\r\n          animation-duration: 9s;\r\n}\r\n\r\n.climacon_component-stroke_snow:nth-child(3) {\r\n  -webkit-animation-name: snowFall2, fillOpacity2;\r\n          animation-name: snowFall2, fillOpacity2;\r\n}\r\n\r\n.climacon_component-stroke_snow:nth-child(1) {\r\n  -webkit-animation-delay: 0s;\r\n          animation-delay: 0s;\r\n}\r\n\r\n.climacon_component-stroke_snow:nth-child(2) {\r\n  -webkit-animation-delay: 3s;\r\n          animation-delay: 3s;\r\n}\r\n\r\n.climacon_component-stroke_snow:nth-child(3) {\r\n  -webkit-animation-delay: 6s;\r\n          animation-delay: 6s;\r\n}\r\n\r\n/* SNOW ALT */\r\n.climacon_wrapperComponent-snowAlt {\r\n  fill-opacity: 0;\r\n  -webkit-animation-name: snowFall, fillOpacity2;\r\n          animation-name: snowFall, fillOpacity2;\r\n  -webkit-animation-timing-function: ease-in-out;\r\n          animation-timing-function: ease-in-out;\r\n  -webkit-animation-duration: 9s;\r\n          animation-duration: 9s;\r\n}\r\n\r\n/* FOG */\r\n.climacon_component-stroke_fogLine {\r\n  fill-opacity: 0.5;\r\n  -webkit-animation-name: translateFog, fillOpacityFog;\r\n          animation-name: translateFog, fillOpacityFog;\r\n  -webkit-animation-iteration-count: infinite;\r\n          animation-iteration-count: infinite;\r\n  -webkit-animation-direction: normal;\r\n          animation-direction: normal;\r\n  -webkit-animation-timing-function: ease-in;\r\n          animation-timing-function: ease-in;\r\n  -webkit-animation-duration: 18s;\r\n          animation-duration: 18s;\r\n}\r\n\r\n.climacon_component-stroke_fogLine:nth-child(even) {\r\n  -webkit-animation-delay: 9s;\r\n          animation-delay: 9s;\r\n}\r\n\r\n/* LIGHTNING */\r\n.climacon_component-stroke_lightning {\r\n  fill-opacity: 0;\r\n  -webkit-animation-name: fillOpacityLightning;\r\n          animation-name: fillOpacityLightning;\r\n  -webkit-animation-iteration-count: infinite;\r\n          animation-iteration-count: infinite;\r\n  -webkit-animation-direction: normal;\r\n          animation-direction: normal;\r\n  -webkit-animation-timing-function: ease-out;\r\n          animation-timing-function: ease-out;\r\n  -webkit-animation-duration: 18s;\r\n          animation-duration: 18s;\r\n}\r\n\r\n/* TORNADO */\r\n.climacon_component-stroke_tornadoLine {\r\n  -webkit-animation-name: translateTornado1;\r\n          animation-name: translateTornado1;\r\n  -webkit-animation-iteration-count: infinite;\r\n          animation-iteration-count: infinite;\r\n  -webkit-animation-direction: alternate;\r\n          animation-direction: alternate;\r\n  -webkit-animation-timing-function: ease-in-out;\r\n          animation-timing-function: ease-in-out;\r\n  -webkit-animation-duration: 18s;\r\n          animation-duration: 18s;\r\n}\r\n\r\n.climacon_component-stroke_tornadoLine:nth-child(1) {\r\n  -webkit-animation-name: translateTornado1;\r\n          animation-name: translateTornado1;\r\n}\r\n\r\n.climacon_component-stroke_tornadoLine:nth-child(2) {\r\n  -webkit-animation-name: translateTornado2;\r\n          animation-name: translateTornado2;\r\n}\r\n\r\n.climacon_component-stroke_tornadoLine:nth-child(3) {\r\n  -webkit-animation-name: translateTornado3;\r\n          animation-name: translateTornado3;\r\n}\r\n\r\n.climacon_component-stroke_tornadoLine:nth-child(4) {\r\n  -webkit-animation-name: translateTornado4;\r\n          animation-name: translateTornado4;\r\n}\r\n\r\n.climacon_component-stroke_tornadoLine:nth-child(5) {\r\n  -webkit-animation-name: translateTornado5;\r\n          animation-name: translateTornado5;\r\n}\r\n\r\n.climacon_component-stroke_tornadoLine:nth-child(6) {\r\n  -webkit-animation-name: translateTornado6;\r\n          animation-name: translateTornado6;\r\n}\r\n\r\n.climacon_componentWrap-sunsetAlt {\r\n  -webkit-animation-name: translateSunset;\r\n          animation-name: translateSunset;\r\n  -webkit-animation-duration: 4.5s;\r\n          animation-duration: 4.5s;\r\n  -webkit-animation-iteration-count: 1;\r\n          animation-iteration-count: 1;\r\n  -webkit-animation-timing-function: ease-out;\r\n          animation-timing-function: ease-out;\r\n}\r\n\r\n.climacon_componentWrap-sunsetAlt {\r\n  -webkit-animation-name: translateSunset;\r\n          animation-name: translateSunset;\r\n  -webkit-animation-iteration-count: 1;\r\n          animation-iteration-count: 1;\r\n}\r\n\r\n.climacon_iconWrap-sun .climacon_component-stroke_sunSpoke, .climacon_iconWrap-sunFill .climacon_component-stroke_sunSpoke {\r\n  fill-opacity: 1;\r\n  -webkit-animation-name: scale;\r\n          animation-name: scale;\r\n  -webkit-animation-duration: 3s;\r\n          animation-duration: 3s;\r\n  -webkit-animation-iteration-count: infinite;\r\n          animation-iteration-count: infinite;\r\n  -webkit-animation-delay: 0;\r\n          animation-delay: 0;\r\n  -webkit-animation-fill-mode: both;\r\n          animation-fill-mode: both;\r\n  -webkit-animation-direction: alternate;\r\n          animation-direction: alternate;\r\n}\r\n\r\n.climacon_iconWrap-sun .climacon_component-stroke_sunSpoke:nth-child(even), .climacon_iconWrap-sunFill .climacon_component-stroke_sunSpoke:nth-child(even) {\r\n  -webkit-animation-delay: 4.5s;\r\n          animation-delay: 4.5s;\r\n}\r\n\r\n.climacon-iconWrap_sunFill .climacon_component-stroke_sunSpoke {\r\n  fill-opacity: 1;\r\n  -webkit-animation-name: scale;\r\n          animation-name: scale;\r\n  -webkit-animation-iteration-count: infinite;\r\n          animation-iteration-count: infinite;\r\n  -webkit-animation-delay: 0;\r\n          animation-delay: 0;\r\n  -webkit-animation-fill-mode: both;\r\n          animation-fill-mode: both;\r\n  -webkit-animation-direction: alternate;\r\n          animation-direction: alternate;\r\n}\r\n\r\n.climacon-iconWrap_sunFill .climacon_component-stroke_sunSpoke:nth-child(even) {\r\n  -webkit-animation-delay: 4.5s;\r\n          animation-delay: 4.5s;\r\n}\r\n\r\n.climacon_component-stroke_arrow-up {\r\n  fill-opacity: 0;\r\n  -webkit-animation-name: fillOpacity2, translateArrowUp;\r\n          animation-name: fillOpacity2, translateArrowUp;\r\n  -webkit-animation-duration: 4.5s;\r\n          animation-duration: 4.5s;\r\n}\r\n\r\n.climacon_component-stroke_arrow-down {\r\n  fill-opacity: 0;\r\n  -webkit-animation-name: fillOpacity2, translateArrowDown;\r\n          animation-name: fillOpacity2, translateArrowDown;\r\n  -webkit-animation-duration: 4.5s;\r\n          animation-duration: 4.5s;\r\n}\r\n\r\n.climacon_componentWrap-sunrise .climacon_component-stroke_sunSpoke, .climacon_componentWrap-sunset .climacon_component-stroke_sunSpoke {\r\n  -webkit-animation-name: scale;\r\n          animation-name: scale;\r\n  -webkit-animation-direction: alternate;\r\n          animation-direction: alternate;\r\n  -webkit-animation-iteration-count: infinite;\r\n          animation-iteration-count: infinite;\r\n  -webkit-animation-duration: 4.5s;\r\n          animation-duration: 4.5s;\r\n  -webkit-animation-delay: 0;\r\n          animation-delay: 0;\r\n  -webkit-animation-fill-mode: both;\r\n          animation-fill-mode: both;\r\n}\r\n\r\n.climacon_componentWrap-sunrise .climacon_component-stroke_sunSpoke:nth-child(even), .climacon_componentWrap-sunset .climacon_component-stroke_sunSpoke:nth-child(even) {\r\n  -webkit-animation-delay: 4.5s;\r\n          animation-delay: 4.5s;\r\n}\r\n\r\n.climacon_componentWrap-sunriseAlt {\r\n  -webkit-animation-name: translateSunrise, fillOpacity;\r\n          animation-name: translateSunrise, fillOpacity;\r\n  -webkit-animation-duration: 18s, 9s;\r\n          animation-duration: 18s, 9s;\r\n  -webkit-animation-direction: normal;\r\n          animation-direction: normal;\r\n  -webkit-animation-iteration-count: 1;\r\n          animation-iteration-count: 1;\r\n  -webkit-animation-fill-mode: forwards;\r\n          animation-fill-mode: forwards;\r\n}\r\n\r\n.climacon_componentWrap-sunriseAlt .climacon_component-stroke_sunSpoke {\r\n  fill-opacity: 0;\r\n  -webkit-animation-name: fillOpacity, scale;\r\n          animation-name: fillOpacity, scale;\r\n  -webkit-animation-direction: normal, alternate;\r\n          animation-direction: normal, alternate;\r\n  -webkit-animation-iteration-count: 1, infinite;\r\n          animation-iteration-count: 1, infinite;\r\n  -webkit-animation-duration: 4.5s;\r\n          animation-duration: 4.5s;\r\n  -webkit-animation-delay: 4.5s, 0;\r\n          animation-delay: 4.5s, 0;\r\n  -webkit-animation-fill-mode: both;\r\n          animation-fill-mode: both;\r\n}\r\n\r\n.climacon_componentWrap-sunriseAlt .climacon_component-stroke_sunSpoke:nth-child(even) {\r\n  -webkit-animation-delay: 4.5s, 4.5s;\r\n          animation-delay: 4.5s, 4.5s;\r\n}\r\n\r\n.climacon_componentWrap-sunsetAlt {\r\n  -webkit-animation-name: translateSunset;\r\n          animation-name: translateSunset;\r\n  -webkit-animation-delay: 0;\r\n          animation-delay: 0;\r\n  -webkit-animation-duration: 18s;\r\n          animation-duration: 18s;\r\n  -webkit-animation-direction: normal;\r\n          animation-direction: normal;\r\n  -webkit-animation-iteration-count: 1;\r\n          animation-iteration-count: 1;\r\n  -webkit-animation-fill-mode: forwards;\r\n          animation-fill-mode: forwards;\r\n}\r\n\r\n/* ANIMATIONS */\r\n@-webkit-keyframes rotate {\r\n  0% {\r\n    -webkit-transform: rotate(0);\r\n            transform: rotate(0);\r\n  }\r\n  100% {\r\n    -webkit-transform: rotate(360deg);\r\n            transform: rotate(360deg);\r\n  }\r\n}\r\n@keyframes rotate {\r\n  0% {\r\n    -webkit-transform: rotate(0);\r\n            transform: rotate(0);\r\n  }\r\n  100% {\r\n    -webkit-transform: rotate(360deg);\r\n            transform: rotate(360deg);\r\n  }\r\n}\r\n@-webkit-keyframes partialRotate {\r\n  0% {\r\n    -webkit-transform: rotate(0);\r\n            transform: rotate(0);\r\n  }\r\n  25% {\r\n    -webkit-transform: rotate(-15deg);\r\n            transform: rotate(-15deg);\r\n  }\r\n  50% {\r\n    -webkit-transform: rotate(0);\r\n            transform: rotate(0);\r\n  }\r\n  75% {\r\n    -webkit-transform: rotate(15deg);\r\n            transform: rotate(15deg);\r\n  }\r\n  100% {\r\n    -webkit-transform: rotate(0deg);\r\n            transform: rotate(0deg);\r\n  }\r\n}\r\n@keyframes partialRotate {\r\n  0% {\r\n    -webkit-transform: rotate(0);\r\n            transform: rotate(0);\r\n  }\r\n  25% {\r\n    -webkit-transform: rotate(-15deg);\r\n            transform: rotate(-15deg);\r\n  }\r\n  50% {\r\n    -webkit-transform: rotate(0);\r\n            transform: rotate(0);\r\n  }\r\n  75% {\r\n    -webkit-transform: rotate(15deg);\r\n            transform: rotate(15deg);\r\n  }\r\n  100% {\r\n    -webkit-transform: rotate(0deg);\r\n            transform: rotate(0deg);\r\n  }\r\n}\r\n@-webkit-keyframes scale {\r\n  0% {\r\n    -webkit-transform: scale(1, 1);\r\n            transform: scale(1, 1);\r\n  }\r\n  100% {\r\n    -webkit-transform: scale(0.5, 0.5);\r\n            transform: scale(0.5, 0.5);\r\n  }\r\n}\r\n@keyframes scale {\r\n  0% {\r\n    -webkit-transform: scale(1, 1);\r\n            transform: scale(1, 1);\r\n  }\r\n  100% {\r\n    -webkit-transform: scale(0.5, 0.5);\r\n            transform: scale(0.5, 0.5);\r\n  }\r\n}\r\n@-webkit-keyframes behindCloudMove {\r\n  0% {\r\n    -webkit-transform: translateX(-1.75px) translateY(1.75px);\r\n            transform: translateX(-1.75px) translateY(1.75px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateX(0) translateY(0);\r\n            transform: translateX(0) translateY(0);\r\n  }\r\n}\r\n@keyframes behindCloudMove {\r\n  0% {\r\n    -webkit-transform: translateX(-1.75px) translateY(1.75px);\r\n            transform: translateX(-1.75px) translateY(1.75px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateX(0) translateY(0);\r\n            transform: translateX(0) translateY(0);\r\n  }\r\n}\r\n@-webkit-keyframes drizzleFall {\r\n  0% {\r\n    -webkit-transform: translateY(0);\r\n            transform: translateY(0);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateY(21px);\r\n            transform: translateY(21px);\r\n  }\r\n}\r\n@keyframes drizzleFall {\r\n  0% {\r\n    -webkit-transform: translateY(0);\r\n            transform: translateY(0);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateY(21px);\r\n            transform: translateY(21px);\r\n  }\r\n}\r\n@-webkit-keyframes rainFall {\r\n  0% {\r\n    -webkit-transform: translateY(0);\r\n            transform: translateY(0);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateY(14px);\r\n            transform: translateY(14px);\r\n  }\r\n}\r\n@keyframes rainFall {\r\n  0% {\r\n    -webkit-transform: translateY(0);\r\n            transform: translateY(0);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateY(14px);\r\n            transform: translateY(14px);\r\n  }\r\n}\r\n@-webkit-keyframes rainFall2 {\r\n  0% {\r\n    -webkit-transform: translateY(0);\r\n            transform: translateY(0);\r\n  }\r\n  50% {\r\n    -webkit-transform: translateY(14px);\r\n            transform: translateY(14px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateX(14px) translateY(14px);\r\n            transform: translateX(14px) translateY(14px);\r\n  }\r\n}\r\n@keyframes rainFall2 {\r\n  0% {\r\n    -webkit-transform: translateY(0);\r\n            transform: translateY(0);\r\n  }\r\n  50% {\r\n    -webkit-transform: translateY(14px);\r\n            transform: translateY(14px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateX(14px) translateY(14px);\r\n            transform: translateX(14px) translateY(14px);\r\n  }\r\n}\r\n@-webkit-keyframes hailLeft {\r\n  50% {\r\n    -webkit-transform: translateY(17.5px);\r\n            transform: translateY(17.5px);\r\n  }\r\n  51% {\r\n    -webkit-transform: translateY(17.5px) translateX(-0.3px);\r\n            transform: translateY(17.5px) translateX(-0.3px);\r\n  }\r\n  53% {\r\n    -webkit-transform: translateY(16.40333px) translateX(-0.9px);\r\n            transform: translateY(16.40333px) translateX(-0.9px);\r\n  }\r\n  55% {\r\n    -webkit-transform: translateY(15.32396px) translateX(-1.5px);\r\n            transform: translateY(15.32396px) translateX(-1.5px);\r\n  }\r\n  57% {\r\n    -webkit-transform: translateY(14.27891px) translateX(-2.1px);\r\n            transform: translateY(14.27891px) translateX(-2.1px);\r\n  }\r\n  59% {\r\n    -webkit-transform: translateY(13.28466px) translateX(-2.7px);\r\n            transform: translateY(13.28466px) translateX(-2.7px);\r\n  }\r\n  61% {\r\n    -webkit-transform: translateY(12.35688px) translateX(-3.3px);\r\n            transform: translateY(12.35688px) translateX(-3.3px);\r\n  }\r\n  63% {\r\n    -webkit-transform: translateY(11.51021px) translateX(-3.9px);\r\n            transform: translateY(11.51021px) translateX(-3.9px);\r\n  }\r\n  65% {\r\n    -webkit-transform: translateY(10.75801px) translateX(-4.5px);\r\n            transform: translateY(10.75801px) translateX(-4.5px);\r\n  }\r\n  67% {\r\n    -webkit-transform: translateY(10.11213px) translateX(-5.1px);\r\n            transform: translateY(10.11213px) translateX(-5.1px);\r\n  }\r\n  69% {\r\n    -webkit-transform: translateY(9.58276px) translateX(-5.7px);\r\n            transform: translateY(9.58276px) translateX(-5.7px);\r\n  }\r\n  71% {\r\n    -webkit-transform: translateY(9.17826px) translateX(-6.3px);\r\n            transform: translateY(9.17826px) translateX(-6.3px);\r\n  }\r\n  73% {\r\n    -webkit-transform: translateY(8.90499px) translateX(-6.9px);\r\n            transform: translateY(8.90499px) translateX(-6.9px);\r\n  }\r\n  75% {\r\n    -webkit-transform: translateY(8.76727px) translateX(-7.5px);\r\n            transform: translateY(8.76727px) translateX(-7.5px);\r\n  }\r\n  77% {\r\n    -webkit-transform: translateY(8.76727px) translateX(-8.1px);\r\n            transform: translateY(8.76727px) translateX(-8.1px);\r\n  }\r\n  79% {\r\n    -webkit-transform: translateY(8.90499px) translateX(-8.7px);\r\n            transform: translateY(8.90499px) translateX(-8.7px);\r\n  }\r\n  81% {\r\n    -webkit-transform: translateY(9.17826px) translateX(-9.3px);\r\n            transform: translateY(9.17826px) translateX(-9.3px);\r\n  }\r\n  83% {\r\n    -webkit-transform: translateY(9.58276px) translateX(-9.9px);\r\n            transform: translateY(9.58276px) translateX(-9.9px);\r\n  }\r\n  85% {\r\n    -webkit-transform: translateY(10.11213px) translateX(-10.5px);\r\n            transform: translateY(10.11213px) translateX(-10.5px);\r\n  }\r\n  87% {\r\n    -webkit-transform: translateY(10.75801px) translateX(-11.1px);\r\n            transform: translateY(10.75801px) translateX(-11.1px);\r\n  }\r\n  89% {\r\n    -webkit-transform: translateY(11.51021px) translateX(-11.7px);\r\n            transform: translateY(11.51021px) translateX(-11.7px);\r\n  }\r\n  91% {\r\n    -webkit-transform: translateY(12.35688px) translateX(-12.3px);\r\n            transform: translateY(12.35688px) translateX(-12.3px);\r\n  }\r\n  93% {\r\n    -webkit-transform: translateY(13.28466px) translateX(-12.9px);\r\n            transform: translateY(13.28466px) translateX(-12.9px);\r\n  }\r\n  95% {\r\n    -webkit-transform: translateY(14.27891px) translateX(-13.5px);\r\n            transform: translateY(14.27891px) translateX(-13.5px);\r\n  }\r\n  97% {\r\n    -webkit-transform: translateY(15.32396px) translateX(-14.1px);\r\n            transform: translateY(15.32396px) translateX(-14.1px);\r\n  }\r\n  99% {\r\n    -webkit-transform: translateY(16.40333px) translateX(-14.7px);\r\n            transform: translateY(16.40333px) translateX(-14.7px);\r\n  }\r\n}\r\n@keyframes hailLeft {\r\n  50% {\r\n    -webkit-transform: translateY(17.5px);\r\n            transform: translateY(17.5px);\r\n  }\r\n  51% {\r\n    -webkit-transform: translateY(17.5px) translateX(-0.3px);\r\n            transform: translateY(17.5px) translateX(-0.3px);\r\n  }\r\n  53% {\r\n    -webkit-transform: translateY(16.40333px) translateX(-0.9px);\r\n            transform: translateY(16.40333px) translateX(-0.9px);\r\n  }\r\n  55% {\r\n    -webkit-transform: translateY(15.32396px) translateX(-1.5px);\r\n            transform: translateY(15.32396px) translateX(-1.5px);\r\n  }\r\n  57% {\r\n    -webkit-transform: translateY(14.27891px) translateX(-2.1px);\r\n            transform: translateY(14.27891px) translateX(-2.1px);\r\n  }\r\n  59% {\r\n    -webkit-transform: translateY(13.28466px) translateX(-2.7px);\r\n            transform: translateY(13.28466px) translateX(-2.7px);\r\n  }\r\n  61% {\r\n    -webkit-transform: translateY(12.35688px) translateX(-3.3px);\r\n            transform: translateY(12.35688px) translateX(-3.3px);\r\n  }\r\n  63% {\r\n    -webkit-transform: translateY(11.51021px) translateX(-3.9px);\r\n            transform: translateY(11.51021px) translateX(-3.9px);\r\n  }\r\n  65% {\r\n    -webkit-transform: translateY(10.75801px) translateX(-4.5px);\r\n            transform: translateY(10.75801px) translateX(-4.5px);\r\n  }\r\n  67% {\r\n    -webkit-transform: translateY(10.11213px) translateX(-5.1px);\r\n            transform: translateY(10.11213px) translateX(-5.1px);\r\n  }\r\n  69% {\r\n    -webkit-transform: translateY(9.58276px) translateX(-5.7px);\r\n            transform: translateY(9.58276px) translateX(-5.7px);\r\n  }\r\n  71% {\r\n    -webkit-transform: translateY(9.17826px) translateX(-6.3px);\r\n            transform: translateY(9.17826px) translateX(-6.3px);\r\n  }\r\n  73% {\r\n    -webkit-transform: translateY(8.90499px) translateX(-6.9px);\r\n            transform: translateY(8.90499px) translateX(-6.9px);\r\n  }\r\n  75% {\r\n    -webkit-transform: translateY(8.76727px) translateX(-7.5px);\r\n            transform: translateY(8.76727px) translateX(-7.5px);\r\n  }\r\n  77% {\r\n    -webkit-transform: translateY(8.76727px) translateX(-8.1px);\r\n            transform: translateY(8.76727px) translateX(-8.1px);\r\n  }\r\n  79% {\r\n    -webkit-transform: translateY(8.90499px) translateX(-8.7px);\r\n            transform: translateY(8.90499px) translateX(-8.7px);\r\n  }\r\n  81% {\r\n    -webkit-transform: translateY(9.17826px) translateX(-9.3px);\r\n            transform: translateY(9.17826px) translateX(-9.3px);\r\n  }\r\n  83% {\r\n    -webkit-transform: translateY(9.58276px) translateX(-9.9px);\r\n            transform: translateY(9.58276px) translateX(-9.9px);\r\n  }\r\n  85% {\r\n    -webkit-transform: translateY(10.11213px) translateX(-10.5px);\r\n            transform: translateY(10.11213px) translateX(-10.5px);\r\n  }\r\n  87% {\r\n    -webkit-transform: translateY(10.75801px) translateX(-11.1px);\r\n            transform: translateY(10.75801px) translateX(-11.1px);\r\n  }\r\n  89% {\r\n    -webkit-transform: translateY(11.51021px) translateX(-11.7px);\r\n            transform: translateY(11.51021px) translateX(-11.7px);\r\n  }\r\n  91% {\r\n    -webkit-transform: translateY(12.35688px) translateX(-12.3px);\r\n            transform: translateY(12.35688px) translateX(-12.3px);\r\n  }\r\n  93% {\r\n    -webkit-transform: translateY(13.28466px) translateX(-12.9px);\r\n            transform: translateY(13.28466px) translateX(-12.9px);\r\n  }\r\n  95% {\r\n    -webkit-transform: translateY(14.27891px) translateX(-13.5px);\r\n            transform: translateY(14.27891px) translateX(-13.5px);\r\n  }\r\n  97% {\r\n    -webkit-transform: translateY(15.32396px) translateX(-14.1px);\r\n            transform: translateY(15.32396px) translateX(-14.1px);\r\n  }\r\n  99% {\r\n    -webkit-transform: translateY(16.40333px) translateX(-14.7px);\r\n            transform: translateY(16.40333px) translateX(-14.7px);\r\n  }\r\n}\r\n@-webkit-keyframes hailMiddle {\r\n  50% {\r\n    -webkit-transform: translateY(17.5px);\r\n            transform: translateY(17.5px);\r\n  }\r\n  51% {\r\n    -webkit-transform: translateY(17.5px) translateX(-0.15px);\r\n            transform: translateY(17.5px) translateX(-0.15px);\r\n  }\r\n  53% {\r\n    -webkit-transform: translateY(16.40333px) translateX(-0.45px);\r\n            transform: translateY(16.40333px) translateX(-0.45px);\r\n  }\r\n  55% {\r\n    -webkit-transform: translateY(15.32396px) translateX(-0.75px);\r\n            transform: translateY(15.32396px) translateX(-0.75px);\r\n  }\r\n  57% {\r\n    -webkit-transform: translateY(14.27891px) translateX(-1.05px);\r\n            transform: translateY(14.27891px) translateX(-1.05px);\r\n  }\r\n  59% {\r\n    -webkit-transform: translateY(13.28466px) translateX(-1.35px);\r\n            transform: translateY(13.28466px) translateX(-1.35px);\r\n  }\r\n  61% {\r\n    -webkit-transform: translateY(12.35688px) translateX(-1.65px);\r\n            transform: translateY(12.35688px) translateX(-1.65px);\r\n  }\r\n  63% {\r\n    -webkit-transform: translateY(11.51021px) translateX(-1.95px);\r\n            transform: translateY(11.51021px) translateX(-1.95px);\r\n  }\r\n  65% {\r\n    -webkit-transform: translateY(10.75801px) translateX(-2.25px);\r\n            transform: translateY(10.75801px) translateX(-2.25px);\r\n  }\r\n  67% {\r\n    -webkit-transform: translateY(10.11213px) translateX(-2.55px);\r\n            transform: translateY(10.11213px) translateX(-2.55px);\r\n  }\r\n  69% {\r\n    -webkit-transform: translateY(9.58276px) translateX(-2.85px);\r\n            transform: translateY(9.58276px) translateX(-2.85px);\r\n  }\r\n  71% {\r\n    -webkit-transform: translateY(9.17826px) translateX(-3.15px);\r\n            transform: translateY(9.17826px) translateX(-3.15px);\r\n  }\r\n  73% {\r\n    -webkit-transform: translateY(8.90499px) translateX(-3.45px);\r\n            transform: translateY(8.90499px) translateX(-3.45px);\r\n  }\r\n  75% {\r\n    -webkit-transform: translateY(8.76727px) translateX(-3.75px);\r\n            transform: translateY(8.76727px) translateX(-3.75px);\r\n  }\r\n  77% {\r\n    -webkit-transform: translateY(8.76727px) translateX(-4.05px);\r\n            transform: translateY(8.76727px) translateX(-4.05px);\r\n  }\r\n  79% {\r\n    -webkit-transform: translateY(8.90499px) translateX(-4.35px);\r\n            transform: translateY(8.90499px) translateX(-4.35px);\r\n  }\r\n  81% {\r\n    -webkit-transform: translateY(9.17826px) translateX(-4.65px);\r\n            transform: translateY(9.17826px) translateX(-4.65px);\r\n  }\r\n  83% {\r\n    -webkit-transform: translateY(9.58276px) translateX(-4.95px);\r\n            transform: translateY(9.58276px) translateX(-4.95px);\r\n  }\r\n  85% {\r\n    -webkit-transform: translateY(10.11213px) translateX(-5.25px);\r\n            transform: translateY(10.11213px) translateX(-5.25px);\r\n  }\r\n  87% {\r\n    -webkit-transform: translateY(10.75801px) translateX(-5.55px);\r\n            transform: translateY(10.75801px) translateX(-5.55px);\r\n  }\r\n  89% {\r\n    -webkit-transform: translateY(11.51021px) translateX(-5.85px);\r\n            transform: translateY(11.51021px) translateX(-5.85px);\r\n  }\r\n  91% {\r\n    -webkit-transform: translateY(12.35688px) translateX(-6.15px);\r\n            transform: translateY(12.35688px) translateX(-6.15px);\r\n  }\r\n  93% {\r\n    -webkit-transform: translateY(13.28466px) translateX(-6.45px);\r\n            transform: translateY(13.28466px) translateX(-6.45px);\r\n  }\r\n  95% {\r\n    -webkit-transform: translateY(14.27891px) translateX(-6.75px);\r\n            transform: translateY(14.27891px) translateX(-6.75px);\r\n  }\r\n  97% {\r\n    -webkit-transform: translateY(15.32396px) translateX(-7.05px);\r\n            transform: translateY(15.32396px) translateX(-7.05px);\r\n  }\r\n  99% {\r\n    -webkit-transform: translateY(16.40333px) translateX(-7.35px);\r\n            transform: translateY(16.40333px) translateX(-7.35px);\r\n  }\r\n}\r\n@keyframes hailMiddle {\r\n  50% {\r\n    -webkit-transform: translateY(17.5px);\r\n            transform: translateY(17.5px);\r\n  }\r\n  51% {\r\n    -webkit-transform: translateY(17.5px) translateX(-0.15px);\r\n            transform: translateY(17.5px) translateX(-0.15px);\r\n  }\r\n  53% {\r\n    -webkit-transform: translateY(16.40333px) translateX(-0.45px);\r\n            transform: translateY(16.40333px) translateX(-0.45px);\r\n  }\r\n  55% {\r\n    -webkit-transform: translateY(15.32396px) translateX(-0.75px);\r\n            transform: translateY(15.32396px) translateX(-0.75px);\r\n  }\r\n  57% {\r\n    -webkit-transform: translateY(14.27891px) translateX(-1.05px);\r\n            transform: translateY(14.27891px) translateX(-1.05px);\r\n  }\r\n  59% {\r\n    -webkit-transform: translateY(13.28466px) translateX(-1.35px);\r\n            transform: translateY(13.28466px) translateX(-1.35px);\r\n  }\r\n  61% {\r\n    -webkit-transform: translateY(12.35688px) translateX(-1.65px);\r\n            transform: translateY(12.35688px) translateX(-1.65px);\r\n  }\r\n  63% {\r\n    -webkit-transform: translateY(11.51021px) translateX(-1.95px);\r\n            transform: translateY(11.51021px) translateX(-1.95px);\r\n  }\r\n  65% {\r\n    -webkit-transform: translateY(10.75801px) translateX(-2.25px);\r\n            transform: translateY(10.75801px) translateX(-2.25px);\r\n  }\r\n  67% {\r\n    -webkit-transform: translateY(10.11213px) translateX(-2.55px);\r\n            transform: translateY(10.11213px) translateX(-2.55px);\r\n  }\r\n  69% {\r\n    -webkit-transform: translateY(9.58276px) translateX(-2.85px);\r\n            transform: translateY(9.58276px) translateX(-2.85px);\r\n  }\r\n  71% {\r\n    -webkit-transform: translateY(9.17826px) translateX(-3.15px);\r\n            transform: translateY(9.17826px) translateX(-3.15px);\r\n  }\r\n  73% {\r\n    -webkit-transform: translateY(8.90499px) translateX(-3.45px);\r\n            transform: translateY(8.90499px) translateX(-3.45px);\r\n  }\r\n  75% {\r\n    -webkit-transform: translateY(8.76727px) translateX(-3.75px);\r\n            transform: translateY(8.76727px) translateX(-3.75px);\r\n  }\r\n  77% {\r\n    -webkit-transform: translateY(8.76727px) translateX(-4.05px);\r\n            transform: translateY(8.76727px) translateX(-4.05px);\r\n  }\r\n  79% {\r\n    -webkit-transform: translateY(8.90499px) translateX(-4.35px);\r\n            transform: translateY(8.90499px) translateX(-4.35px);\r\n  }\r\n  81% {\r\n    -webkit-transform: translateY(9.17826px) translateX(-4.65px);\r\n            transform: translateY(9.17826px) translateX(-4.65px);\r\n  }\r\n  83% {\r\n    -webkit-transform: translateY(9.58276px) translateX(-4.95px);\r\n            transform: translateY(9.58276px) translateX(-4.95px);\r\n  }\r\n  85% {\r\n    -webkit-transform: translateY(10.11213px) translateX(-5.25px);\r\n            transform: translateY(10.11213px) translateX(-5.25px);\r\n  }\r\n  87% {\r\n    -webkit-transform: translateY(10.75801px) translateX(-5.55px);\r\n            transform: translateY(10.75801px) translateX(-5.55px);\r\n  }\r\n  89% {\r\n    -webkit-transform: translateY(11.51021px) translateX(-5.85px);\r\n            transform: translateY(11.51021px) translateX(-5.85px);\r\n  }\r\n  91% {\r\n    -webkit-transform: translateY(12.35688px) translateX(-6.15px);\r\n            transform: translateY(12.35688px) translateX(-6.15px);\r\n  }\r\n  93% {\r\n    -webkit-transform: translateY(13.28466px) translateX(-6.45px);\r\n            transform: translateY(13.28466px) translateX(-6.45px);\r\n  }\r\n  95% {\r\n    -webkit-transform: translateY(14.27891px) translateX(-6.75px);\r\n            transform: translateY(14.27891px) translateX(-6.75px);\r\n  }\r\n  97% {\r\n    -webkit-transform: translateY(15.32396px) translateX(-7.05px);\r\n            transform: translateY(15.32396px) translateX(-7.05px);\r\n  }\r\n  99% {\r\n    -webkit-transform: translateY(16.40333px) translateX(-7.35px);\r\n            transform: translateY(16.40333px) translateX(-7.35px);\r\n  }\r\n}\r\n@-webkit-keyframes hailMiddle2 {\r\n  50% {\r\n    -webkit-transform: translateY(17.5px);\r\n            transform: translateY(17.5px);\r\n  }\r\n  51% {\r\n    -webkit-transform: translateY(17.5px) translateX(0.15px);\r\n            transform: translateY(17.5px) translateX(0.15px);\r\n  }\r\n  53% {\r\n    -webkit-transform: translateY(16.40333px) translateX(0.45px);\r\n            transform: translateY(16.40333px) translateX(0.45px);\r\n  }\r\n  55% {\r\n    -webkit-transform: translateY(15.32396px) translateX(0.75px);\r\n            transform: translateY(15.32396px) translateX(0.75px);\r\n  }\r\n  57% {\r\n    -webkit-transform: translateY(14.27891px) translateX(1.05px);\r\n            transform: translateY(14.27891px) translateX(1.05px);\r\n  }\r\n  59% {\r\n    -webkit-transform: translateY(13.28466px) translateX(1.35px);\r\n            transform: translateY(13.28466px) translateX(1.35px);\r\n  }\r\n  61% {\r\n    -webkit-transform: translateY(12.35688px) translateX(1.65px);\r\n            transform: translateY(12.35688px) translateX(1.65px);\r\n  }\r\n  63% {\r\n    -webkit-transform: translateY(11.51021px) translateX(1.95px);\r\n            transform: translateY(11.51021px) translateX(1.95px);\r\n  }\r\n  65% {\r\n    -webkit-transform: translateY(10.75801px) translateX(2.25px);\r\n            transform: translateY(10.75801px) translateX(2.25px);\r\n  }\r\n  67% {\r\n    -webkit-transform: translateY(10.11213px) translateX(2.55px);\r\n            transform: translateY(10.11213px) translateX(2.55px);\r\n  }\r\n  69% {\r\n    -webkit-transform: translateY(9.58276px) translateX(2.85px);\r\n            transform: translateY(9.58276px) translateX(2.85px);\r\n  }\r\n  71% {\r\n    -webkit-transform: translateY(9.17826px) translateX(3.15px);\r\n            transform: translateY(9.17826px) translateX(3.15px);\r\n  }\r\n  73% {\r\n    -webkit-transform: translateY(8.90499px) translateX(3.45px);\r\n            transform: translateY(8.90499px) translateX(3.45px);\r\n  }\r\n  75% {\r\n    -webkit-transform: translateY(8.76727px) translateX(3.75px);\r\n            transform: translateY(8.76727px) translateX(3.75px);\r\n  }\r\n  77% {\r\n    -webkit-transform: translateY(8.76727px) translateX(4.05px);\r\n            transform: translateY(8.76727px) translateX(4.05px);\r\n  }\r\n  79% {\r\n    -webkit-transform: translateY(8.90499px) translateX(4.35px);\r\n            transform: translateY(8.90499px) translateX(4.35px);\r\n  }\r\n  81% {\r\n    -webkit-transform: translateY(9.17826px) translateX(4.65px);\r\n            transform: translateY(9.17826px) translateX(4.65px);\r\n  }\r\n  83% {\r\n    -webkit-transform: translateY(9.58276px) translateX(4.95px);\r\n            transform: translateY(9.58276px) translateX(4.95px);\r\n  }\r\n  85% {\r\n    -webkit-transform: translateY(10.11213px) translateX(5.25px);\r\n            transform: translateY(10.11213px) translateX(5.25px);\r\n  }\r\n  87% {\r\n    -webkit-transform: translateY(10.75801px) translateX(5.55px);\r\n            transform: translateY(10.75801px) translateX(5.55px);\r\n  }\r\n  89% {\r\n    -webkit-transform: translateY(11.51021px) translateX(5.85px);\r\n            transform: translateY(11.51021px) translateX(5.85px);\r\n  }\r\n  91% {\r\n    -webkit-transform: translateY(12.35688px) translateX(6.15px);\r\n            transform: translateY(12.35688px) translateX(6.15px);\r\n  }\r\n  93% {\r\n    -webkit-transform: translateY(13.28466px) translateX(6.45px);\r\n            transform: translateY(13.28466px) translateX(6.45px);\r\n  }\r\n  95% {\r\n    -webkit-transform: translateY(14.27891px) translateX(6.75px);\r\n            transform: translateY(14.27891px) translateX(6.75px);\r\n  }\r\n  97% {\r\n    -webkit-transform: translateY(15.32396px) translateX(7.05px);\r\n            transform: translateY(15.32396px) translateX(7.05px);\r\n  }\r\n  99% {\r\n    -webkit-transform: translateY(16.40333px) translateX(7.35px);\r\n            transform: translateY(16.40333px) translateX(7.35px);\r\n  }\r\n}\r\n@keyframes hailMiddle2 {\r\n  50% {\r\n    -webkit-transform: translateY(17.5px);\r\n            transform: translateY(17.5px);\r\n  }\r\n  51% {\r\n    -webkit-transform: translateY(17.5px) translateX(0.15px);\r\n            transform: translateY(17.5px) translateX(0.15px);\r\n  }\r\n  53% {\r\n    -webkit-transform: translateY(16.40333px) translateX(0.45px);\r\n            transform: translateY(16.40333px) translateX(0.45px);\r\n  }\r\n  55% {\r\n    -webkit-transform: translateY(15.32396px) translateX(0.75px);\r\n            transform: translateY(15.32396px) translateX(0.75px);\r\n  }\r\n  57% {\r\n    -webkit-transform: translateY(14.27891px) translateX(1.05px);\r\n            transform: translateY(14.27891px) translateX(1.05px);\r\n  }\r\n  59% {\r\n    -webkit-transform: translateY(13.28466px) translateX(1.35px);\r\n            transform: translateY(13.28466px) translateX(1.35px);\r\n  }\r\n  61% {\r\n    -webkit-transform: translateY(12.35688px) translateX(1.65px);\r\n            transform: translateY(12.35688px) translateX(1.65px);\r\n  }\r\n  63% {\r\n    -webkit-transform: translateY(11.51021px) translateX(1.95px);\r\n            transform: translateY(11.51021px) translateX(1.95px);\r\n  }\r\n  65% {\r\n    -webkit-transform: translateY(10.75801px) translateX(2.25px);\r\n            transform: translateY(10.75801px) translateX(2.25px);\r\n  }\r\n  67% {\r\n    -webkit-transform: translateY(10.11213px) translateX(2.55px);\r\n            transform: translateY(10.11213px) translateX(2.55px);\r\n  }\r\n  69% {\r\n    -webkit-transform: translateY(9.58276px) translateX(2.85px);\r\n            transform: translateY(9.58276px) translateX(2.85px);\r\n  }\r\n  71% {\r\n    -webkit-transform: translateY(9.17826px) translateX(3.15px);\r\n            transform: translateY(9.17826px) translateX(3.15px);\r\n  }\r\n  73% {\r\n    -webkit-transform: translateY(8.90499px) translateX(3.45px);\r\n            transform: translateY(8.90499px) translateX(3.45px);\r\n  }\r\n  75% {\r\n    -webkit-transform: translateY(8.76727px) translateX(3.75px);\r\n            transform: translateY(8.76727px) translateX(3.75px);\r\n  }\r\n  77% {\r\n    -webkit-transform: translateY(8.76727px) translateX(4.05px);\r\n            transform: translateY(8.76727px) translateX(4.05px);\r\n  }\r\n  79% {\r\n    -webkit-transform: translateY(8.90499px) translateX(4.35px);\r\n            transform: translateY(8.90499px) translateX(4.35px);\r\n  }\r\n  81% {\r\n    -webkit-transform: translateY(9.17826px) translateX(4.65px);\r\n            transform: translateY(9.17826px) translateX(4.65px);\r\n  }\r\n  83% {\r\n    -webkit-transform: translateY(9.58276px) translateX(4.95px);\r\n            transform: translateY(9.58276px) translateX(4.95px);\r\n  }\r\n  85% {\r\n    -webkit-transform: translateY(10.11213px) translateX(5.25px);\r\n            transform: translateY(10.11213px) translateX(5.25px);\r\n  }\r\n  87% {\r\n    -webkit-transform: translateY(10.75801px) translateX(5.55px);\r\n            transform: translateY(10.75801px) translateX(5.55px);\r\n  }\r\n  89% {\r\n    -webkit-transform: translateY(11.51021px) translateX(5.85px);\r\n            transform: translateY(11.51021px) translateX(5.85px);\r\n  }\r\n  91% {\r\n    -webkit-transform: translateY(12.35688px) translateX(6.15px);\r\n            transform: translateY(12.35688px) translateX(6.15px);\r\n  }\r\n  93% {\r\n    -webkit-transform: translateY(13.28466px) translateX(6.45px);\r\n            transform: translateY(13.28466px) translateX(6.45px);\r\n  }\r\n  95% {\r\n    -webkit-transform: translateY(14.27891px) translateX(6.75px);\r\n            transform: translateY(14.27891px) translateX(6.75px);\r\n  }\r\n  97% {\r\n    -webkit-transform: translateY(15.32396px) translateX(7.05px);\r\n            transform: translateY(15.32396px) translateX(7.05px);\r\n  }\r\n  99% {\r\n    -webkit-transform: translateY(16.40333px) translateX(7.35px);\r\n            transform: translateY(16.40333px) translateX(7.35px);\r\n  }\r\n}\r\n@-webkit-keyframes hailRight {\r\n  50% {\r\n    -webkit-transform: translateY(17.5px);\r\n            transform: translateY(17.5px);\r\n  }\r\n  51% {\r\n    -webkit-transform: translateY(17.5px) translateX(0.3px);\r\n            transform: translateY(17.5px) translateX(0.3px);\r\n  }\r\n  53% {\r\n    -webkit-transform: translateY(16.40333px) translateX(0.9px);\r\n            transform: translateY(16.40333px) translateX(0.9px);\r\n  }\r\n  55% {\r\n    -webkit-transform: translateY(15.32396px) translateX(1.5px);\r\n            transform: translateY(15.32396px) translateX(1.5px);\r\n  }\r\n  57% {\r\n    -webkit-transform: translateY(14.27891px) translateX(2.1px);\r\n            transform: translateY(14.27891px) translateX(2.1px);\r\n  }\r\n  59% {\r\n    -webkit-transform: translateY(13.28466px) translateX(2.7px);\r\n            transform: translateY(13.28466px) translateX(2.7px);\r\n  }\r\n  61% {\r\n    -webkit-transform: translateY(12.35688px) translateX(3.3px);\r\n            transform: translateY(12.35688px) translateX(3.3px);\r\n  }\r\n  63% {\r\n    -webkit-transform: translateY(11.51021px) translateX(3.9px);\r\n            transform: translateY(11.51021px) translateX(3.9px);\r\n  }\r\n  65% {\r\n    -webkit-transform: translateY(10.75801px) translateX(4.5px);\r\n            transform: translateY(10.75801px) translateX(4.5px);\r\n  }\r\n  67% {\r\n    -webkit-transform: translateY(10.11213px) translateX(5.1px);\r\n            transform: translateY(10.11213px) translateX(5.1px);\r\n  }\r\n  69% {\r\n    -webkit-transform: translateY(9.58276px) translateX(5.7px);\r\n            transform: translateY(9.58276px) translateX(5.7px);\r\n  }\r\n  71% {\r\n    -webkit-transform: translateY(9.17826px) translateX(6.3px);\r\n            transform: translateY(9.17826px) translateX(6.3px);\r\n  }\r\n  73% {\r\n    -webkit-transform: translateY(8.90499px) translateX(6.9px);\r\n            transform: translateY(8.90499px) translateX(6.9px);\r\n  }\r\n  75% {\r\n    -webkit-transform: translateY(8.76727px) translateX(7.5px);\r\n            transform: translateY(8.76727px) translateX(7.5px);\r\n  }\r\n  77% {\r\n    -webkit-transform: translateY(8.76727px) translateX(8.1px);\r\n            transform: translateY(8.76727px) translateX(8.1px);\r\n  }\r\n  79% {\r\n    -webkit-transform: translateY(8.90499px) translateX(8.7px);\r\n            transform: translateY(8.90499px) translateX(8.7px);\r\n  }\r\n  81% {\r\n    -webkit-transform: translateY(9.17826px) translateX(9.3px);\r\n            transform: translateY(9.17826px) translateX(9.3px);\r\n  }\r\n  83% {\r\n    -webkit-transform: translateY(9.58276px) translateX(9.9px);\r\n            transform: translateY(9.58276px) translateX(9.9px);\r\n  }\r\n  85% {\r\n    -webkit-transform: translateY(10.11213px) translateX(10.5px);\r\n            transform: translateY(10.11213px) translateX(10.5px);\r\n  }\r\n  87% {\r\n    -webkit-transform: translateY(10.75801px) translateX(11.1px);\r\n            transform: translateY(10.75801px) translateX(11.1px);\r\n  }\r\n  89% {\r\n    -webkit-transform: translateY(11.51021px) translateX(11.7px);\r\n            transform: translateY(11.51021px) translateX(11.7px);\r\n  }\r\n  91% {\r\n    -webkit-transform: translateY(12.35688px) translateX(12.3px);\r\n            transform: translateY(12.35688px) translateX(12.3px);\r\n  }\r\n  93% {\r\n    -webkit-transform: translateY(13.28466px) translateX(12.9px);\r\n            transform: translateY(13.28466px) translateX(12.9px);\r\n  }\r\n  95% {\r\n    -webkit-transform: translateY(14.27891px) translateX(13.5px);\r\n            transform: translateY(14.27891px) translateX(13.5px);\r\n  }\r\n  97% {\r\n    -webkit-transform: translateY(15.32396px) translateX(14.1px);\r\n            transform: translateY(15.32396px) translateX(14.1px);\r\n  }\r\n  99% {\r\n    -webkit-transform: translateY(16.40333px) translateX(14.7px);\r\n            transform: translateY(16.40333px) translateX(14.7px);\r\n  }\r\n}\r\n@keyframes hailRight {\r\n  50% {\r\n    -webkit-transform: translateY(17.5px);\r\n            transform: translateY(17.5px);\r\n  }\r\n  51% {\r\n    -webkit-transform: translateY(17.5px) translateX(0.3px);\r\n            transform: translateY(17.5px) translateX(0.3px);\r\n  }\r\n  53% {\r\n    -webkit-transform: translateY(16.40333px) translateX(0.9px);\r\n            transform: translateY(16.40333px) translateX(0.9px);\r\n  }\r\n  55% {\r\n    -webkit-transform: translateY(15.32396px) translateX(1.5px);\r\n            transform: translateY(15.32396px) translateX(1.5px);\r\n  }\r\n  57% {\r\n    -webkit-transform: translateY(14.27891px) translateX(2.1px);\r\n            transform: translateY(14.27891px) translateX(2.1px);\r\n  }\r\n  59% {\r\n    -webkit-transform: translateY(13.28466px) translateX(2.7px);\r\n            transform: translateY(13.28466px) translateX(2.7px);\r\n  }\r\n  61% {\r\n    -webkit-transform: translateY(12.35688px) translateX(3.3px);\r\n            transform: translateY(12.35688px) translateX(3.3px);\r\n  }\r\n  63% {\r\n    -webkit-transform: translateY(11.51021px) translateX(3.9px);\r\n            transform: translateY(11.51021px) translateX(3.9px);\r\n  }\r\n  65% {\r\n    -webkit-transform: translateY(10.75801px) translateX(4.5px);\r\n            transform: translateY(10.75801px) translateX(4.5px);\r\n  }\r\n  67% {\r\n    -webkit-transform: translateY(10.11213px) translateX(5.1px);\r\n            transform: translateY(10.11213px) translateX(5.1px);\r\n  }\r\n  69% {\r\n    -webkit-transform: translateY(9.58276px) translateX(5.7px);\r\n            transform: translateY(9.58276px) translateX(5.7px);\r\n  }\r\n  71% {\r\n    -webkit-transform: translateY(9.17826px) translateX(6.3px);\r\n            transform: translateY(9.17826px) translateX(6.3px);\r\n  }\r\n  73% {\r\n    -webkit-transform: translateY(8.90499px) translateX(6.9px);\r\n            transform: translateY(8.90499px) translateX(6.9px);\r\n  }\r\n  75% {\r\n    -webkit-transform: translateY(8.76727px) translateX(7.5px);\r\n            transform: translateY(8.76727px) translateX(7.5px);\r\n  }\r\n  77% {\r\n    -webkit-transform: translateY(8.76727px) translateX(8.1px);\r\n            transform: translateY(8.76727px) translateX(8.1px);\r\n  }\r\n  79% {\r\n    -webkit-transform: translateY(8.90499px) translateX(8.7px);\r\n            transform: translateY(8.90499px) translateX(8.7px);\r\n  }\r\n  81% {\r\n    -webkit-transform: translateY(9.17826px) translateX(9.3px);\r\n            transform: translateY(9.17826px) translateX(9.3px);\r\n  }\r\n  83% {\r\n    -webkit-transform: translateY(9.58276px) translateX(9.9px);\r\n            transform: translateY(9.58276px) translateX(9.9px);\r\n  }\r\n  85% {\r\n    -webkit-transform: translateY(10.11213px) translateX(10.5px);\r\n            transform: translateY(10.11213px) translateX(10.5px);\r\n  }\r\n  87% {\r\n    -webkit-transform: translateY(10.75801px) translateX(11.1px);\r\n            transform: translateY(10.75801px) translateX(11.1px);\r\n  }\r\n  89% {\r\n    -webkit-transform: translateY(11.51021px) translateX(11.7px);\r\n            transform: translateY(11.51021px) translateX(11.7px);\r\n  }\r\n  91% {\r\n    -webkit-transform: translateY(12.35688px) translateX(12.3px);\r\n            transform: translateY(12.35688px) translateX(12.3px);\r\n  }\r\n  93% {\r\n    -webkit-transform: translateY(13.28466px) translateX(12.9px);\r\n            transform: translateY(13.28466px) translateX(12.9px);\r\n  }\r\n  95% {\r\n    -webkit-transform: translateY(14.27891px) translateX(13.5px);\r\n            transform: translateY(14.27891px) translateX(13.5px);\r\n  }\r\n  97% {\r\n    -webkit-transform: translateY(15.32396px) translateX(14.1px);\r\n            transform: translateY(15.32396px) translateX(14.1px);\r\n  }\r\n  99% {\r\n    -webkit-transform: translateY(16.40333px) translateX(14.7px);\r\n            transform: translateY(16.40333px) translateX(14.7px);\r\n  }\r\n}\r\n@-webkit-keyframes fillOpacity {\r\n  0% {\r\n    fill-opacity: 0;\r\n    stroke-opacity: 0;\r\n  }\r\n  100% {\r\n    fill-opacity: 1;\r\n    stroke-opacity: 1;\r\n  }\r\n}\r\n@keyframes fillOpacity {\r\n  0% {\r\n    fill-opacity: 0;\r\n    stroke-opacity: 0;\r\n  }\r\n  100% {\r\n    fill-opacity: 1;\r\n    stroke-opacity: 1;\r\n  }\r\n}\r\n@-webkit-keyframes fillOpacity2 {\r\n  0% {\r\n    fill-opacity: 0;\r\n    stroke-opacity: 0;\r\n  }\r\n  50% {\r\n    fill-opacity: 1;\r\n    stroke-opacity: 1;\r\n  }\r\n  100% {\r\n    fill-opacity: 0;\r\n    stroke-opacity: 0;\r\n  }\r\n}\r\n@keyframes fillOpacity2 {\r\n  0% {\r\n    fill-opacity: 0;\r\n    stroke-opacity: 0;\r\n  }\r\n  50% {\r\n    fill-opacity: 1;\r\n    stroke-opacity: 1;\r\n  }\r\n  100% {\r\n    fill-opacity: 0;\r\n    stroke-opacity: 0;\r\n  }\r\n}\r\n@-webkit-keyframes lightningFlash {\r\n  0% {\r\n    fill-opacity: 0;\r\n  }\r\n  1% {\r\n    fill-opacity: 1;\r\n  }\r\n  2% {\r\n    fill-opacity: 0;\r\n  }\r\n  50% {\r\n    fill-opacity: 0;\r\n  }\r\n  51% {\r\n    fill-opacity: 1;\r\n  }\r\n  52% {\r\n    fill-opacity: 0;\r\n  }\r\n  53% {\r\n    fill-opacity: 1;\r\n  }\r\n  54% {\r\n    fill-opacity: 0;\r\n  }\r\n  100% {\r\n    fill-opacity: 0;\r\n  }\r\n}\r\n@keyframes lightningFlash {\r\n  0% {\r\n    fill-opacity: 0;\r\n  }\r\n  1% {\r\n    fill-opacity: 1;\r\n  }\r\n  2% {\r\n    fill-opacity: 0;\r\n  }\r\n  50% {\r\n    fill-opacity: 0;\r\n  }\r\n  51% {\r\n    fill-opacity: 1;\r\n  }\r\n  52% {\r\n    fill-opacity: 0;\r\n  }\r\n  53% {\r\n    fill-opacity: 1;\r\n  }\r\n  54% {\r\n    fill-opacity: 0;\r\n  }\r\n  100% {\r\n    fill-opacity: 0;\r\n  }\r\n}\r\n@-webkit-keyframes snowFall {\r\n  0% {\r\n    -webkit-transform: translateY(0px) translateX(0px);\r\n            transform: translateY(0px) translateX(0px);\r\n  }\r\n  2% {\r\n    -webkit-transform: translateY(0.35px) translateX(0.75349px);\r\n            transform: translateY(0.35px) translateX(0.75349px);\r\n  }\r\n  4% {\r\n    -webkit-transform: translateY(0.7px) translateX(1.44133px);\r\n            transform: translateY(0.7px) translateX(1.44133px);\r\n  }\r\n  6% {\r\n    -webkit-transform: translateY(1.05px) translateX(2.06119px);\r\n            transform: translateY(1.05px) translateX(2.06119px);\r\n  }\r\n  8% {\r\n    -webkit-transform: translateY(1.4px) translateX(2.61124px);\r\n            transform: translateY(1.4px) translateX(2.61124px);\r\n  }\r\n  10% {\r\n    -webkit-transform: translateY(1.75px) translateX(3.09017px);\r\n            transform: translateY(1.75px) translateX(3.09017px);\r\n  }\r\n  12% {\r\n    -webkit-transform: translateY(2.1px) translateX(3.49718px);\r\n            transform: translateY(2.1px) translateX(3.49718px);\r\n  }\r\n  14% {\r\n    -webkit-transform: translateY(2.45px) translateX(3.83201px);\r\n            transform: translateY(2.45px) translateX(3.83201px);\r\n  }\r\n  16% {\r\n    -webkit-transform: translateY(2.8px) translateX(4.09491px);\r\n            transform: translateY(2.8px) translateX(4.09491px);\r\n  }\r\n  18% {\r\n    -webkit-transform: translateY(3.15px) translateX(4.28661px);\r\n            transform: translateY(3.15px) translateX(4.28661px);\r\n  }\r\n  20% {\r\n    -webkit-transform: translateY(3.5px) translateX(4.40839px);\r\n            transform: translateY(3.5px) translateX(4.40839px);\r\n  }\r\n  22% {\r\n    -webkit-transform: translateY(3.85px) translateX(4.46197px);\r\n            transform: translateY(3.85px) translateX(4.46197px);\r\n  }\r\n  24% {\r\n    -webkit-transform: translateY(4.2px) translateX(4.44956px);\r\n            transform: translateY(4.2px) translateX(4.44956px);\r\n  }\r\n  26% {\r\n    -webkit-transform: translateY(4.55px) translateX(4.37381px);\r\n            transform: translateY(4.55px) translateX(4.37381px);\r\n  }\r\n  28% {\r\n    -webkit-transform: translateY(4.9px) translateX(4.23782px);\r\n            transform: translateY(4.9px) translateX(4.23782px);\r\n  }\r\n  30% {\r\n    -webkit-transform: translateY(5.25px) translateX(4.04508px);\r\n            transform: translateY(5.25px) translateX(4.04508px);\r\n  }\r\n  32% {\r\n    -webkit-transform: translateY(5.6px) translateX(3.79948px);\r\n            transform: translateY(5.6px) translateX(3.79948px);\r\n  }\r\n  34% {\r\n    -webkit-transform: translateY(5.95px) translateX(3.50523px);\r\n            transform: translateY(5.95px) translateX(3.50523px);\r\n  }\r\n  36% {\r\n    -webkit-transform: translateY(6.3px) translateX(3.16689px);\r\n            transform: translateY(6.3px) translateX(3.16689px);\r\n  }\r\n  38% {\r\n    -webkit-transform: translateY(6.65px) translateX(2.78933px);\r\n            transform: translateY(6.65px) translateX(2.78933px);\r\n  }\r\n  40% {\r\n    -webkit-transform: translateY(7px) translateX(2.37764px);\r\n            transform: translateY(7px) translateX(2.37764px);\r\n  }\r\n  42% {\r\n    -webkit-transform: translateY(7.35px) translateX(1.93717px);\r\n            transform: translateY(7.35px) translateX(1.93717px);\r\n  }\r\n  44% {\r\n    -webkit-transform: translateY(7.7px) translateX(1.47343px);\r\n            transform: translateY(7.7px) translateX(1.47343px);\r\n  }\r\n  46% {\r\n    -webkit-transform: translateY(8.05px) translateX(0.99211px);\r\n            transform: translateY(8.05px) translateX(0.99211px);\r\n  }\r\n  48% {\r\n    -webkit-transform: translateY(8.4px) translateX(0.49901px);\r\n            transform: translateY(8.4px) translateX(0.49901px);\r\n  }\r\n  50% {\r\n    -webkit-transform: translateY(8.75px) translateX(0px);\r\n            transform: translateY(8.75px) translateX(0px);\r\n  }\r\n  52% {\r\n    -webkit-transform: translateY(9.1px) translateX(-0.49901px);\r\n            transform: translateY(9.1px) translateX(-0.49901px);\r\n  }\r\n  54% {\r\n    -webkit-transform: translateY(9.45px) translateX(-0.99211px);\r\n            transform: translateY(9.45px) translateX(-0.99211px);\r\n  }\r\n  56% {\r\n    -webkit-transform: translateY(9.8px) translateX(-1.47343px);\r\n            transform: translateY(9.8px) translateX(-1.47343px);\r\n  }\r\n  58% {\r\n    -webkit-transform: translateY(10.15px) translateX(-1.93717px);\r\n            transform: translateY(10.15px) translateX(-1.93717px);\r\n  }\r\n  60% {\r\n    -webkit-transform: translateY(10.5px) translateX(-2.37764px);\r\n            transform: translateY(10.5px) translateX(-2.37764px);\r\n  }\r\n  62% {\r\n    -webkit-transform: translateY(10.85px) translateX(-2.78933px);\r\n            transform: translateY(10.85px) translateX(-2.78933px);\r\n  }\r\n  64% {\r\n    -webkit-transform: translateY(11.2px) translateX(-3.16689px);\r\n            transform: translateY(11.2px) translateX(-3.16689px);\r\n  }\r\n  66% {\r\n    -webkit-transform: translateY(11.55px) translateX(-3.50523px);\r\n            transform: translateY(11.55px) translateX(-3.50523px);\r\n  }\r\n  68% {\r\n    -webkit-transform: translateY(11.9px) translateX(-3.79948px);\r\n            transform: translateY(11.9px) translateX(-3.79948px);\r\n  }\r\n  70% {\r\n    -webkit-transform: translateY(12.25px) translateX(-4.04508px);\r\n            transform: translateY(12.25px) translateX(-4.04508px);\r\n  }\r\n  72% {\r\n    -webkit-transform: translateY(12.6px) translateX(-4.23782px);\r\n            transform: translateY(12.6px) translateX(-4.23782px);\r\n  }\r\n  74% {\r\n    -webkit-transform: translateY(12.95px) translateX(-4.37381px);\r\n            transform: translateY(12.95px) translateX(-4.37381px);\r\n  }\r\n  76% {\r\n    -webkit-transform: translateY(13.3px) translateX(-4.44956px);\r\n            transform: translateY(13.3px) translateX(-4.44956px);\r\n  }\r\n  78% {\r\n    -webkit-transform: translateY(13.65px) translateX(-4.46197px);\r\n            transform: translateY(13.65px) translateX(-4.46197px);\r\n  }\r\n  80% {\r\n    -webkit-transform: translateY(14px) translateX(-4.40839px);\r\n            transform: translateY(14px) translateX(-4.40839px);\r\n  }\r\n  82% {\r\n    -webkit-transform: translateY(14.35px) translateX(-4.28661px);\r\n            transform: translateY(14.35px) translateX(-4.28661px);\r\n  }\r\n  84% {\r\n    -webkit-transform: translateY(14.7px) translateX(-4.09491px);\r\n            transform: translateY(14.7px) translateX(-4.09491px);\r\n  }\r\n  86% {\r\n    -webkit-transform: translateY(15.05px) translateX(-3.83201px);\r\n            transform: translateY(15.05px) translateX(-3.83201px);\r\n  }\r\n  88% {\r\n    -webkit-transform: translateY(15.4px) translateX(-3.49718px);\r\n            transform: translateY(15.4px) translateX(-3.49718px);\r\n  }\r\n  90% {\r\n    -webkit-transform: translateY(15.75px) translateX(-3.09017px);\r\n            transform: translateY(15.75px) translateX(-3.09017px);\r\n  }\r\n  92% {\r\n    -webkit-transform: translateY(16.1px) translateX(-2.61124px);\r\n            transform: translateY(16.1px) translateX(-2.61124px);\r\n  }\r\n  94% {\r\n    -webkit-transform: translateY(16.45px) translateX(-2.06119px);\r\n            transform: translateY(16.45px) translateX(-2.06119px);\r\n  }\r\n  96% {\r\n    -webkit-transform: translateY(16.8px) translateX(-1.44133px);\r\n            transform: translateY(16.8px) translateX(-1.44133px);\r\n  }\r\n  98% {\r\n    -webkit-transform: translateY(17.15px) translateX(-0.75349px);\r\n            transform: translateY(17.15px) translateX(-0.75349px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateY(17.5px) translateX(0px);\r\n            transform: translateY(17.5px) translateX(0px);\r\n  }\r\n}\r\n@keyframes snowFall {\r\n  0% {\r\n    -webkit-transform: translateY(0px) translateX(0px);\r\n            transform: translateY(0px) translateX(0px);\r\n  }\r\n  2% {\r\n    -webkit-transform: translateY(0.35px) translateX(0.75349px);\r\n            transform: translateY(0.35px) translateX(0.75349px);\r\n  }\r\n  4% {\r\n    -webkit-transform: translateY(0.7px) translateX(1.44133px);\r\n            transform: translateY(0.7px) translateX(1.44133px);\r\n  }\r\n  6% {\r\n    -webkit-transform: translateY(1.05px) translateX(2.06119px);\r\n            transform: translateY(1.05px) translateX(2.06119px);\r\n  }\r\n  8% {\r\n    -webkit-transform: translateY(1.4px) translateX(2.61124px);\r\n            transform: translateY(1.4px) translateX(2.61124px);\r\n  }\r\n  10% {\r\n    -webkit-transform: translateY(1.75px) translateX(3.09017px);\r\n            transform: translateY(1.75px) translateX(3.09017px);\r\n  }\r\n  12% {\r\n    -webkit-transform: translateY(2.1px) translateX(3.49718px);\r\n            transform: translateY(2.1px) translateX(3.49718px);\r\n  }\r\n  14% {\r\n    -webkit-transform: translateY(2.45px) translateX(3.83201px);\r\n            transform: translateY(2.45px) translateX(3.83201px);\r\n  }\r\n  16% {\r\n    -webkit-transform: translateY(2.8px) translateX(4.09491px);\r\n            transform: translateY(2.8px) translateX(4.09491px);\r\n  }\r\n  18% {\r\n    -webkit-transform: translateY(3.15px) translateX(4.28661px);\r\n            transform: translateY(3.15px) translateX(4.28661px);\r\n  }\r\n  20% {\r\n    -webkit-transform: translateY(3.5px) translateX(4.40839px);\r\n            transform: translateY(3.5px) translateX(4.40839px);\r\n  }\r\n  22% {\r\n    -webkit-transform: translateY(3.85px) translateX(4.46197px);\r\n            transform: translateY(3.85px) translateX(4.46197px);\r\n  }\r\n  24% {\r\n    -webkit-transform: translateY(4.2px) translateX(4.44956px);\r\n            transform: translateY(4.2px) translateX(4.44956px);\r\n  }\r\n  26% {\r\n    -webkit-transform: translateY(4.55px) translateX(4.37381px);\r\n            transform: translateY(4.55px) translateX(4.37381px);\r\n  }\r\n  28% {\r\n    -webkit-transform: translateY(4.9px) translateX(4.23782px);\r\n            transform: translateY(4.9px) translateX(4.23782px);\r\n  }\r\n  30% {\r\n    -webkit-transform: translateY(5.25px) translateX(4.04508px);\r\n            transform: translateY(5.25px) translateX(4.04508px);\r\n  }\r\n  32% {\r\n    -webkit-transform: translateY(5.6px) translateX(3.79948px);\r\n            transform: translateY(5.6px) translateX(3.79948px);\r\n  }\r\n  34% {\r\n    -webkit-transform: translateY(5.95px) translateX(3.50523px);\r\n            transform: translateY(5.95px) translateX(3.50523px);\r\n  }\r\n  36% {\r\n    -webkit-transform: translateY(6.3px) translateX(3.16689px);\r\n            transform: translateY(6.3px) translateX(3.16689px);\r\n  }\r\n  38% {\r\n    -webkit-transform: translateY(6.65px) translateX(2.78933px);\r\n            transform: translateY(6.65px) translateX(2.78933px);\r\n  }\r\n  40% {\r\n    -webkit-transform: translateY(7px) translateX(2.37764px);\r\n            transform: translateY(7px) translateX(2.37764px);\r\n  }\r\n  42% {\r\n    -webkit-transform: translateY(7.35px) translateX(1.93717px);\r\n            transform: translateY(7.35px) translateX(1.93717px);\r\n  }\r\n  44% {\r\n    -webkit-transform: translateY(7.7px) translateX(1.47343px);\r\n            transform: translateY(7.7px) translateX(1.47343px);\r\n  }\r\n  46% {\r\n    -webkit-transform: translateY(8.05px) translateX(0.99211px);\r\n            transform: translateY(8.05px) translateX(0.99211px);\r\n  }\r\n  48% {\r\n    -webkit-transform: translateY(8.4px) translateX(0.49901px);\r\n            transform: translateY(8.4px) translateX(0.49901px);\r\n  }\r\n  50% {\r\n    -webkit-transform: translateY(8.75px) translateX(0px);\r\n            transform: translateY(8.75px) translateX(0px);\r\n  }\r\n  52% {\r\n    -webkit-transform: translateY(9.1px) translateX(-0.49901px);\r\n            transform: translateY(9.1px) translateX(-0.49901px);\r\n  }\r\n  54% {\r\n    -webkit-transform: translateY(9.45px) translateX(-0.99211px);\r\n            transform: translateY(9.45px) translateX(-0.99211px);\r\n  }\r\n  56% {\r\n    -webkit-transform: translateY(9.8px) translateX(-1.47343px);\r\n            transform: translateY(9.8px) translateX(-1.47343px);\r\n  }\r\n  58% {\r\n    -webkit-transform: translateY(10.15px) translateX(-1.93717px);\r\n            transform: translateY(10.15px) translateX(-1.93717px);\r\n  }\r\n  60% {\r\n    -webkit-transform: translateY(10.5px) translateX(-2.37764px);\r\n            transform: translateY(10.5px) translateX(-2.37764px);\r\n  }\r\n  62% {\r\n    -webkit-transform: translateY(10.85px) translateX(-2.78933px);\r\n            transform: translateY(10.85px) translateX(-2.78933px);\r\n  }\r\n  64% {\r\n    -webkit-transform: translateY(11.2px) translateX(-3.16689px);\r\n            transform: translateY(11.2px) translateX(-3.16689px);\r\n  }\r\n  66% {\r\n    -webkit-transform: translateY(11.55px) translateX(-3.50523px);\r\n            transform: translateY(11.55px) translateX(-3.50523px);\r\n  }\r\n  68% {\r\n    -webkit-transform: translateY(11.9px) translateX(-3.79948px);\r\n            transform: translateY(11.9px) translateX(-3.79948px);\r\n  }\r\n  70% {\r\n    -webkit-transform: translateY(12.25px) translateX(-4.04508px);\r\n            transform: translateY(12.25px) translateX(-4.04508px);\r\n  }\r\n  72% {\r\n    -webkit-transform: translateY(12.6px) translateX(-4.23782px);\r\n            transform: translateY(12.6px) translateX(-4.23782px);\r\n  }\r\n  74% {\r\n    -webkit-transform: translateY(12.95px) translateX(-4.37381px);\r\n            transform: translateY(12.95px) translateX(-4.37381px);\r\n  }\r\n  76% {\r\n    -webkit-transform: translateY(13.3px) translateX(-4.44956px);\r\n            transform: translateY(13.3px) translateX(-4.44956px);\r\n  }\r\n  78% {\r\n    -webkit-transform: translateY(13.65px) translateX(-4.46197px);\r\n            transform: translateY(13.65px) translateX(-4.46197px);\r\n  }\r\n  80% {\r\n    -webkit-transform: translateY(14px) translateX(-4.40839px);\r\n            transform: translateY(14px) translateX(-4.40839px);\r\n  }\r\n  82% {\r\n    -webkit-transform: translateY(14.35px) translateX(-4.28661px);\r\n            transform: translateY(14.35px) translateX(-4.28661px);\r\n  }\r\n  84% {\r\n    -webkit-transform: translateY(14.7px) translateX(-4.09491px);\r\n            transform: translateY(14.7px) translateX(-4.09491px);\r\n  }\r\n  86% {\r\n    -webkit-transform: translateY(15.05px) translateX(-3.83201px);\r\n            transform: translateY(15.05px) translateX(-3.83201px);\r\n  }\r\n  88% {\r\n    -webkit-transform: translateY(15.4px) translateX(-3.49718px);\r\n            transform: translateY(15.4px) translateX(-3.49718px);\r\n  }\r\n  90% {\r\n    -webkit-transform: translateY(15.75px) translateX(-3.09017px);\r\n            transform: translateY(15.75px) translateX(-3.09017px);\r\n  }\r\n  92% {\r\n    -webkit-transform: translateY(16.1px) translateX(-2.61124px);\r\n            transform: translateY(16.1px) translateX(-2.61124px);\r\n  }\r\n  94% {\r\n    -webkit-transform: translateY(16.45px) translateX(-2.06119px);\r\n            transform: translateY(16.45px) translateX(-2.06119px);\r\n  }\r\n  96% {\r\n    -webkit-transform: translateY(16.8px) translateX(-1.44133px);\r\n            transform: translateY(16.8px) translateX(-1.44133px);\r\n  }\r\n  98% {\r\n    -webkit-transform: translateY(17.15px) translateX(-0.75349px);\r\n            transform: translateY(17.15px) translateX(-0.75349px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateY(17.5px) translateX(0px);\r\n            transform: translateY(17.5px) translateX(0px);\r\n  }\r\n}\r\n@-webkit-keyframes snowFall2 {\r\n  0% {\r\n    -webkit-transform: translateY(0px) translateX(0px);\r\n            transform: translateY(0px) translateX(0px);\r\n  }\r\n  2% {\r\n    -webkit-transform: translateY(0.35px) translateX(-0.75349px);\r\n            transform: translateY(0.35px) translateX(-0.75349px);\r\n  }\r\n  4% {\r\n    -webkit-transform: translateY(0.7px) translateX(-1.44133px);\r\n            transform: translateY(0.7px) translateX(-1.44133px);\r\n  }\r\n  6% {\r\n    -webkit-transform: translateY(1.05px) translateX(-2.06119px);\r\n            transform: translateY(1.05px) translateX(-2.06119px);\r\n  }\r\n  8% {\r\n    -webkit-transform: translateY(1.4px) translateX(-2.61124px);\r\n            transform: translateY(1.4px) translateX(-2.61124px);\r\n  }\r\n  10% {\r\n    -webkit-transform: translateY(1.75px) translateX(-3.09017px);\r\n            transform: translateY(1.75px) translateX(-3.09017px);\r\n  }\r\n  12% {\r\n    -webkit-transform: translateY(2.1px) translateX(-3.49718px);\r\n            transform: translateY(2.1px) translateX(-3.49718px);\r\n  }\r\n  14% {\r\n    -webkit-transform: translateY(2.45px) translateX(-3.83201px);\r\n            transform: translateY(2.45px) translateX(-3.83201px);\r\n  }\r\n  16% {\r\n    -webkit-transform: translateY(2.8px) translateX(-4.09491px);\r\n            transform: translateY(2.8px) translateX(-4.09491px);\r\n  }\r\n  18% {\r\n    -webkit-transform: translateY(3.15px) translateX(-4.28661px);\r\n            transform: translateY(3.15px) translateX(-4.28661px);\r\n  }\r\n  20% {\r\n    -webkit-transform: translateY(3.5px) translateX(-4.40839px);\r\n            transform: translateY(3.5px) translateX(-4.40839px);\r\n  }\r\n  22% {\r\n    -webkit-transform: translateY(3.85px) translateX(-4.46197px);\r\n            transform: translateY(3.85px) translateX(-4.46197px);\r\n  }\r\n  24% {\r\n    -webkit-transform: translateY(4.2px) translateX(-4.44956px);\r\n            transform: translateY(4.2px) translateX(-4.44956px);\r\n  }\r\n  26% {\r\n    -webkit-transform: translateY(4.55px) translateX(-4.37381px);\r\n            transform: translateY(4.55px) translateX(-4.37381px);\r\n  }\r\n  28% {\r\n    -webkit-transform: translateY(4.9px) translateX(-4.23782px);\r\n            transform: translateY(4.9px) translateX(-4.23782px);\r\n  }\r\n  30% {\r\n    -webkit-transform: translateY(5.25px) translateX(-4.04508px);\r\n            transform: translateY(5.25px) translateX(-4.04508px);\r\n  }\r\n  32% {\r\n    -webkit-transform: translateY(5.6px) translateX(-3.79948px);\r\n            transform: translateY(5.6px) translateX(-3.79948px);\r\n  }\r\n  34% {\r\n    -webkit-transform: translateY(5.95px) translateX(-3.50523px);\r\n            transform: translateY(5.95px) translateX(-3.50523px);\r\n  }\r\n  36% {\r\n    -webkit-transform: translateY(6.3px) translateX(-3.16689px);\r\n            transform: translateY(6.3px) translateX(-3.16689px);\r\n  }\r\n  38% {\r\n    -webkit-transform: translateY(6.65px) translateX(-2.78933px);\r\n            transform: translateY(6.65px) translateX(-2.78933px);\r\n  }\r\n  40% {\r\n    -webkit-transform: translateY(7px) translateX(-2.37764px);\r\n            transform: translateY(7px) translateX(-2.37764px);\r\n  }\r\n  42% {\r\n    -webkit-transform: translateY(7.35px) translateX(-1.93717px);\r\n            transform: translateY(7.35px) translateX(-1.93717px);\r\n  }\r\n  44% {\r\n    -webkit-transform: translateY(7.7px) translateX(-1.47343px);\r\n            transform: translateY(7.7px) translateX(-1.47343px);\r\n  }\r\n  46% {\r\n    -webkit-transform: translateY(8.05px) translateX(-0.99211px);\r\n            transform: translateY(8.05px) translateX(-0.99211px);\r\n  }\r\n  48% {\r\n    -webkit-transform: translateY(8.4px) translateX(-0.49901px);\r\n            transform: translateY(8.4px) translateX(-0.49901px);\r\n  }\r\n  50% {\r\n    -webkit-transform: translateY(8.75px) translateX(0px);\r\n            transform: translateY(8.75px) translateX(0px);\r\n  }\r\n  52% {\r\n    -webkit-transform: translateY(9.1px) translateX(0.49901px);\r\n            transform: translateY(9.1px) translateX(0.49901px);\r\n  }\r\n  54% {\r\n    -webkit-transform: translateY(9.45px) translateX(0.99211px);\r\n            transform: translateY(9.45px) translateX(0.99211px);\r\n  }\r\n  56% {\r\n    -webkit-transform: translateY(9.8px) translateX(1.47343px);\r\n            transform: translateY(9.8px) translateX(1.47343px);\r\n  }\r\n  58% {\r\n    -webkit-transform: translateY(10.15px) translateX(1.93717px);\r\n            transform: translateY(10.15px) translateX(1.93717px);\r\n  }\r\n  60% {\r\n    -webkit-transform: translateY(10.5px) translateX(2.37764px);\r\n            transform: translateY(10.5px) translateX(2.37764px);\r\n  }\r\n  62% {\r\n    -webkit-transform: translateY(10.85px) translateX(2.78933px);\r\n            transform: translateY(10.85px) translateX(2.78933px);\r\n  }\r\n  64% {\r\n    -webkit-transform: translateY(11.2px) translateX(3.16689px);\r\n            transform: translateY(11.2px) translateX(3.16689px);\r\n  }\r\n  66% {\r\n    -webkit-transform: translateY(11.55px) translateX(3.50523px);\r\n            transform: translateY(11.55px) translateX(3.50523px);\r\n  }\r\n  68% {\r\n    -webkit-transform: translateY(11.9px) translateX(3.79948px);\r\n            transform: translateY(11.9px) translateX(3.79948px);\r\n  }\r\n  70% {\r\n    -webkit-transform: translateY(12.25px) translateX(4.04508px);\r\n            transform: translateY(12.25px) translateX(4.04508px);\r\n  }\r\n  72% {\r\n    -webkit-transform: translateY(12.6px) translateX(4.23782px);\r\n            transform: translateY(12.6px) translateX(4.23782px);\r\n  }\r\n  74% {\r\n    -webkit-transform: translateY(12.95px) translateX(4.37381px);\r\n            transform: translateY(12.95px) translateX(4.37381px);\r\n  }\r\n  76% {\r\n    -webkit-transform: translateY(13.3px) translateX(4.44956px);\r\n            transform: translateY(13.3px) translateX(4.44956px);\r\n  }\r\n  78% {\r\n    -webkit-transform: translateY(13.65px) translateX(4.46197px);\r\n            transform: translateY(13.65px) translateX(4.46197px);\r\n  }\r\n  80% {\r\n    -webkit-transform: translateY(14px) translateX(4.40839px);\r\n            transform: translateY(14px) translateX(4.40839px);\r\n  }\r\n  82% {\r\n    -webkit-transform: translateY(14.35px) translateX(4.28661px);\r\n            transform: translateY(14.35px) translateX(4.28661px);\r\n  }\r\n  84% {\r\n    -webkit-transform: translateY(14.7px) translateX(4.09491px);\r\n            transform: translateY(14.7px) translateX(4.09491px);\r\n  }\r\n  86% {\r\n    -webkit-transform: translateY(15.05px) translateX(3.83201px);\r\n            transform: translateY(15.05px) translateX(3.83201px);\r\n  }\r\n  88% {\r\n    -webkit-transform: translateY(15.4px) translateX(3.49718px);\r\n            transform: translateY(15.4px) translateX(3.49718px);\r\n  }\r\n  90% {\r\n    -webkit-transform: translateY(15.75px) translateX(3.09017px);\r\n            transform: translateY(15.75px) translateX(3.09017px);\r\n  }\r\n  92% {\r\n    -webkit-transform: translateY(16.1px) translateX(2.61124px);\r\n            transform: translateY(16.1px) translateX(2.61124px);\r\n  }\r\n  94% {\r\n    -webkit-transform: translateY(16.45px) translateX(2.06119px);\r\n            transform: translateY(16.45px) translateX(2.06119px);\r\n  }\r\n  96% {\r\n    -webkit-transform: translateY(16.8px) translateX(1.44133px);\r\n            transform: translateY(16.8px) translateX(1.44133px);\r\n  }\r\n  98% {\r\n    -webkit-transform: translateY(17.15px) translateX(0.75349px);\r\n            transform: translateY(17.15px) translateX(0.75349px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateY(17.5px) translateX(0px);\r\n            transform: translateY(17.5px) translateX(0px);\r\n  }\r\n}\r\n@keyframes snowFall2 {\r\n  0% {\r\n    -webkit-transform: translateY(0px) translateX(0px);\r\n            transform: translateY(0px) translateX(0px);\r\n  }\r\n  2% {\r\n    -webkit-transform: translateY(0.35px) translateX(-0.75349px);\r\n            transform: translateY(0.35px) translateX(-0.75349px);\r\n  }\r\n  4% {\r\n    -webkit-transform: translateY(0.7px) translateX(-1.44133px);\r\n            transform: translateY(0.7px) translateX(-1.44133px);\r\n  }\r\n  6% {\r\n    -webkit-transform: translateY(1.05px) translateX(-2.06119px);\r\n            transform: translateY(1.05px) translateX(-2.06119px);\r\n  }\r\n  8% {\r\n    -webkit-transform: translateY(1.4px) translateX(-2.61124px);\r\n            transform: translateY(1.4px) translateX(-2.61124px);\r\n  }\r\n  10% {\r\n    -webkit-transform: translateY(1.75px) translateX(-3.09017px);\r\n            transform: translateY(1.75px) translateX(-3.09017px);\r\n  }\r\n  12% {\r\n    -webkit-transform: translateY(2.1px) translateX(-3.49718px);\r\n            transform: translateY(2.1px) translateX(-3.49718px);\r\n  }\r\n  14% {\r\n    -webkit-transform: translateY(2.45px) translateX(-3.83201px);\r\n            transform: translateY(2.45px) translateX(-3.83201px);\r\n  }\r\n  16% {\r\n    -webkit-transform: translateY(2.8px) translateX(-4.09491px);\r\n            transform: translateY(2.8px) translateX(-4.09491px);\r\n  }\r\n  18% {\r\n    -webkit-transform: translateY(3.15px) translateX(-4.28661px);\r\n            transform: translateY(3.15px) translateX(-4.28661px);\r\n  }\r\n  20% {\r\n    -webkit-transform: translateY(3.5px) translateX(-4.40839px);\r\n            transform: translateY(3.5px) translateX(-4.40839px);\r\n  }\r\n  22% {\r\n    -webkit-transform: translateY(3.85px) translateX(-4.46197px);\r\n            transform: translateY(3.85px) translateX(-4.46197px);\r\n  }\r\n  24% {\r\n    -webkit-transform: translateY(4.2px) translateX(-4.44956px);\r\n            transform: translateY(4.2px) translateX(-4.44956px);\r\n  }\r\n  26% {\r\n    -webkit-transform: translateY(4.55px) translateX(-4.37381px);\r\n            transform: translateY(4.55px) translateX(-4.37381px);\r\n  }\r\n  28% {\r\n    -webkit-transform: translateY(4.9px) translateX(-4.23782px);\r\n            transform: translateY(4.9px) translateX(-4.23782px);\r\n  }\r\n  30% {\r\n    -webkit-transform: translateY(5.25px) translateX(-4.04508px);\r\n            transform: translateY(5.25px) translateX(-4.04508px);\r\n  }\r\n  32% {\r\n    -webkit-transform: translateY(5.6px) translateX(-3.79948px);\r\n            transform: translateY(5.6px) translateX(-3.79948px);\r\n  }\r\n  34% {\r\n    -webkit-transform: translateY(5.95px) translateX(-3.50523px);\r\n            transform: translateY(5.95px) translateX(-3.50523px);\r\n  }\r\n  36% {\r\n    -webkit-transform: translateY(6.3px) translateX(-3.16689px);\r\n            transform: translateY(6.3px) translateX(-3.16689px);\r\n  }\r\n  38% {\r\n    -webkit-transform: translateY(6.65px) translateX(-2.78933px);\r\n            transform: translateY(6.65px) translateX(-2.78933px);\r\n  }\r\n  40% {\r\n    -webkit-transform: translateY(7px) translateX(-2.37764px);\r\n            transform: translateY(7px) translateX(-2.37764px);\r\n  }\r\n  42% {\r\n    -webkit-transform: translateY(7.35px) translateX(-1.93717px);\r\n            transform: translateY(7.35px) translateX(-1.93717px);\r\n  }\r\n  44% {\r\n    -webkit-transform: translateY(7.7px) translateX(-1.47343px);\r\n            transform: translateY(7.7px) translateX(-1.47343px);\r\n  }\r\n  46% {\r\n    -webkit-transform: translateY(8.05px) translateX(-0.99211px);\r\n            transform: translateY(8.05px) translateX(-0.99211px);\r\n  }\r\n  48% {\r\n    -webkit-transform: translateY(8.4px) translateX(-0.49901px);\r\n            transform: translateY(8.4px) translateX(-0.49901px);\r\n  }\r\n  50% {\r\n    -webkit-transform: translateY(8.75px) translateX(0px);\r\n            transform: translateY(8.75px) translateX(0px);\r\n  }\r\n  52% {\r\n    -webkit-transform: translateY(9.1px) translateX(0.49901px);\r\n            transform: translateY(9.1px) translateX(0.49901px);\r\n  }\r\n  54% {\r\n    -webkit-transform: translateY(9.45px) translateX(0.99211px);\r\n            transform: translateY(9.45px) translateX(0.99211px);\r\n  }\r\n  56% {\r\n    -webkit-transform: translateY(9.8px) translateX(1.47343px);\r\n            transform: translateY(9.8px) translateX(1.47343px);\r\n  }\r\n  58% {\r\n    -webkit-transform: translateY(10.15px) translateX(1.93717px);\r\n            transform: translateY(10.15px) translateX(1.93717px);\r\n  }\r\n  60% {\r\n    -webkit-transform: translateY(10.5px) translateX(2.37764px);\r\n            transform: translateY(10.5px) translateX(2.37764px);\r\n  }\r\n  62% {\r\n    -webkit-transform: translateY(10.85px) translateX(2.78933px);\r\n            transform: translateY(10.85px) translateX(2.78933px);\r\n  }\r\n  64% {\r\n    -webkit-transform: translateY(11.2px) translateX(3.16689px);\r\n            transform: translateY(11.2px) translateX(3.16689px);\r\n  }\r\n  66% {\r\n    -webkit-transform: translateY(11.55px) translateX(3.50523px);\r\n            transform: translateY(11.55px) translateX(3.50523px);\r\n  }\r\n  68% {\r\n    -webkit-transform: translateY(11.9px) translateX(3.79948px);\r\n            transform: translateY(11.9px) translateX(3.79948px);\r\n  }\r\n  70% {\r\n    -webkit-transform: translateY(12.25px) translateX(4.04508px);\r\n            transform: translateY(12.25px) translateX(4.04508px);\r\n  }\r\n  72% {\r\n    -webkit-transform: translateY(12.6px) translateX(4.23782px);\r\n            transform: translateY(12.6px) translateX(4.23782px);\r\n  }\r\n  74% {\r\n    -webkit-transform: translateY(12.95px) translateX(4.37381px);\r\n            transform: translateY(12.95px) translateX(4.37381px);\r\n  }\r\n  76% {\r\n    -webkit-transform: translateY(13.3px) translateX(4.44956px);\r\n            transform: translateY(13.3px) translateX(4.44956px);\r\n  }\r\n  78% {\r\n    -webkit-transform: translateY(13.65px) translateX(4.46197px);\r\n            transform: translateY(13.65px) translateX(4.46197px);\r\n  }\r\n  80% {\r\n    -webkit-transform: translateY(14px) translateX(4.40839px);\r\n            transform: translateY(14px) translateX(4.40839px);\r\n  }\r\n  82% {\r\n    -webkit-transform: translateY(14.35px) translateX(4.28661px);\r\n            transform: translateY(14.35px) translateX(4.28661px);\r\n  }\r\n  84% {\r\n    -webkit-transform: translateY(14.7px) translateX(4.09491px);\r\n            transform: translateY(14.7px) translateX(4.09491px);\r\n  }\r\n  86% {\r\n    -webkit-transform: translateY(15.05px) translateX(3.83201px);\r\n            transform: translateY(15.05px) translateX(3.83201px);\r\n  }\r\n  88% {\r\n    -webkit-transform: translateY(15.4px) translateX(3.49718px);\r\n            transform: translateY(15.4px) translateX(3.49718px);\r\n  }\r\n  90% {\r\n    -webkit-transform: translateY(15.75px) translateX(3.09017px);\r\n            transform: translateY(15.75px) translateX(3.09017px);\r\n  }\r\n  92% {\r\n    -webkit-transform: translateY(16.1px) translateX(2.61124px);\r\n            transform: translateY(16.1px) translateX(2.61124px);\r\n  }\r\n  94% {\r\n    -webkit-transform: translateY(16.45px) translateX(2.06119px);\r\n            transform: translateY(16.45px) translateX(2.06119px);\r\n  }\r\n  96% {\r\n    -webkit-transform: translateY(16.8px) translateX(1.44133px);\r\n            transform: translateY(16.8px) translateX(1.44133px);\r\n  }\r\n  98% {\r\n    -webkit-transform: translateY(17.15px) translateX(0.75349px);\r\n            transform: translateY(17.15px) translateX(0.75349px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateY(17.5px) translateX(0px);\r\n            transform: translateY(17.5px) translateX(0px);\r\n  }\r\n}\r\n/* Tornado */\r\n@-webkit-keyframes translateTornado1 {\r\n  0% {\r\n    -webkit-transform: translateY(0);\r\n            transform: translateY(0);\r\n  }\r\n  25% {\r\n    -webkit-transform: translateX(3.997px);\r\n            transform: translateX(3.997px);\r\n  }\r\n  75% {\r\n    -webkit-transform: translateX(-3.997px);\r\n            transform: translateX(-3.997px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateX(0);\r\n            transform: translateX(0);\r\n  }\r\n}\r\n@keyframes translateTornado1 {\r\n  0% {\r\n    -webkit-transform: translateY(0);\r\n            transform: translateY(0);\r\n  }\r\n  25% {\r\n    -webkit-transform: translateX(3.997px);\r\n            transform: translateX(3.997px);\r\n  }\r\n  75% {\r\n    -webkit-transform: translateX(-3.997px);\r\n            transform: translateX(-3.997px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateX(0);\r\n            transform: translateX(0);\r\n  }\r\n}\r\n@-webkit-keyframes translateTornado2 {\r\n  0% {\r\n    -webkit-transform: translateY(0);\r\n            transform: translateY(0);\r\n  }\r\n  25% {\r\n    -webkit-transform: translateX(2.002px);\r\n            transform: translateX(2.002px);\r\n  }\r\n  75% {\r\n    -webkit-transform: translateX(-2.002px);\r\n            transform: translateX(-2.002px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateX(0);\r\n            transform: translateX(0);\r\n  }\r\n}\r\n@keyframes translateTornado2 {\r\n  0% {\r\n    -webkit-transform: translateY(0);\r\n            transform: translateY(0);\r\n  }\r\n  25% {\r\n    -webkit-transform: translateX(2.002px);\r\n            transform: translateX(2.002px);\r\n  }\r\n  75% {\r\n    -webkit-transform: translateX(-2.002px);\r\n            transform: translateX(-2.002px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateX(0);\r\n            transform: translateX(0);\r\n  }\r\n}\r\n@-webkit-keyframes translateTornado3 {\r\n  0% {\r\n    -webkit-transform: translateY(0);\r\n            transform: translateY(0);\r\n  }\r\n  25% {\r\n    -webkit-transform: translateX(8.001px);\r\n            transform: translateX(8.001px);\r\n  }\r\n  75% {\r\n    -webkit-transform: translateX(-8.001px);\r\n            transform: translateX(-8.001px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateX(0);\r\n            transform: translateX(0);\r\n  }\r\n}\r\n@keyframes translateTornado3 {\r\n  0% {\r\n    -webkit-transform: translateY(0);\r\n            transform: translateY(0);\r\n  }\r\n  25% {\r\n    -webkit-transform: translateX(8.001px);\r\n            transform: translateX(8.001px);\r\n  }\r\n  75% {\r\n    -webkit-transform: translateX(-8.001px);\r\n            transform: translateX(-8.001px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateX(0);\r\n            transform: translateX(0);\r\n  }\r\n}\r\n@-webkit-keyframes translateTornado4 {\r\n  0% {\r\n    -webkit-transform: translateY(0);\r\n            transform: translateY(0);\r\n  }\r\n  25% {\r\n    -webkit-transform: translateX(5.999px);\r\n            transform: translateX(5.999px);\r\n  }\r\n  75% {\r\n    -webkit-transform: translateX(-5.999px);\r\n            transform: translateX(-5.999px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateX(0);\r\n            transform: translateX(0);\r\n  }\r\n}\r\n@keyframes translateTornado4 {\r\n  0% {\r\n    -webkit-transform: translateY(0);\r\n            transform: translateY(0);\r\n  }\r\n  25% {\r\n    -webkit-transform: translateX(5.999px);\r\n            transform: translateX(5.999px);\r\n  }\r\n  75% {\r\n    -webkit-transform: translateX(-5.999px);\r\n            transform: translateX(-5.999px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateX(0);\r\n            transform: translateX(0);\r\n  }\r\n}\r\n@-webkit-keyframes translateTornado5 {\r\n  0% {\r\n    -webkit-transform: translateY(0);\r\n            transform: translateY(0);\r\n  }\r\n  25% {\r\n    -webkit-transform: translateX(10.003px);\r\n            transform: translateX(10.003px);\r\n  }\r\n  75% {\r\n    -webkit-transform: translateX(-10.003px);\r\n            transform: translateX(-10.003px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateX(0);\r\n            transform: translateX(0);\r\n  }\r\n}\r\n@keyframes translateTornado5 {\r\n  0% {\r\n    -webkit-transform: translateY(0);\r\n            transform: translateY(0);\r\n  }\r\n  25% {\r\n    -webkit-transform: translateX(10.003px);\r\n            transform: translateX(10.003px);\r\n  }\r\n  75% {\r\n    -webkit-transform: translateX(-10.003px);\r\n            transform: translateX(-10.003px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateX(0);\r\n            transform: translateX(0);\r\n  }\r\n}\r\n@-webkit-keyframes translateTornado6 {\r\n  0% {\r\n    -webkit-transform: translateY(0);\r\n            transform: translateY(0);\r\n  }\r\n  25% {\r\n    -webkit-transform: translateX(5.999px);\r\n            transform: translateX(5.999px);\r\n  }\r\n  75% {\r\n    -webkit-transform: translateX(-5.999px);\r\n            transform: translateX(-5.999px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateX(0);\r\n            transform: translateX(0);\r\n  }\r\n}\r\n@keyframes translateTornado6 {\r\n  0% {\r\n    -webkit-transform: translateY(0);\r\n            transform: translateY(0);\r\n  }\r\n  25% {\r\n    -webkit-transform: translateX(5.999px);\r\n            transform: translateX(5.999px);\r\n  }\r\n  75% {\r\n    -webkit-transform: translateX(-5.999px);\r\n            transform: translateX(-5.999px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateX(0);\r\n            transform: translateX(0);\r\n  }\r\n}\r\n@-webkit-keyframes fillOpacityLightning {\r\n  0% {\r\n    fill-opacity: 0;\r\n    stroke-opacity: 0;\r\n  }\r\n  1% {\r\n    fill-opacity: 1;\r\n    stroke-opacity: 1;\r\n  }\r\n  7% {\r\n    fill-opacity: 0;\r\n    stroke-opacity: 0;\r\n  }\r\n  50% {\r\n    fill-opacity: 0;\r\n    stroke-opacity: 0;\r\n  }\r\n  51% {\r\n    fill-opacity: 1;\r\n    stroke-opacity: 1;\r\n  }\r\n  53% {\r\n    fill-opacity: 0;\r\n    stroke-opacity: 0;\r\n  }\r\n  54% {\r\n    fill-opacity: 1;\r\n    stroke-opacity: 1;\r\n  }\r\n  60% {\r\n    fill-opacity: 0;\r\n    stroke-opacity: 0;\r\n  }\r\n  100% {\r\n    fill-opacity: 0;\r\n    stroke-opacity: 0;\r\n  }\r\n}\r\n@keyframes fillOpacityLightning {\r\n  0% {\r\n    fill-opacity: 0;\r\n    stroke-opacity: 0;\r\n  }\r\n  1% {\r\n    fill-opacity: 1;\r\n    stroke-opacity: 1;\r\n  }\r\n  7% {\r\n    fill-opacity: 0;\r\n    stroke-opacity: 0;\r\n  }\r\n  50% {\r\n    fill-opacity: 0;\r\n    stroke-opacity: 0;\r\n  }\r\n  51% {\r\n    fill-opacity: 1;\r\n    stroke-opacity: 1;\r\n  }\r\n  53% {\r\n    fill-opacity: 0;\r\n    stroke-opacity: 0;\r\n  }\r\n  54% {\r\n    fill-opacity: 1;\r\n    stroke-opacity: 1;\r\n  }\r\n  60% {\r\n    fill-opacity: 0;\r\n    stroke-opacity: 0;\r\n  }\r\n  100% {\r\n    fill-opacity: 0;\r\n    stroke-opacity: 0;\r\n  }\r\n}\r\n@-webkit-keyframes translateFog {\r\n  0% {\r\n    -webkit-transform: translateY(0);\r\n            transform: translateY(0);\r\n  }\r\n  25% {\r\n    -webkit-transform: translateX(2.499px);\r\n            transform: translateX(2.499px);\r\n  }\r\n  75% {\r\n    -webkit-transform: translateX(-2.499px);\r\n            transform: translateX(-2.499px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateX(0);\r\n            transform: translateX(0);\r\n  }\r\n}\r\n@keyframes translateFog {\r\n  0% {\r\n    -webkit-transform: translateY(0);\r\n            transform: translateY(0);\r\n  }\r\n  25% {\r\n    -webkit-transform: translateX(2.499px);\r\n            transform: translateX(2.499px);\r\n  }\r\n  75% {\r\n    -webkit-transform: translateX(-2.499px);\r\n            transform: translateX(-2.499px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateX(0);\r\n            transform: translateX(0);\r\n  }\r\n}\r\n@-webkit-keyframes fillOpacityFog {\r\n  0% {\r\n    fill-opacity: 0.5;\r\n    stroke-opacity: 0.5;\r\n  }\r\n  50% {\r\n    fill-opacity: 1;\r\n    stroke-opacity: 1;\r\n  }\r\n  100% {\r\n    fill-opacity: 0.5;\r\n    stroke-opacity: 0.5;\r\n  }\r\n}\r\n@keyframes fillOpacityFog {\r\n  0% {\r\n    fill-opacity: 0.5;\r\n    stroke-opacity: 0.5;\r\n  }\r\n  50% {\r\n    fill-opacity: 1;\r\n    stroke-opacity: 1;\r\n  }\r\n  100% {\r\n    fill-opacity: 0.5;\r\n    stroke-opacity: 0.5;\r\n  }\r\n}\r\n@-webkit-keyframes translateSunrise {\r\n  0% {\r\n    -webkit-transform: translateY(0);\r\n            transform: translateY(0);\r\n  }\r\n  25% {\r\n    -webkit-transform: translateY(-16.002px);\r\n            transform: translateY(-16.002px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateY(-16.002px);\r\n            transform: translateY(-16.002px);\r\n  }\r\n}\r\n@keyframes translateSunrise {\r\n  0% {\r\n    -webkit-transform: translateY(0);\r\n            transform: translateY(0);\r\n  }\r\n  25% {\r\n    -webkit-transform: translateY(-16.002px);\r\n            transform: translateY(-16.002px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateY(-16.002px);\r\n            transform: translateY(-16.002px);\r\n  }\r\n}\r\n@-webkit-keyframes translateSunset {\r\n  0% {\r\n    -webkit-transform: translateY(-16.002px);\r\n            transform: translateY(-16.002px);\r\n  }\r\n  25% {\r\n    -webkit-transform: translateY(-3.997px);\r\n            transform: translateY(-3.997px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateY(-3.997px);\r\n            transform: translateY(-3.997px);\r\n  }\r\n}\r\n@keyframes translateSunset {\r\n  0% {\r\n    -webkit-transform: translateY(-16.002px);\r\n            transform: translateY(-16.002px);\r\n  }\r\n  25% {\r\n    -webkit-transform: translateY(-3.997px);\r\n            transform: translateY(-3.997px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateY(-3.997px);\r\n            transform: translateY(-3.997px);\r\n  }\r\n}\r\n@-webkit-keyframes translateArrowDown {\r\n  0% {\r\n    -webkit-transform: translateY(2.002px);\r\n            transform: translateY(2.002px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateY(4.998px);\r\n            transform: translateY(4.998px);\r\n  }\r\n}\r\n@keyframes translateArrowDown {\r\n  0% {\r\n    -webkit-transform: translateY(2.002px);\r\n            transform: translateY(2.002px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateY(4.998px);\r\n            transform: translateY(4.998px);\r\n  }\r\n}\r\n@-webkit-keyframes translateArrowUp {\r\n  0% {\r\n    -webkit-transform: translateY(-2.002px);\r\n            transform: translateY(-2.002px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateY(-4.998px);\r\n            transform: translateY(-4.998px);\r\n  }\r\n}\r\n@keyframes translateArrowUp {\r\n  0% {\r\n    -webkit-transform: translateY(-2.002px);\r\n            transform: translateY(-2.002px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateY(-4.998px);\r\n            transform: translateY(-4.998px);\r\n  }\r\n}\r\n@-webkit-keyframes translateWind {\r\n  0% {\r\n    -webkit-transform: translateY(0);\r\n            transform: translateY(0);\r\n  }\r\n  25% {\r\n    -webkit-transform: translateX(4.998px);\r\n            transform: translateX(4.998px);\r\n  }\r\n  75% {\r\n    -webkit-transform: translateX(-4.998px);\r\n            transform: translateX(-4.998px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateX(0);\r\n            transform: translateX(0);\r\n  }\r\n}\r\n@keyframes translateWind {\r\n  0% {\r\n    -webkit-transform: translateY(0);\r\n            transform: translateY(0);\r\n  }\r\n  25% {\r\n    -webkit-transform: translateX(4.998px);\r\n            transform: translateX(4.998px);\r\n  }\r\n  75% {\r\n    -webkit-transform: translateX(-4.998px);\r\n            transform: translateX(-4.998px);\r\n  }\r\n  100% {\r\n    -webkit-transform: translateX(0);\r\n            transform: translateX(0);\r\n  }\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../c3/c3.min.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".c3 svg{font:10px sans-serif;-webkit-tap-highlight-color:transparent}.c3 line,.c3 path{fill:none;stroke:#000}.c3 text{-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.c3-bars path,.c3-event-rect,.c3-legend-item-tile,.c3-xgrid-focus,.c3-ygrid{shape-rendering:crispEdges}.c3-chart-arc path{stroke:#fff}.c3-chart-arc text{fill:#fff;font-size:13px}.c3-grid line{stroke:#aaa}.c3-grid text{fill:#aaa}.c3-xgrid,.c3-ygrid{stroke-dasharray:3 3}.c3-text.c3-empty{fill:grey;font-size:2em}.c3-line{stroke-width:1px}.c3-circle._expanded_{stroke-width:1px;stroke:#fff}.c3-selected-circle{fill:#fff;stroke-width:2px}.c3-bar{stroke-width:0}.c3-bar._expanded_{fill-opacity:1;fill-opacity:.75}.c3-target.c3-focused{opacity:1}.c3-target.c3-focused path.c3-line,.c3-target.c3-focused path.c3-step{stroke-width:2px}.c3-target.c3-defocused{opacity:.3!important}.c3-region{fill:#4682b4;fill-opacity:.1}.c3-brush .extent{fill-opacity:.1}.c3-legend-item{font-size:12px}.c3-legend-item-hidden{opacity:.15}.c3-legend-background{opacity:.75;fill:#fff;stroke:#d3d3d3;stroke-width:1}.c3-title{font:14px sans-serif}.c3-tooltip-container{z-index:10}.c3-tooltip{border-collapse:collapse;border-spacing:0;background-color:#fff;empty-cells:show;box-shadow:7px 7px 12px -9px #777;opacity:.9}.c3-tooltip tr{border:1px solid #ccc}.c3-tooltip th{background-color:#aaa;font-size:14px;padding:2px 5px;text-align:left;color:#fff}.c3-tooltip td{font-size:13px;padding:3px 6px;background-color:#fff;border-left:1px dotted #999}.c3-tooltip td>span{display:inline-block;width:10px;height:10px;margin-right:6px}.c3-tooltip td.value{text-align:right}.c3-area{stroke-width:0;opacity:.2}.c3-chart-arcs-title{dominant-baseline:middle;font-size:1.3em}.c3-chart-arcs .c3-chart-arcs-background{fill:#e0e0e0;stroke:none}.c3-chart-arcs .c3-chart-arcs-gauge-unit{fill:#000;font-size:16px}.c3-chart-arcs .c3-chart-arcs-gauge-max{fill:#777}.c3-chart-arcs .c3-chart-arcs-gauge-min{fill:#777}.c3-chart-arc .c3-gauge-value{fill:#000}.c3-chart-arc.c3-target g path{opacity:1}.c3-chart-arc.c3-target.c3-focused g path{opacity:1}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../nvd3/build/nv.d3.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/* nvd3 version 1.8.5 (https://github.com/novus/nvd3) 2016-12-01 */\n.nvd3 .nv-axis {\r\n    pointer-events:none;\r\n    opacity: 1;\r\n}\r\n\r\n.nvd3 .nv-axis path {\r\n    fill: none;\r\n    stroke: #000;\r\n    stroke-opacity: .75;\r\n    shape-rendering: crispEdges;\r\n}\r\n\r\n.nvd3 .nv-axis path.domain {\r\n    stroke-opacity: .75;\r\n}\r\n\r\n.nvd3 .nv-axis.nv-x path.domain {\r\n    stroke-opacity: 0;\r\n}\r\n\r\n.nvd3 .nv-axis line {\r\n    fill: none;\r\n    stroke: #e5e5e5;\r\n    shape-rendering: crispEdges;\r\n}\r\n\r\n.nvd3 .nv-axis .zero line,\r\n    /*this selector may not be necessary*/ .nvd3 .nv-axis line.zero {\r\n    stroke-opacity: .75;\r\n}\r\n\r\n.nvd3 .nv-axis .nv-axisMaxMin text {\r\n    font-weight: bold;\r\n}\r\n\r\n.nvd3 .x  .nv-axis .nv-axisMaxMin text,\r\n.nvd3 .x2 .nv-axis .nv-axisMaxMin text,\r\n.nvd3 .x3 .nv-axis .nv-axisMaxMin text {\r\n    text-anchor: middle;\r\n}\r\n\r\n.nvd3 .nv-axis.nv-disabled {\r\n    opacity: 0;\r\n}\r\n\n.nvd3 .nv-bars rect {\r\n    fill-opacity: .75;\r\n\r\n    transition: fill-opacity 250ms linear;\r\n}\n\r\n.nvd3 .nv-bars rect.hover {\r\n    fill-opacity: 1;\r\n}\r\n\r\n.nvd3 .nv-bars .hover rect {\r\n    fill: lightblue;\r\n}\r\n\r\n.nvd3 .nv-bars text {\r\n    fill: rgba(0,0,0,0);\r\n}\r\n\r\n.nvd3 .nv-bars .hover text {\r\n    fill: rgba(0,0,0,1);\r\n}\r\n\r\n.nvd3 .nv-multibar .nv-groups rect,\r\n.nvd3 .nv-multibarHorizontal .nv-groups rect,\r\n.nvd3 .nv-discretebar .nv-groups rect {\r\n    stroke-opacity: 0;\r\n\r\n    transition: fill-opacity 250ms linear;\r\n}\n\r\n.nvd3 .nv-multibar .nv-groups rect:hover,\r\n.nvd3 .nv-multibarHorizontal .nv-groups rect:hover,\r\n.nvd3 .nv-candlestickBar .nv-ticks rect:hover,\r\n.nvd3 .nv-discretebar .nv-groups rect:hover {\r\n    fill-opacity: 1;\r\n}\r\n\r\n.nvd3 .nv-discretebar .nv-groups text,\r\n.nvd3 .nv-multibarHorizontal .nv-groups text {\r\n    font-weight: bold;\r\n    fill: rgba(0,0,0,1);\r\n    stroke: rgba(0,0,0,0);\r\n}\r\n\n/* boxplot CSS */\n.nvd3 .nv-boxplot circle {\n  fill-opacity: 0.5;\n}\n\n.nvd3 .nv-boxplot circle:hover {\n  fill-opacity: 1;\n}\n\n.nvd3 .nv-boxplot rect:hover {\n  fill-opacity: 1;\n}\n\n.nvd3 line.nv-boxplot-median {\n  stroke: black;\n}\n\n.nv-boxplot-tick:hover {\n  stroke-width: 2.5px;\n}\n/* bullet */\r\n.nvd3.nv-bullet { font: 10px sans-serif; }\r\n.nvd3.nv-bullet .nv-measure { fill-opacity: .8; }\r\n.nvd3.nv-bullet .nv-measure:hover { fill-opacity: 1; }\r\n.nvd3.nv-bullet .nv-marker { stroke: #000; stroke-width: 2px; }\r\n.nvd3.nv-bullet .nv-markerTriangle { stroke: #000; fill: #fff; stroke-width: 1.5px; }\r\n.nvd3.nv-bullet .nv-markerLine { stroke: #000; stroke-width: 1.5px; }\r\n.nvd3.nv-bullet .nv-tick line { stroke: #666; stroke-width: .5px; }\r\n.nvd3.nv-bullet .nv-range.nv-s0 { fill: #eee; }\r\n.nvd3.nv-bullet .nv-range.nv-s1 { fill: #ddd; }\r\n.nvd3.nv-bullet .nv-range.nv-s2 { fill: #ccc; }\r\n.nvd3.nv-bullet .nv-title { font-size: 14px; font-weight: bold; }\r\n.nvd3.nv-bullet .nv-subtitle { fill: #999; }\r\n\r\n.nvd3.nv-bullet .nv-range {\n    fill: #bababa;\r\n    fill-opacity: .4;\r\n}\n\n.nvd3.nv-bullet .nv-range:hover {\r\n    fill-opacity: .7;\r\n}\r\n\n.nvd3.nv-candlestickBar .nv-ticks .nv-tick {\r\n    stroke-width: 1px;\r\n}\r\n\r\n.nvd3.nv-candlestickBar .nv-ticks .nv-tick.hover {\r\n    stroke-width: 2px;\r\n}\r\n\r\n.nvd3.nv-candlestickBar .nv-ticks .nv-tick.positive rect {\r\n    stroke: #2ca02c;\r\n    fill: #2ca02c;\r\n}\r\n\r\n.nvd3.nv-candlestickBar .nv-ticks .nv-tick.negative rect {\r\n    stroke: #d62728;\r\n    fill: #d62728;\r\n}\r\n\r\n.with-transitions .nv-candlestickBar .nv-ticks .nv-tick {\r\n    transition: stroke-width 250ms linear, stroke-opacity 250ms linear;\r\n}\n\r\n.nvd3.nv-candlestickBar .nv-ticks line {\r\n    stroke: #333;\r\n}\r\n\n.nv-force-node {\n    stroke: #fff;\n    stroke-width: 1.5px;\n}\n\n.nv-force-link {\n    stroke: #999;\n    stroke-opacity: .6;\n}\n\n.nv-force-node text {\n    stroke-width: 0px;\n}\n\n.nvd3 .nv-legend .nv-disabled rect {\n    /*fill-opacity: 0;*/\n}\n\n.nvd3 .nv-check-box .nv-box {\n    fill-opacity:0;\n    stroke-width:2;\n}\n\n.nvd3 .nv-check-box .nv-check {\n    fill-opacity:0;\n    stroke-width:4;\n}\n\n.nvd3 .nv-series.nv-disabled .nv-check-box .nv-check {\n    fill-opacity:0;\n    stroke-opacity:0;\n}\n\n.nvd3 .nv-controlsWrap .nv-legend .nv-check-box .nv-check {\n    opacity: 0;\n}\n\n/* line plus bar */\r\n.nvd3.nv-linePlusBar .nv-bar rect {\r\n    fill-opacity: .75;\r\n}\r\n\r\n.nvd3.nv-linePlusBar .nv-bar rect:hover {\r\n    fill-opacity: 1;\r\n}\n.nvd3 .nv-groups path.nv-line {\r\n    fill: none;\r\n}\r\n\r\n.nvd3 .nv-groups path.nv-area {\r\n    stroke: none;\r\n}\r\n\r\n.nvd3.nv-line .nvd3.nv-scatter .nv-groups .nv-point {\r\n    fill-opacity: 0;\r\n    stroke-opacity: 0;\r\n}\r\n\r\n.nvd3.nv-scatter.nv-single-point .nv-groups .nv-point {\r\n    fill-opacity: .5 !important;\r\n    stroke-opacity: .5 !important;\r\n}\r\n\r\n\r\n.with-transitions .nvd3 .nv-groups .nv-point {\r\n    transition: stroke-width 250ms linear, stroke-opacity 250ms linear;\r\n}\n\r\n.nvd3.nv-scatter .nv-groups .nv-point.hover,\r\n.nvd3 .nv-groups .nv-point.hover {\r\n    stroke-width: 7px;\r\n    fill-opacity: .95 !important;\r\n    stroke-opacity: .95 !important;\r\n}\r\n\r\n\r\n.nvd3 .nv-point-paths path {\r\n    stroke: #aaa;\r\n    stroke-opacity: 0;\r\n    fill: #eee;\r\n    fill-opacity: 0;\r\n}\r\n\r\n\r\n.nvd3 .nv-indexLine {\n    cursor: ew-resize;\r\n}\r\n\n/********************\r\n * SVG CSS\r\n */\r\n\r\n/********************\r\n  Default CSS for an svg element nvd3 used\r\n*/\r\nsvg.nvd3-svg {\r\n    -webkit-user-select: none;\r\n       -moz-user-select: none;\r\n        -ms-user-select: none;\r\n            user-select: none;\n    display: block;\r\n    width:100%;\r\n    height:100%;\r\n}\r\n\r\n/********************\r\n  Box shadow and border radius styling\r\n*/\r\n.nvtooltip.with-3d-shadow, .with-3d-shadow .nvtooltip {\r\n    box-shadow: 0 5px 10px rgba(0,0,0,.2);\n    border-radius: 5px;\n}\r\n\r\n\r\n.nvd3 text {\r\n    font: normal 12px Arial, sans-serif;\r\n}\r\n\r\n.nvd3 .title {\r\n    font: bold 14px Arial, sans-serif;\r\n}\r\n\r\n.nvd3 .nv-background {\r\n    fill: white;\r\n    fill-opacity: 0;\r\n}\r\n\r\n.nvd3.nv-noData {\r\n    font-size: 18px;\r\n    font-weight: bold;\r\n}\r\n\r\n\r\n/**********\r\n*  Brush\r\n*/\r\n\r\n.nv-brush .extent {\r\n    fill-opacity: .125;\r\n    shape-rendering: crispEdges;\r\n}\r\n\r\n.nv-brush .resize path {\r\n    fill: #eee;\r\n    stroke: #666;\r\n}\r\n\r\n\r\n/**********\r\n*  Legend\r\n*/\r\n\r\n.nvd3 .nv-legend .nv-series {\r\n    cursor: pointer;\r\n}\r\n\r\n.nvd3 .nv-legend .nv-disabled circle {\r\n    fill-opacity: 0;\r\n}\r\n\r\n/* focus */\r\n.nvd3 .nv-brush .extent {\r\n    fill-opacity: 0 !important;\r\n}\r\n\r\n.nvd3 .nv-brushBackground rect {\r\n    stroke: #000;\r\n    stroke-width: .4;\r\n    fill: #fff;\r\n    fill-opacity: .7;\r\n}\r\n\r\n/**********\r\n*  Print\r\n*/\r\n\r\n@media print {\r\n    .nvd3 text {\n        stroke-width: 0;\n        fill-opacity: 1;\n    }\n}\r\n\n.nvd3.nv-ohlcBar .nv-ticks .nv-tick {\r\n    stroke-width: 1px;\r\n}\r\n\r\n.nvd3.nv-ohlcBar .nv-ticks .nv-tick.hover {\r\n    stroke-width: 2px;\r\n}\r\n\r\n.nvd3.nv-ohlcBar .nv-ticks .nv-tick.positive {\r\n    stroke: #2ca02c;\r\n}\r\n\r\n.nvd3.nv-ohlcBar .nv-ticks .nv-tick.negative {\r\n    stroke: #d62728;\r\n}\r\n\r\n\n.nvd3 .background path {\r\n    fill: none;\r\n    stroke: #EEE;\r\n    stroke-opacity: .4;\r\n    shape-rendering: crispEdges;\r\n}\r\n\r\n.nvd3 .foreground path {\r\n    fill: none;\r\n    stroke-opacity: .7;\r\n}\r\n\r\n.nvd3 .nv-parallelCoordinates-brush .extent {\n    fill: #fff;\r\n    fill-opacity: .6;\r\n    stroke: gray;\r\n    shape-rendering: crispEdges;\r\n}\r\n\r\n.nvd3 .nv-parallelCoordinates .hover  {\r\n    fill-opacity: 1;\r\n\tstroke-width: 3px;\r\n}\r\n\r\n\r\n.nvd3 .missingValuesline line {\r\n  fill: none;\r\n  stroke: black;\r\n  stroke-width: 1;\r\n  stroke-opacity: 1;\r\n  stroke-dasharray: 5, 5;\n}\n\n.nvd3.nv-pie path {\r\n    stroke-opacity: 0;\r\n    transition: fill-opacity 250ms linear, stroke-width 250ms linear, stroke-opacity 250ms linear;\r\n}\n\r\n.nvd3.nv-pie .nv-pie-title {\r\n    font-size: 24px;\r\n    fill: rgba(19, 196, 249, 0.59);\r\n}\r\n\r\n.nvd3.nv-pie .nv-slice text {\r\n    stroke: #000;\r\n    stroke-width: 0;\r\n}\r\n\r\n.nvd3.nv-pie path {\r\n    stroke: #fff;\r\n    stroke-width: 1px;\r\n    stroke-opacity: 1;\r\n}\r\n\r\n.nvd3.nv-pie path {\r\n    fill-opacity: .7;\r\n}\n\n.nvd3.nv-pie .hover path {\r\n    fill-opacity: 1;\r\n}\r\n\n.nvd3.nv-pie .nv-label {\n    pointer-events: none;\r\n}\r\n\n.nvd3.nv-pie .nv-label rect {\n    fill-opacity: 0;\r\n    stroke-opacity: 0;\r\n}\r\n\n/* scatter */\r\n.nvd3 .nv-groups .nv-point.hover {\r\n    stroke-width: 20px;\r\n    stroke-opacity: .5;\r\n}\r\n\r\n.nvd3 .nv-scatter .nv-point.hover {\r\n    fill-opacity: 1;\r\n}\n\n.nv-noninteractive {\r\n    pointer-events: none;\r\n}\r\n\r\n.nv-distx, .nv-disty {\r\n    pointer-events: none;\r\n}\r\n\n/* sparkline */\r\n.nvd3.nv-sparkline path {\r\n    fill: none;\r\n}\r\n\r\n.nvd3.nv-sparklineplus g.nv-hoverValue {\r\n    pointer-events: none;\r\n}\r\n\r\n.nvd3.nv-sparklineplus .nv-hoverValue line {\r\n    stroke: #333;\r\n    stroke-width: 1.5px;\r\n}\r\n\r\n.nvd3.nv-sparklineplus,\r\n.nvd3.nv-sparklineplus g {\r\n    pointer-events: all;\r\n}\r\n\r\n.nvd3 .nv-hoverArea {\r\n    fill-opacity: 0;\r\n    stroke-opacity: 0;\r\n}\r\n\r\n.nvd3.nv-sparklineplus .nv-xValue,\r\n.nvd3.nv-sparklineplus .nv-yValue {\r\n    stroke-width: 0;\r\n    font-size: .9em;\r\n    font-weight: normal;\r\n}\r\n\r\n.nvd3.nv-sparklineplus .nv-yValue {\r\n    stroke: #f66;\r\n}\r\n\r\n.nvd3.nv-sparklineplus .nv-maxValue {\r\n    stroke: #2ca02c;\r\n    fill: #2ca02c;\r\n}\r\n\r\n.nvd3.nv-sparklineplus .nv-minValue {\r\n    stroke: #d62728;\r\n    fill: #d62728;\r\n}\r\n\r\n.nvd3.nv-sparklineplus .nv-currentValue {\r\n    font-weight: bold;\r\n    font-size: 1.1em;\r\n}\n/* stacked area */\r\n.nvd3.nv-stackedarea path.nv-area {\r\n    fill-opacity: .7;\r\n    stroke-opacity: 0;\r\n    transition: fill-opacity 250ms linear, stroke-opacity 250ms linear;\r\n}\n\r\n.nvd3.nv-stackedarea path.nv-area.hover {\r\n    fill-opacity: .9;\r\n}\r\n\r\n\r\n.nvd3.nv-stackedarea .nv-groups .nv-point {\r\n    stroke-opacity: 0;\r\n    fill-opacity: 0;\r\n}\n\n.nvtooltip {\n    position: absolute;\r\n    background-color: rgba(255,255,255,1.0);\r\n    color: rgba(0,0,0,1.0);\r\n    padding: 1px;\r\n    border: 1px solid rgba(0,0,0,.2);\r\n    z-index: 10000;\r\n    display: block;\r\n\r\n    font-family: Arial, sans-serif;\r\n    font-size: 13px;\r\n    text-align: left;\r\n    pointer-events: none;\r\n\r\n    white-space: nowrap;\r\n\r\n    -webkit-user-select: none;\r\n\r\n       -moz-user-select: none;\r\n\r\n        -ms-user-select: none;\r\n\r\n            user-select: none;\n}\n\r\n.nvtooltip {\r\n    background: rgba(255,255,255, 0.8);\r\n    border: 1px solid rgba(0,0,0,0.5);\r\n    border-radius: 4px;\r\n}\r\n\r\n/*Give tooltips that old fade in transition by\r\n    putting a \"with-transitions\" class on the container div.\r\n*/\r\n.nvtooltip.with-transitions, .with-transitions .nvtooltip {\r\n    transition: opacity 50ms linear;\r\n\n    transition-delay: 200ms;\n}\n\r\n.nvtooltip.x-nvtooltip,\r\n.nvtooltip.y-nvtooltip {\r\n    padding: 8px;\r\n}\r\n\r\n.nvtooltip h3 {\r\n    margin: 0;\r\n    padding: 4px 14px;\r\n    line-height: 18px;\r\n    font-weight: normal;\r\n    background-color: rgba(247,247,247,0.75);\r\n    color: rgba(0,0,0,1.0);\r\n    text-align: center;\r\n\r\n    border-bottom: 1px solid #ebebeb;\r\n\r\n    border-radius: 5px 5px 0 0;\n}\r\n\r\n.nvtooltip p {\r\n    margin: 0;\r\n    padding: 5px 14px;\r\n    text-align: center;\r\n}\r\n\r\n.nvtooltip span {\r\n    display: inline-block;\r\n    margin: 2px 0;\r\n}\r\n\r\n.nvtooltip table {\r\n    margin: 6px;\r\n    border-spacing:0;\r\n}\r\n\r\n\r\n.nvtooltip table td {\r\n    padding: 2px 9px 2px 0;\r\n    vertical-align: middle;\r\n}\r\n\r\n.nvtooltip table td.key {\r\n    font-weight: normal;\r\n}\n\n.nvtooltip table td.key.total {\r\n    font-weight: bold;\r\n}\r\n\n.nvtooltip table td.value {\n    text-align: right;\r\n    font-weight: bold;\r\n}\r\n\r\n.nvtooltip table td.percent {\r\n    color: darkgray;\r\n}\r\n\r\n.nvtooltip table tr.highlight td {\r\n    padding: 1px 9px 1px 0;\r\n    border-bottom-style: solid;\r\n    border-bottom-width: 1px;\r\n    border-top-style: solid;\r\n    border-top-width: 1px;\r\n}\r\n\r\n.nvtooltip table td.legend-color-guide div {\r\n    width: 8px;\r\n    height: 8px;\r\n    vertical-align: middle;\r\n}\r\n\r\n.nvtooltip table td.legend-color-guide div {\r\n    width: 12px;\r\n    height: 12px;\r\n    border: 1px solid #999;\r\n}\r\n\r\n.nvtooltip .footer {\r\n    padding: 3px;\r\n    text-align: center;\r\n}\r\n\r\n.nvtooltip-pending-removal {\r\n    pointer-events: none;\r\n    display: none;\r\n}\r\n\r\n\r\n/****\r\nInteractive Layer\r\n*/\r\n.nvd3 .nv-interactiveGuideLine {\r\n    pointer-events:none;\r\n}\n\n.nvd3 line.nv-guideline {\r\n    stroke: #ccc;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../sweetalert2/dist/sweetalert2.js":
/***/ (function(module, exports, __webpack_require__) {

/*!
 * sweetalert2 v6.6.6
 * Released under the MIT License.
 */
(function (global, factory) {
	 true ? module.exports = factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(global.Sweetalert2 = factory());
}(this, (function () { 'use strict';

var defaultParams = {
  title: '',
  titleText: '',
  text: '',
  html: '',
  type: null,
  customClass: '',
  target: 'body',
  animation: true,
  allowOutsideClick: true,
  allowEscapeKey: true,
  allowEnterKey: true,
  showConfirmButton: true,
  showCancelButton: false,
  preConfirm: null,
  confirmButtonText: 'OK',
  confirmButtonColor: '#3085d6',
  confirmButtonClass: null,
  cancelButtonText: 'Cancel',
  cancelButtonColor: '#aaa',
  cancelButtonClass: null,
  buttonsStyling: true,
  reverseButtons: false,
  focusCancel: false,
  showCloseButton: false,
  showLoaderOnConfirm: false,
  imageUrl: null,
  imageWidth: null,
  imageHeight: null,
  imageClass: null,
  timer: null,
  width: 500,
  padding: 20,
  background: '#fff',
  input: null,
  inputPlaceholder: '',
  inputValue: '',
  inputOptions: {},
  inputAutoTrim: true,
  inputClass: null,
  inputAttributes: {},
  inputValidator: null,
  progressSteps: [],
  currentProgressStep: null,
  progressStepsDistance: '40px',
  onOpen: null,
  onClose: null,
  useRejections: true
};

var swalPrefix = 'swal2-';

var prefix = function prefix(items) {
  var result = {};
  for (var i in items) {
    result[items[i]] = swalPrefix + items[i];
  }
  return result;
};

var swalClasses = prefix(['container', 'shown', 'iosfix', 'modal', 'overlay', 'fade', 'show', 'hide', 'noanimation', 'close', 'title', 'content', 'buttonswrapper', 'confirm', 'cancel', 'icon', 'image', 'input', 'file', 'range', 'select', 'radio', 'checkbox', 'textarea', 'inputerror', 'validationerror', 'progresssteps', 'activeprogressstep', 'progresscircle', 'progressline', 'loading', 'styled']);

var iconTypes = prefix(['success', 'warning', 'info', 'question', 'error']);

/*
 * Set hover, active and focus-states for buttons (source: http://www.sitepoint.com/javascript-generate-lighter-darker-color)
 */
var colorLuminance = function colorLuminance(hex, lum) {
  // Validate hex string
  hex = String(hex).replace(/[^0-9a-f]/gi, '');
  if (hex.length < 6) {
    hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
  }
  lum = lum || 0;

  // Convert to decimal and change luminosity
  var rgb = '#';
  for (var i = 0; i < 3; i++) {
    var c = parseInt(hex.substr(i * 2, 2), 16);
    c = Math.round(Math.min(Math.max(0, c + c * lum), 255)).toString(16);
    rgb += ('00' + c).substr(c.length);
  }

  return rgb;
};

var uniqueArray = function uniqueArray(arr) {
  var result = [];
  for (var i in arr) {
    if (result.indexOf(arr[i]) === -1) {
      result.push(arr[i]);
    }
  }
  return result;
};

/* global MouseEvent */

// Remember state in cases where opening and handling a modal will fiddle with it.
var states = {
  previousWindowKeyDown: null,
  previousActiveElement: null,
  previousBodyPadding: null

  /*
   * Add modal + overlay to DOM
   */
};var init = function init(params) {
  if (typeof document === 'undefined') {
    console.error('SweetAlert2 requires document to initialize');
    return;
  }

  var container = document.createElement('div');
  container.className = swalClasses.container;
  container.innerHTML = sweetHTML;

  var targetElement = document.querySelector(params.target);
  if (!targetElement) {
    console.warn('SweetAlert2: Can\'t find the target "' + params.target + '"');
    targetElement = document.body;
  }
  targetElement.appendChild(container);

  var modal = getModal();
  var input = getChildByClass(modal, swalClasses.input);
  var file = getChildByClass(modal, swalClasses.file);
  var range = modal.querySelector('.' + swalClasses.range + ' input');
  var rangeOutput = modal.querySelector('.' + swalClasses.range + ' output');
  var select = getChildByClass(modal, swalClasses.select);
  var checkbox = modal.querySelector('.' + swalClasses.checkbox + ' input');
  var textarea = getChildByClass(modal, swalClasses.textarea);

  input.oninput = function () {
    sweetAlert.resetValidationError();
  };

  input.onkeydown = function (event) {
    setTimeout(function () {
      if (event.keyCode === 13 && params.allowEnterKey) {
        event.stopPropagation();
        sweetAlert.clickConfirm();
      }
    }, 0);
  };

  file.onchange = function () {
    sweetAlert.resetValidationError();
  };

  range.oninput = function () {
    sweetAlert.resetValidationError();
    rangeOutput.value = range.value;
  };

  range.onchange = function () {
    sweetAlert.resetValidationError();
    range.previousSibling.value = range.value;
  };

  select.onchange = function () {
    sweetAlert.resetValidationError();
  };

  checkbox.onchange = function () {
    sweetAlert.resetValidationError();
  };

  textarea.oninput = function () {
    sweetAlert.resetValidationError();
  };

  return modal;
};

/*
 * Manipulate DOM
 */

var sweetHTML = ('\n <div role="dialog" aria-labelledby="' + swalClasses.title + '" aria-describedby="' + swalClasses.content + '" class="' + swalClasses.modal + '" tabindex="-1">\n   <ul class="' + swalClasses.progresssteps + '"></ul>\n   <div class="' + swalClasses.icon + ' ' + iconTypes.error + '">\n     <span class="swal2-x-mark"><span class="swal2-x-mark-line-left"></span><span class="swal2-x-mark-line-right"></span></span>\n   </div>\n   <div class="' + swalClasses.icon + ' ' + iconTypes.question + '">?</div>\n   <div class="' + swalClasses.icon + ' ' + iconTypes.warning + '">!</div>\n   <div class="' + swalClasses.icon + ' ' + iconTypes.info + '">i</div>\n   <div class="' + swalClasses.icon + ' ' + iconTypes.success + '">\n     <div class="swal2-success-circular-line-left"></div>\n     <span class="swal2-success-line-tip"></span> <span class="swal2-success-line-long"></span>\n     <div class="swal2-success-ring"></div> <div class="swal2-success-fix"></div>\n     <div class="swal2-success-circular-line-right"></div>\n   </div>\n   <img class="' + swalClasses.image + '" />\n   <h2 class="' + swalClasses.title + '" id="' + swalClasses.title + '"></h2>\n   <div id="' + swalClasses.content + '" class="' + swalClasses.content + '"></div>\n   <input class="' + swalClasses.input + '" />\n   <input type="file" class="' + swalClasses.file + '" />\n   <div class="' + swalClasses.range + '">\n     <output></output>\n     <input type="range" />\n   </div>\n   <select class="' + swalClasses.select + '"></select>\n   <div class="' + swalClasses.radio + '"></div>\n   <label for="' + swalClasses.checkbox + '" class="' + swalClasses.checkbox + '">\n     <input type="checkbox" />\n   </label>\n   <textarea class="' + swalClasses.textarea + '"></textarea>\n   <div class="' + swalClasses.validationerror + '"></div>\n   <div class="' + swalClasses.buttonswrapper + '">\n     <button type="button" class="' + swalClasses.confirm + '">OK</button>\n     <button type="button" class="' + swalClasses.cancel + '">Cancel</button>\n   </div>\n   <button type="button" class="' + swalClasses.close + '" aria-label="Close this dialog">\xD7</button>\n </div>\n').replace(/(^|\n)\s*/g, '');

var getContainer = function getContainer() {
  return document.body.querySelector('.' + swalClasses.container);
};

var getModal = function getModal() {
  return getContainer() ? getContainer().querySelector('.' + swalClasses.modal) : null;
};

var getIcons = function getIcons() {
  var modal = getModal();
  return modal.querySelectorAll('.' + swalClasses.icon);
};

var elementByClass = function elementByClass(className) {
  return getContainer() ? getContainer().querySelector('.' + className) : null;
};

var getTitle = function getTitle() {
  return elementByClass(swalClasses.title);
};

var getContent = function getContent() {
  return elementByClass(swalClasses.content);
};

var getImage = function getImage() {
  return elementByClass(swalClasses.image);
};

var getButtonsWrapper = function getButtonsWrapper() {
  return elementByClass(swalClasses.buttonswrapper);
};

var getProgressSteps = function getProgressSteps() {
  return elementByClass(swalClasses.progresssteps);
};

var getValidationError = function getValidationError() {
  return elementByClass(swalClasses.validationerror);
};

var getConfirmButton = function getConfirmButton() {
  return elementByClass(swalClasses.confirm);
};

var getCancelButton = function getCancelButton() {
  return elementByClass(swalClasses.cancel);
};

var getCloseButton = function getCloseButton() {
  return elementByClass(swalClasses.close);
};

var getFocusableElements = function getFocusableElements(focusCancel) {
  var buttons = [getConfirmButton(), getCancelButton()];
  if (focusCancel) {
    buttons.reverse();
  }
  var focusableElements = buttons.concat(Array.prototype.slice.call(getModal().querySelectorAll('button, input:not([type=hidden]), textarea, select, a, *[tabindex]:not([tabindex="-1"])')));
  return uniqueArray(focusableElements);
};

var hasClass = function hasClass(elem, className) {
  if (elem.classList) {
    return elem.classList.contains(className);
  }
  return false;
};

var focusInput = function focusInput(input) {
  input.focus();

  // place cursor at end of text in text input
  if (input.type !== 'file') {
    // http://stackoverflow.com/a/2345915/1331425
    var val = input.value;
    input.value = '';
    input.value = val;
  }
};

var addClass = function addClass(elem, className) {
  if (!elem || !className) {
    return;
  }
  var classes = className.split(/\s+/).filter(Boolean);
  classes.forEach(function (className) {
    elem.classList.add(className);
  });
};

var removeClass = function removeClass(elem, className) {
  if (!elem || !className) {
    return;
  }
  var classes = className.split(/\s+/).filter(Boolean);
  classes.forEach(function (className) {
    elem.classList.remove(className);
  });
};

var getChildByClass = function getChildByClass(elem, className) {
  for (var i = 0; i < elem.childNodes.length; i++) {
    if (hasClass(elem.childNodes[i], className)) {
      return elem.childNodes[i];
    }
  }
};

var show = function show(elem, display) {
  if (!display) {
    display = 'block';
  }
  elem.style.opacity = '';
  elem.style.display = display;
};

var hide = function hide(elem) {
  elem.style.opacity = '';
  elem.style.display = 'none';
};

var empty = function empty(elem) {
  while (elem.firstChild) {
    elem.removeChild(elem.firstChild);
  }
};

// borrowed from jqeury $(elem).is(':visible') implementation
var isVisible = function isVisible(elem) {
  return elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length;
};

var removeStyleProperty = function removeStyleProperty(elem, property) {
  if (elem.style.removeProperty) {
    elem.style.removeProperty(property);
  } else {
    elem.style.removeAttribute(property);
  }
};

var fireClick = function fireClick(node) {
  if (!isVisible(node)) {
    return false;
  }

  // Taken from http://www.nonobtrusive.com/2011/11/29/programatically-fire-crossbrowser-click-event-with-javascript/
  // Then fixed for today's Chrome browser.
  if (typeof MouseEvent === 'function') {
    // Up-to-date approach
    var mevt = new MouseEvent('click', {
      view: window,
      bubbles: false,
      cancelable: true
    });
    node.dispatchEvent(mevt);
  } else if (document.createEvent) {
    // Fallback
    var evt = document.createEvent('MouseEvents');
    evt.initEvent('click', false, false);
    node.dispatchEvent(evt);
  } else if (document.createEventObject) {
    node.fireEvent('onclick');
  } else if (typeof node.onclick === 'function') {
    node.onclick();
  }
};

var animationEndEvent = function () {
  var testEl = document.createElement('div');
  var transEndEventNames = {
    'WebkitAnimation': 'webkitAnimationEnd',
    'OAnimation': 'oAnimationEnd oanimationend',
    'msAnimation': 'MSAnimationEnd',
    'animation': 'animationend'
  };
  for (var i in transEndEventNames) {
    if (transEndEventNames.hasOwnProperty(i) && testEl.style[i] !== undefined) {
      return transEndEventNames[i];
    }
  }

  return false;
}();

// Reset previous window keydown handler and focued element
var resetPrevState = function resetPrevState() {
  window.onkeydown = states.previousWindowKeyDown;
  if (states.previousActiveElement && states.previousActiveElement.focus) {
    var x = window.scrollX;
    var y = window.scrollY;
    states.previousActiveElement.focus();
    if (x && y) {
      // IE has no scrollX/scrollY support
      window.scrollTo(x, y);
    }
  }
};

// Measure width of scrollbar
// https://github.com/twbs/bootstrap/blob/master/js/modal.js#L279-L286
var measureScrollbar = function measureScrollbar() {
  var supportsTouch = 'ontouchstart' in window || navigator.msMaxTouchPoints;
  if (supportsTouch) {
    return 0;
  }
  var scrollDiv = document.createElement('div');
  scrollDiv.style.width = '50px';
  scrollDiv.style.height = '50px';
  scrollDiv.style.overflow = 'scroll';
  document.body.appendChild(scrollDiv);
  var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
  document.body.removeChild(scrollDiv);
  return scrollbarWidth;
};

// JavaScript Debounce Function
// Simplivied version of https://davidwalsh.name/javascript-debounce-function
var debounce = function debounce(func, wait) {
  var timeout = void 0;
  return function () {
    var later = function later() {
      timeout = null;
      func();
    };
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
  };
};

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
  return typeof obj;
} : function (obj) {
  return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
};





















var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var modalParams = _extends({}, defaultParams);
var queue = [];
var swal2Observer = void 0;

/*
 * Set type, text and actions on modal
 */
var setParameters = function setParameters(params) {
  var modal = getModal() || init(params);

  for (var param in params) {
    if (!defaultParams.hasOwnProperty(param) && param !== 'extraParams') {
      console.warn('SweetAlert2: Unknown parameter "' + param + '"');
    }
  }

  // Set modal width
  modal.style.width = typeof params.width === 'number' ? params.width + 'px' : params.width;

  modal.style.padding = params.padding + 'px';
  modal.style.background = params.background;
  var successIconParts = modal.querySelectorAll('[class^=swal2-success-circular-line], .swal2-success-fix');
  for (var i = 0; i < successIconParts.length; i++) {
    successIconParts[i].style.background = params.background;
  }

  var title = getTitle();
  var content = getContent();
  var buttonsWrapper = getButtonsWrapper();
  var confirmButton = getConfirmButton();
  var cancelButton = getCancelButton();
  var closeButton = getCloseButton();

  // Title
  if (params.titleText) {
    title.innerText = params.titleText;
  } else {
    title.innerHTML = params.title.split('\n').join('<br />');
  }

  // Content
  if (params.text || params.html) {
    if (_typeof(params.html) === 'object') {
      content.innerHTML = '';
      if (0 in params.html) {
        for (var _i = 0; _i in params.html; _i++) {
          content.appendChild(params.html[_i].cloneNode(true));
        }
      } else {
        content.appendChild(params.html.cloneNode(true));
      }
    } else if (params.html) {
      content.innerHTML = params.html;
    } else if (params.text) {
      content.textContent = params.text;
    }
    show(content);
  } else {
    hide(content);
  }

  // Close button
  if (params.showCloseButton) {
    show(closeButton);
  } else {
    hide(closeButton);
  }

  // Custom Class
  modal.className = swalClasses.modal;
  if (params.customClass) {
    addClass(modal, params.customClass);
  }

  // Progress steps
  var progressStepsContainer = getProgressSteps();
  var currentProgressStep = parseInt(params.currentProgressStep === null ? sweetAlert.getQueueStep() : params.currentProgressStep, 10);
  if (params.progressSteps.length) {
    show(progressStepsContainer);
    empty(progressStepsContainer);
    if (currentProgressStep >= params.progressSteps.length) {
      console.warn('SweetAlert2: Invalid currentProgressStep parameter, it should be less than progressSteps.length ' + '(currentProgressStep like JS arrays starts from 0)');
    }
    params.progressSteps.forEach(function (step, index) {
      var circle = document.createElement('li');
      addClass(circle, swalClasses.progresscircle);
      circle.innerHTML = step;
      if (index === currentProgressStep) {
        addClass(circle, swalClasses.activeprogressstep);
      }
      progressStepsContainer.appendChild(circle);
      if (index !== params.progressSteps.length - 1) {
        var line = document.createElement('li');
        addClass(line, swalClasses.progressline);
        line.style.width = params.progressStepsDistance;
        progressStepsContainer.appendChild(line);
      }
    });
  } else {
    hide(progressStepsContainer);
  }

  // Icon
  var icons = getIcons();
  for (var _i2 = 0; _i2 < icons.length; _i2++) {
    hide(icons[_i2]);
  }
  if (params.type) {
    var validType = false;
    for (var iconType in iconTypes) {
      if (params.type === iconType) {
        validType = true;
        break;
      }
    }
    if (!validType) {
      console.error('SweetAlert2: Unknown alert type: ' + params.type);
      return false;
    }
    var icon = modal.querySelector('.' + swalClasses.icon + '.' + iconTypes[params.type]);
    show(icon);

    // Animate icon
    if (params.animation) {
      switch (params.type) {
        case 'success':
          addClass(icon, 'swal2-animate-success-icon');
          addClass(icon.querySelector('.swal2-success-line-tip'), 'swal2-animate-success-line-tip');
          addClass(icon.querySelector('.swal2-success-line-long'), 'swal2-animate-success-line-long');
          break;
        case 'error':
          addClass(icon, 'swal2-animate-error-icon');
          addClass(icon.querySelector('.swal2-x-mark'), 'swal2-animate-x-mark');
          break;
        default:
          break;
      }
    }
  }

  // Custom image
  var image = getImage();
  if (params.imageUrl) {
    image.setAttribute('src', params.imageUrl);
    show(image);

    if (params.imageWidth) {
      image.setAttribute('width', params.imageWidth);
    } else {
      image.removeAttribute('width');
    }

    if (params.imageHeight) {
      image.setAttribute('height', params.imageHeight);
    } else {
      image.removeAttribute('height');
    }

    image.className = swalClasses.image;
    if (params.imageClass) {
      addClass(image, params.imageClass);
    }
  } else {
    hide(image);
  }

  // Cancel button
  if (params.showCancelButton) {
    cancelButton.style.display = 'inline-block';
  } else {
    hide(cancelButton);
  }

  // Confirm button
  if (params.showConfirmButton) {
    removeStyleProperty(confirmButton, 'display');
  } else {
    hide(confirmButton);
  }

  // Buttons wrapper
  if (!params.showConfirmButton && !params.showCancelButton) {
    hide(buttonsWrapper);
  } else {
    show(buttonsWrapper);
  }

  // Edit text on cancel and confirm buttons
  confirmButton.innerHTML = params.confirmButtonText;
  cancelButton.innerHTML = params.cancelButtonText;

  // Set buttons to selected background colors
  if (params.buttonsStyling) {
    confirmButton.style.backgroundColor = params.confirmButtonColor;
    cancelButton.style.backgroundColor = params.cancelButtonColor;
  }

  // Add buttons custom classes
  confirmButton.className = swalClasses.confirm;
  addClass(confirmButton, params.confirmButtonClass);
  cancelButton.className = swalClasses.cancel;
  addClass(cancelButton, params.cancelButtonClass);

  // Buttons styling
  if (params.buttonsStyling) {
    addClass(confirmButton, swalClasses.styled);
    addClass(cancelButton, swalClasses.styled);
  } else {
    removeClass(confirmButton, swalClasses.styled);
    removeClass(cancelButton, swalClasses.styled);

    confirmButton.style.backgroundColor = confirmButton.style.borderLeftColor = confirmButton.style.borderRightColor = '';
    cancelButton.style.backgroundColor = cancelButton.style.borderLeftColor = cancelButton.style.borderRightColor = '';
  }

  // CSS animation
  if (params.animation === true) {
    removeClass(modal, swalClasses.noanimation);
  } else {
    addClass(modal, swalClasses.noanimation);
  }
};

/*
 * Animations
 */
var openModal = function openModal(animation, onComplete) {
  var container = getContainer();
  var modal = getModal();

  if (animation) {
    addClass(modal, swalClasses.show);
    addClass(container, swalClasses.fade);
    removeClass(modal, swalClasses.hide);
  } else {
    removeClass(modal, swalClasses.fade);
  }
  show(modal);

  // scrolling is 'hidden' until animation is done, after that 'auto'
  container.style.overflowY = 'hidden';
  if (animationEndEvent && !hasClass(modal, swalClasses.noanimation)) {
    modal.addEventListener(animationEndEvent, function swalCloseEventFinished() {
      modal.removeEventListener(animationEndEvent, swalCloseEventFinished);
      container.style.overflowY = 'auto';
    });
  } else {
    container.style.overflowY = 'auto';
  }

  addClass(document.documentElement, swalClasses.shown);
  addClass(document.body, swalClasses.shown);
  addClass(container, swalClasses.shown);
  fixScrollbar();
  iOSfix();
  states.previousActiveElement = document.activeElement;
  if (onComplete !== null && typeof onComplete === 'function') {
    setTimeout(function () {
      onComplete(modal);
    });
  }
};

var fixScrollbar = function fixScrollbar() {
  // for queues, do not do this more than once
  if (states.previousBodyPadding !== null) {
    return;
  }
  // if the body has overflow
  if (document.body.scrollHeight > window.innerHeight) {
    // add padding so the content doesn't shift after removal of scrollbar
    states.previousBodyPadding = document.body.style.paddingRight;
    document.body.style.paddingRight = measureScrollbar() + 'px';
  }
};

var undoScrollbar = function undoScrollbar() {
  if (states.previousBodyPadding !== null) {
    document.body.style.paddingRight = states.previousBodyPadding;
    states.previousBodyPadding = null;
  }
};

// Fix iOS scrolling http://stackoverflow.com/q/39626302/1331425
var iOSfix = function iOSfix() {
  var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
  if (iOS && !hasClass(document.body, swalClasses.iosfix)) {
    var offset = document.body.scrollTop;
    document.body.style.top = offset * -1 + 'px';
    addClass(document.body, swalClasses.iosfix);
  }
};

var undoIOSfix = function undoIOSfix() {
  if (hasClass(document.body, swalClasses.iosfix)) {
    var offset = parseInt(document.body.style.top, 10);
    removeClass(document.body, swalClasses.iosfix);
    document.body.style.top = '';
    document.body.scrollTop = offset * -1;
  }
};

// SweetAlert entry point
var sweetAlert = function sweetAlert() {
  for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  if (args[0] === undefined) {
    console.error('SweetAlert2 expects at least 1 attribute!');
    return false;
  }

  var params = _extends({}, modalParams);

  switch (_typeof(args[0])) {
    case 'string':
      params.title = args[0];
      params.html = args[1];
      params.type = args[2];

      break;

    case 'object':
      _extends(params, args[0]);
      params.extraParams = args[0].extraParams;

      if (params.input === 'email' && params.inputValidator === null) {
        params.inputValidator = function (email) {
          return new Promise(function (resolve, reject) {
            var emailRegex = /^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
            if (emailRegex.test(email)) {
              resolve();
            } else {
              reject('Invalid email address');
            }
          });
        };
      }

      if (params.input === 'url' && params.inputValidator === null) {
        params.inputValidator = function (url) {
          return new Promise(function (resolve, reject) {
            // taken from https://stackoverflow.com/a/3809435/1331425
            var urlRegex = /^https?:\/\/(www\.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)$/;
            if (urlRegex.test(url)) {
              resolve();
            } else {
              reject('Invalid URL');
            }
          });
        };
      }
      break;

    default:
      console.error('SweetAlert2: Unexpected type of argument! Expected "string" or "object", got ' + _typeof(args[0]));
      return false;
  }

  setParameters(params);

  var container = getContainer();
  var modal = getModal();

  return new Promise(function (resolve, reject) {
    // Close on timer
    if (params.timer) {
      modal.timeout = setTimeout(function () {
        sweetAlert.closeModal(params.onClose);
        if (params.useRejections) {
          reject('timer');
        } else {
          resolve({ dismiss: 'timer' });
        }
      }, params.timer);
    }

    // Get input element by specified type or, if type isn't specified, by params.input
    var getInput = function getInput(inputType) {
      inputType = inputType || params.input;
      if (!inputType) {
        return null;
      }
      switch (inputType) {
        case 'select':
        case 'textarea':
        case 'file':
          return getChildByClass(modal, swalClasses[inputType]);
        case 'checkbox':
          return modal.querySelector('.' + swalClasses.checkbox + ' input');
        case 'radio':
          return modal.querySelector('.' + swalClasses.radio + ' input:checked') || modal.querySelector('.' + swalClasses.radio + ' input:first-child');
        case 'range':
          return modal.querySelector('.' + swalClasses.range + ' input');
        default:
          return getChildByClass(modal, swalClasses.input);
      }
    };

    // Get the value of the modal input
    var getInputValue = function getInputValue() {
      var input = getInput();
      if (!input) {
        return null;
      }
      switch (params.input) {
        case 'checkbox':
          return input.checked ? 1 : 0;
        case 'radio':
          return input.checked ? input.value : null;
        case 'file':
          return input.files.length ? input.files[0] : null;
        default:
          return params.inputAutoTrim ? input.value.trim() : input.value;
      }
    };

    // input autofocus
    if (params.input) {
      setTimeout(function () {
        var input = getInput();
        if (input) {
          focusInput(input);
        }
      }, 0);
    }

    var confirm = function confirm(value) {
      if (params.showLoaderOnConfirm) {
        sweetAlert.showLoading();
      }

      if (params.preConfirm) {
        params.preConfirm(value, params.extraParams).then(function (preConfirmValue) {
          sweetAlert.closeModal(params.onClose);
          resolve(preConfirmValue || value);
        }, function (error) {
          sweetAlert.hideLoading();
          if (error) {
            sweetAlert.showValidationError(error);
          }
        });
      } else {
        sweetAlert.closeModal(params.onClose);
        if (params.useRejections) {
          resolve(value);
        } else {
          resolve({ value: value });
        }
      }
    };

    // Mouse interactions
    var onButtonEvent = function onButtonEvent(event) {
      var e = event || window.event;
      var target = e.target || e.srcElement;
      var confirmButton = getConfirmButton();
      var cancelButton = getCancelButton();
      var targetedConfirm = confirmButton && (confirmButton === target || confirmButton.contains(target));
      var targetedCancel = cancelButton && (cancelButton === target || cancelButton.contains(target));

      switch (e.type) {
        case 'mouseover':
        case 'mouseup':
          if (params.buttonsStyling) {
            if (targetedConfirm) {
              confirmButton.style.backgroundColor = colorLuminance(params.confirmButtonColor, -0.1);
            } else if (targetedCancel) {
              cancelButton.style.backgroundColor = colorLuminance(params.cancelButtonColor, -0.1);
            }
          }
          break;
        case 'mouseout':
          if (params.buttonsStyling) {
            if (targetedConfirm) {
              confirmButton.style.backgroundColor = params.confirmButtonColor;
            } else if (targetedCancel) {
              cancelButton.style.backgroundColor = params.cancelButtonColor;
            }
          }
          break;
        case 'mousedown':
          if (params.buttonsStyling) {
            if (targetedConfirm) {
              confirmButton.style.backgroundColor = colorLuminance(params.confirmButtonColor, -0.2);
            } else if (targetedCancel) {
              cancelButton.style.backgroundColor = colorLuminance(params.cancelButtonColor, -0.2);
            }
          }
          break;
        case 'click':
          // Clicked 'confirm'
          if (targetedConfirm && sweetAlert.isVisible()) {
            sweetAlert.disableButtons();
            if (params.input) {
              var inputValue = getInputValue();

              if (params.inputValidator) {
                sweetAlert.disableInput();
                params.inputValidator(inputValue, params.extraParams).then(function () {
                  sweetAlert.enableButtons();
                  sweetAlert.enableInput();
                  confirm(inputValue);
                }, function (error) {
                  sweetAlert.enableButtons();
                  sweetAlert.enableInput();
                  if (error) {
                    sweetAlert.showValidationError(error);
                  }
                });
              } else {
                confirm(inputValue);
              }
            } else {
              confirm(true);
            }

            // Clicked 'cancel'
          } else if (targetedCancel && sweetAlert.isVisible()) {
            sweetAlert.disableButtons();
            sweetAlert.closeModal(params.onClose);
            if (params.useRejections) {
              reject('cancel');
            } else {
              resolve({ dismiss: 'cancel' });
            }
          }
          break;
        default:
      }
    };

    var buttons = modal.querySelectorAll('button');
    for (var i = 0; i < buttons.length; i++) {
      buttons[i].onclick = onButtonEvent;
      buttons[i].onmouseover = onButtonEvent;
      buttons[i].onmouseout = onButtonEvent;
      buttons[i].onmousedown = onButtonEvent;
    }

    // Closing modal by close button
    getCloseButton().onclick = function () {
      sweetAlert.closeModal(params.onClose);
      if (params.useRejections) {
        reject('close');
      } else {
        resolve({ dismiss: 'close' });
      }
    };

    // Closing modal by overlay click
    container.onclick = function (e) {
      if (e.target !== container) {
        return;
      }
      if (params.allowOutsideClick) {
        sweetAlert.closeModal(params.onClose);
        if (params.useRejections) {
          reject('overlay');
        } else {
          resolve({ dismiss: 'overlay' });
        }
      }
    };

    var buttonsWrapper = getButtonsWrapper();
    var confirmButton = getConfirmButton();
    var cancelButton = getCancelButton();

    // Reverse buttons (Confirm on the right side)
    if (params.reverseButtons) {
      confirmButton.parentNode.insertBefore(cancelButton, confirmButton);
    } else {
      confirmButton.parentNode.insertBefore(confirmButton, cancelButton);
    }

    // Focus handling
    var setFocus = function setFocus(index, increment) {
      var focusableElements = getFocusableElements(params.focusCancel);
      // search for visible elements and select the next possible match
      for (var _i3 = 0; _i3 < focusableElements.length; _i3++) {
        index = index + increment;

        // rollover to first item
        if (index === focusableElements.length) {
          index = 0;

          // go to last item
        } else if (index === -1) {
          index = focusableElements.length - 1;
        }

        // determine if element is visible
        var el = focusableElements[index];
        if (isVisible(el)) {
          return el.focus();
        }
      }
    };

    var handleKeyDown = function handleKeyDown(event) {
      var e = event || window.event;
      var keyCode = e.keyCode || e.which;

      if ([9, 13, 32, 27, 37, 38, 39, 40].indexOf(keyCode) === -1) {
        // Don't do work on keys we don't care about.
        return;
      }

      var targetElement = e.target || e.srcElement;

      var focusableElements = getFocusableElements(params.focusCancel);
      var btnIndex = -1; // Find the button - note, this is a nodelist, not an array.
      for (var _i4 = 0; _i4 < focusableElements.length; _i4++) {
        if (targetElement === focusableElements[_i4]) {
          btnIndex = _i4;
          break;
        }
      }

      // TAB
      if (keyCode === 9) {
        if (!e.shiftKey) {
          // Cycle to the next button
          setFocus(btnIndex, 1);
        } else {
          // Cycle to the prev button
          setFocus(btnIndex, -1);
        }
        e.stopPropagation();
        e.preventDefault();

        // ARROWS - switch focus between buttons
      } else if (keyCode === 37 || keyCode === 38 || keyCode === 39 || keyCode === 40) {
        // focus Cancel button if Confirm button is currently focused
        if (document.activeElement === confirmButton && isVisible(cancelButton)) {
          cancelButton.focus();
          // and vice versa
        } else if (document.activeElement === cancelButton && isVisible(confirmButton)) {
          confirmButton.focus();
        }

        // ENTER/SPACE
      } else if (keyCode === 13 || keyCode === 32) {
        if (btnIndex === -1 && params.allowEnterKey) {
          // ENTER/SPACE clicked outside of a button.
          if (params.focusCancel) {
            fireClick(cancelButton, e);
          } else {
            fireClick(confirmButton, e);
          }
          e.stopPropagation();
          e.preventDefault();
        }

        // ESC
      } else if (keyCode === 27 && params.allowEscapeKey === true) {
        sweetAlert.closeModal(params.onClose);
        if (params.useRejections) {
          reject('esc');
        } else {
          resolve({ dismiss: 'esc' });
        }
      }
    };

    if (!window.onkeydown || window.onkeydown.toString() !== handleKeyDown.toString()) {
      states.previousWindowKeyDown = window.onkeydown;
      window.onkeydown = handleKeyDown;
    }

    // Loading state
    if (params.buttonsStyling) {
      confirmButton.style.borderLeftColor = params.confirmButtonColor;
      confirmButton.style.borderRightColor = params.confirmButtonColor;
    }

    /**
     * Show spinner instead of Confirm button and disable Cancel button
     */
    sweetAlert.hideLoading = sweetAlert.disableLoading = function () {
      if (!params.showConfirmButton) {
        hide(confirmButton);
        if (!params.showCancelButton) {
          hide(getButtonsWrapper());
        }
      }
      removeClass(buttonsWrapper, swalClasses.loading);
      removeClass(modal, swalClasses.loading);
      confirmButton.disabled = false;
      cancelButton.disabled = false;
    };

    sweetAlert.getTitle = function () {
      return getTitle();
    };
    sweetAlert.getContent = function () {
      return getContent();
    };
    sweetAlert.getInput = function () {
      return getInput();
    };
    sweetAlert.getImage = function () {
      return getImage();
    };
    sweetAlert.getButtonsWrapper = function () {
      return getButtonsWrapper();
    };
    sweetAlert.getConfirmButton = function () {
      return getConfirmButton();
    };
    sweetAlert.getCancelButton = function () {
      return getCancelButton();
    };

    sweetAlert.enableButtons = function () {
      confirmButton.disabled = false;
      cancelButton.disabled = false;
    };

    sweetAlert.disableButtons = function () {
      confirmButton.disabled = true;
      cancelButton.disabled = true;
    };

    sweetAlert.enableConfirmButton = function () {
      confirmButton.disabled = false;
    };

    sweetAlert.disableConfirmButton = function () {
      confirmButton.disabled = true;
    };

    sweetAlert.enableInput = function () {
      var input = getInput();
      if (!input) {
        return false;
      }
      if (input.type === 'radio') {
        var radiosContainer = input.parentNode.parentNode;
        var radios = radiosContainer.querySelectorAll('input');
        for (var _i5 = 0; _i5 < radios.length; _i5++) {
          radios[_i5].disabled = false;
        }
      } else {
        input.disabled = false;
      }
    };

    sweetAlert.disableInput = function () {
      var input = getInput();
      if (!input) {
        return false;
      }
      if (input && input.type === 'radio') {
        var radiosContainer = input.parentNode.parentNode;
        var radios = radiosContainer.querySelectorAll('input');
        for (var _i6 = 0; _i6 < radios.length; _i6++) {
          radios[_i6].disabled = true;
        }
      } else {
        input.disabled = true;
      }
    };

    // Set modal min-height to disable scrolling inside the modal
    sweetAlert.recalculateHeight = debounce(function () {
      var modal = getModal();
      if (!modal) {
        return;
      }
      var prevState = modal.style.display;
      modal.style.minHeight = '';
      show(modal);
      modal.style.minHeight = modal.scrollHeight + 1 + 'px';
      modal.style.display = prevState;
    }, 50);

    // Show block with validation error
    sweetAlert.showValidationError = function (error) {
      var validationError = getValidationError();
      validationError.innerHTML = error;
      show(validationError);

      var input = getInput();
      if (input) {
        focusInput(input);
        addClass(input, swalClasses.inputerror);
      }
    };

    // Hide block with validation error
    sweetAlert.resetValidationError = function () {
      var validationError = getValidationError();
      hide(validationError);
      sweetAlert.recalculateHeight();

      var input = getInput();
      if (input) {
        removeClass(input, swalClasses.inputerror);
      }
    };

    sweetAlert.getProgressSteps = function () {
      return params.progressSteps;
    };

    sweetAlert.setProgressSteps = function (progressSteps) {
      params.progressSteps = progressSteps;
      setParameters(params);
    };

    sweetAlert.showProgressSteps = function () {
      show(getProgressSteps());
    };

    sweetAlert.hideProgressSteps = function () {
      hide(getProgressSteps());
    };

    sweetAlert.enableButtons();
    sweetAlert.hideLoading();
    sweetAlert.resetValidationError();

    // inputs
    var inputTypes = ['input', 'file', 'range', 'select', 'radio', 'checkbox', 'textarea'];
    var input = void 0;
    for (var _i7 = 0; _i7 < inputTypes.length; _i7++) {
      var inputClass = swalClasses[inputTypes[_i7]];
      var inputContainer = getChildByClass(modal, inputClass);
      input = getInput(inputTypes[_i7]);

      // set attributes
      if (input) {
        for (var j in input.attributes) {
          if (input.attributes.hasOwnProperty(j)) {
            var attrName = input.attributes[j].name;
            if (attrName !== 'type' && attrName !== 'value') {
              input.removeAttribute(attrName);
            }
          }
        }
        for (var attr in params.inputAttributes) {
          input.setAttribute(attr, params.inputAttributes[attr]);
        }
      }

      // set class
      inputContainer.className = inputClass;
      if (params.inputClass) {
        addClass(inputContainer, params.inputClass);
      }

      hide(inputContainer);
    }

    var populateInputOptions = void 0;
    switch (params.input) {
      case 'text':
      case 'email':
      case 'password':
      case 'number':
      case 'tel':
      case 'url':
        input = getChildByClass(modal, swalClasses.input);
        input.value = params.inputValue;
        input.placeholder = params.inputPlaceholder;
        input.type = params.input;
        show(input);
        break;
      case 'file':
        input = getChildByClass(modal, swalClasses.file);
        input.placeholder = params.inputPlaceholder;
        input.type = params.input;
        show(input);
        break;
      case 'range':
        var range = getChildByClass(modal, swalClasses.range);
        var rangeInput = range.querySelector('input');
        var rangeOutput = range.querySelector('output');
        rangeInput.value = params.inputValue;
        rangeInput.type = params.input;
        rangeOutput.value = params.inputValue;
        show(range);
        break;
      case 'select':
        var select = getChildByClass(modal, swalClasses.select);
        select.innerHTML = '';
        if (params.inputPlaceholder) {
          var placeholder = document.createElement('option');
          placeholder.innerHTML = params.inputPlaceholder;
          placeholder.value = '';
          placeholder.disabled = true;
          placeholder.selected = true;
          select.appendChild(placeholder);
        }
        populateInputOptions = function populateInputOptions(inputOptions) {
          for (var optionValue in inputOptions) {
            var option = document.createElement('option');
            option.value = optionValue;
            option.innerHTML = inputOptions[optionValue];
            if (params.inputValue === optionValue) {
              option.selected = true;
            }
            select.appendChild(option);
          }
          show(select);
          select.focus();
        };
        break;
      case 'radio':
        var radio = getChildByClass(modal, swalClasses.radio);
        radio.innerHTML = '';
        populateInputOptions = function populateInputOptions(inputOptions) {
          for (var radioValue in inputOptions) {
            var radioInput = document.createElement('input');
            var radioLabel = document.createElement('label');
            var radioLabelSpan = document.createElement('span');
            radioInput.type = 'radio';
            radioInput.name = swalClasses.radio;
            radioInput.value = radioValue;
            if (params.inputValue === radioValue) {
              radioInput.checked = true;
            }
            radioLabelSpan.innerHTML = inputOptions[radioValue];
            radioLabel.appendChild(radioInput);
            radioLabel.appendChild(radioLabelSpan);
            radioLabel.for = radioInput.id;
            radio.appendChild(radioLabel);
          }
          show(radio);
          var radios = radio.querySelectorAll('input');
          if (radios.length) {
            radios[0].focus();
          }
        };
        break;
      case 'checkbox':
        var checkbox = getChildByClass(modal, swalClasses.checkbox);
        var checkboxInput = getInput('checkbox');
        checkboxInput.type = 'checkbox';
        checkboxInput.value = 1;
        checkboxInput.id = swalClasses.checkbox;
        checkboxInput.checked = Boolean(params.inputValue);
        var label = checkbox.getElementsByTagName('span');
        if (label.length) {
          checkbox.removeChild(label[0]);
        }
        label = document.createElement('span');
        label.innerHTML = params.inputPlaceholder;
        checkbox.appendChild(label);
        show(checkbox);
        break;
      case 'textarea':
        var textarea = getChildByClass(modal, swalClasses.textarea);
        textarea.value = params.inputValue;
        textarea.placeholder = params.inputPlaceholder;
        show(textarea);
        break;
      case null:
        break;
      default:
        console.error('SweetAlert2: Unexpected type of input! Expected "text", "email", "password", "number", "tel", "select", "radio", "checkbox", "textarea", "file" or "url", got "' + params.input + '"');
        break;
    }

    if (params.input === 'select' || params.input === 'radio') {
      if (params.inputOptions instanceof Promise) {
        sweetAlert.showLoading();
        params.inputOptions.then(function (inputOptions) {
          sweetAlert.hideLoading();
          populateInputOptions(inputOptions);
        });
      } else if (_typeof(params.inputOptions) === 'object') {
        populateInputOptions(params.inputOptions);
      } else {
        console.error('SweetAlert2: Unexpected type of inputOptions! Expected object or Promise, got ' + _typeof(params.inputOptions));
      }
    }

    openModal(params.animation, params.onOpen);

    // Focus the first element (input or button)
    if (params.allowEnterKey) {
      setFocus(-1, 1);
    } else {
      if (document.activeElement) {
        document.activeElement.blur();
      }
    }

    // fix scroll
    getContainer().scrollTop = 0;

    // Observe changes inside the modal and adjust height
    if (typeof MutationObserver !== 'undefined' && !swal2Observer) {
      swal2Observer = new MutationObserver(sweetAlert.recalculateHeight);
      swal2Observer.observe(modal, { childList: true, characterData: true, subtree: true });
    }
  });
};

/*
 * Global function to determine if swal2 modal is shown
 */
sweetAlert.isVisible = function () {
  return !!getModal();
};

/*
 * Global function for chaining sweetAlert modals
 */
sweetAlert.queue = function (steps) {
  queue = steps;
  var resetQueue = function resetQueue() {
    queue = [];
    document.body.removeAttribute('data-swal2-queue-step');
  };
  var queueResult = [];
  return new Promise(function (resolve, reject) {
    (function step(i, callback) {
      if (i < queue.length) {
        document.body.setAttribute('data-swal2-queue-step', i);

        sweetAlert(queue[i]).then(function (result) {
          queueResult.push(result);
          step(i + 1, callback);
        }, function (dismiss) {
          resetQueue();
          reject(dismiss);
        });
      } else {
        resetQueue();
        resolve(queueResult);
      }
    })(0);
  });
};

/*
 * Global function for getting the index of current modal in queue
 */
sweetAlert.getQueueStep = function () {
  return document.body.getAttribute('data-swal2-queue-step');
};

/*
 * Global function for inserting a modal to the queue
 */
sweetAlert.insertQueueStep = function (step, index) {
  if (index && index < queue.length) {
    return queue.splice(index, 0, step);
  }
  return queue.push(step);
};

/*
 * Global function for deleting a modal from the queue
 */
sweetAlert.deleteQueueStep = function (index) {
  if (typeof queue[index] !== 'undefined') {
    queue.splice(index, 1);
  }
};

/*
 * Global function to close sweetAlert
 */
sweetAlert.close = sweetAlert.closeModal = function (onComplete) {
  var container = getContainer();
  var modal = getModal();
  if (!modal) {
    return;
  }
  removeClass(modal, swalClasses.show);
  addClass(modal, swalClasses.hide);
  clearTimeout(modal.timeout);

  resetPrevState();

  var removeModalAndResetState = function removeModalAndResetState() {
    if (container.parentNode) {
      container.parentNode.removeChild(container);
    }
    removeClass(document.documentElement, swalClasses.shown);
    removeClass(document.body, swalClasses.shown);
    undoScrollbar();
    undoIOSfix();
  };

  // If animation is supported, animate
  if (animationEndEvent && !hasClass(modal, swalClasses.noanimation)) {
    modal.addEventListener(animationEndEvent, function swalCloseEventFinished() {
      modal.removeEventListener(animationEndEvent, swalCloseEventFinished);
      if (hasClass(modal, swalClasses.hide)) {
        removeModalAndResetState();
      }
    });
  } else {
    // Otherwise, remove immediately
    removeModalAndResetState();
  }
  if (onComplete !== null && typeof onComplete === 'function') {
    setTimeout(function () {
      onComplete(modal);
    });
  }
};

/*
 * Global function to click 'Confirm' button
 */
sweetAlert.clickConfirm = function () {
  return getConfirmButton().click();
};

/*
 * Global function to click 'Cancel' button
 */
sweetAlert.clickCancel = function () {
  return getCancelButton().click();
};

/**
 * Show spinner instead of Confirm button and disable Cancel button
 */
sweetAlert.showLoading = sweetAlert.enableLoading = function () {
  var modal = getModal();
  if (!modal) {
    sweetAlert('');
  }
  var buttonsWrapper = getButtonsWrapper();
  var confirmButton = getConfirmButton();
  var cancelButton = getCancelButton();

  show(buttonsWrapper);
  show(confirmButton, 'inline-block');
  addClass(buttonsWrapper, swalClasses.loading);
  addClass(modal, swalClasses.loading);
  confirmButton.disabled = true;
  cancelButton.disabled = true;
};

/**
 * Set default params for each popup
 * @param {Object} userParams
 */
sweetAlert.setDefaults = function (userParams) {
  if (!userParams || (typeof userParams === 'undefined' ? 'undefined' : _typeof(userParams)) !== 'object') {
    return console.error('SweetAlert2: the argument for setDefaults() is required and has to be a object');
  }

  for (var param in userParams) {
    if (!defaultParams.hasOwnProperty(param) && param !== 'extraParams') {
      console.warn('SweetAlert2: Unknown parameter "' + param + '"');
      delete userParams[param];
    }
  }

  _extends(modalParams, userParams);
};

/**
 * Reset default params for each popup
 */
sweetAlert.resetDefaults = function () {
  modalParams = _extends({}, defaultParams);
};

sweetAlert.noop = function () {};

sweetAlert.version = '6.6.6';

sweetAlert.default = sweetAlert;

return sweetAlert;

})));
if (window.Sweetalert2) window.sweetAlert = window.swal = window.Sweetalert2;


/***/ })

});
//# sourceMappingURL=common.chunk.js.map