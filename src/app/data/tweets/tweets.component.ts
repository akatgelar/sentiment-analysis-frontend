import {Component, OnInit, ViewEncapsulation} from '@angular/core';

declare var $: any;
declare var Morris: any;
import 'd3';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import { HttpClient } from '@angular/common/http';
import swal from 'sweetalert2';
import * as moment from 'moment'

@Component({
  moduleId: module.id,
  selector: 'app-tweets',
  templateUrl: './tweets.component.html',
  styleUrls: [
    '../../../../node_modules/c3/c3.min.css',
    '../../../assets/icon/SVG-animated/svg-weather.css',
    '../../../assets/css/chartist.css',
    '../../../../node_modules/nvd3/build/nv.d3.css'
  ],
  encapsulation: ViewEncapsulation.None,
  animations: [fadeInOutTranslate]
})


export class TweetsComponent implements OnInit {
  private discreteBarOptions: any;
  private discreteBarData: any;
  private messageError: String = '';
  private _apiURLGetTotalTweet = '/api/getTotalTweet';
  private _dataGetTotalTweet: DataResult;
  private _dataLoaded: boolean;

  constructor(private httpClient: HttpClient) {
  }


  ngOnInit() {

    this.getTotalTweet();

  }

  getTotalTweet() {
    try {
      this.httpClient.get<DataResult>(this._apiURLGetTotalTweet)
      .subscribe(result => {

        this._dataGetTotalTweet = result as DataResult;

        // this._dataGetTotalTweet.data.first_tweet = formattedDate(this._dataGetTotalTweet.data.first_tweet);
        // this._dataGetTotalTweet.data.last_tweet = formattedDate(this._dataGetTotalTweet.data.last_tweet);
        console.log(this.formattedDate(this._dataGetTotalTweet.data.first_tweet));
        console.log(this.formattedDate(this._dataGetTotalTweet.data.last_tweet));


        this.discreteBarOptions = {
          chart: {
              type: 'discreteBarChart',
              height: 400,
              margin : {
                top: 50,
                right: 50,
                bottom: 50,
                left: 100
              },
              x: function (d) {
                  return d.label;
              },
              y: function (d) {
                  return d.value;
              },
              xAxis: {
                showMaxMin: false,
                tickPadding: 15
              },
              yAxis: {
                tickFormat: function(d){
                    return d3.format(',.0f')(d);
                },
                tickPadding: 10
              },
              valueFormat : d3.format(',.0f'),
              staggerLabels: false,
              showValues: true,
              tooltips : true
          }
        };
        this.discreteBarData = [{
            key: 'Cumulative Return',
            values: [{
                'label': '#' + this._dataGetTotalTweet.data.topics[0].search_value,
                'value': this._dataGetTotalTweet.data.topics[0].count_all,
                'color': '#00A14D'
            }, {
                'label': '#' + this._dataGetTotalTweet.data.topics[1].search_value,
                'value': this._dataGetTotalTweet.data.topics[1].count_all,
                'color': '#212467'
            }, {
                'label': this._dataGetTotalTweet.data.topics[2].search_value,
                'value': this._dataGetTotalTweet.data.topics[2].count_all,
                'color': '#DA201B'
            }, {
                'label': this._dataGetTotalTweet.data.topics[3].search_value,
                'value': this._dataGetTotalTweet.data.topics[3].count_all,
                'color': '#E3B402'
            }]
        }];

        this._dataLoaded = true;
      },
      error => {
        console.log(error);
      });

    } catch (error) {
      swal(
          'Infomation!',
          'Can\'t connect to server.',
          'error'
      );
    }

  }

  formattedDate(date) {
    moment.locale('id');
    return moment(date).format('dddd DD MMMM YYYY HH:mm:ss')
  }

  numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }


}



export interface DataTopic {
  search_by: string;
  search_value: string;
  count_all: number;
  percentage: number;
}

export interface DataTweet {
  total_tweet: number;
  first_tweet: Date;
  last_tweet: Date;
  range: number;
  tweet_per_day: string;
  topics: Array<DataTopic>;
}

export interface DataResult {
  code: number;
  success: boolean;
  message: string;
  data: DataTweet;
}
