import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {SampleTweetsComponent} from './sample-tweets.component';
import {SampleTweetsRoutes} from './sample-tweets.routing';
import {SharedModule} from '../../shared/shared.module';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(SampleTweetsRoutes),
        SharedModule,
        HttpClientModule
    ],
    declarations: [SampleTweetsComponent]
})

export class SampleTweetsModule {}
