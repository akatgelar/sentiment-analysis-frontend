import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {ProgressComponent} from './progress.component';
import {ProgressRoutes} from './progress.routing';
import {SharedModule} from '../../shared/shared.module';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ProgressRoutes),
        SharedModule,
        HttpClientModule
    ],
    declarations: [ProgressComponent]
})

export class ProgressModule {}
