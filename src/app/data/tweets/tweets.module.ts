import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {TweetsComponent} from './tweets.component';
import {TweetsRoutes} from './tweets.routing';
import {SharedModule} from '../../shared/shared.module';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(TweetsRoutes),
        SharedModule,
        HttpClientModule
    ],
    declarations: [TweetsComponent],
    providers: [
    ]
})

export class TweetsModule {}
