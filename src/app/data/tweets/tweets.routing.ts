import { Routes } from '@angular/router';
import { TweetsComponent } from './tweets.component';

export const TweetsRoutes: Routes = [{
    path: '',
    component: TweetsComponent,
    data: {
        breadcrumb: 'Data Tweets'
    }
}];
