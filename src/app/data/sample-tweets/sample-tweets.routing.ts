import { Routes } from '@angular/router';
import { SampleTweetsComponent } from "./sample-tweets.component";

export const SampleTweetsRoutes: Routes = [{
    path: '',
    component: SampleTweetsComponent,
    data: {
        breadcrumb: "Sample Tweets"
    }
}];
