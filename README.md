# MashAble

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.2.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).





#install package 
npm install


#semantic-ui: During npm install ask to install semantic-ui, following below steps

1. Yes, Extend my current setting
2. Automatic
3. Yes
4. set path as node_modules

 
#start service 
ng serve --port 4300 --proxy-config proxy.config.json 
 

note:
#if service not start (error)

# error:Metadata version mismatch for module  /node_modules/@ng-bootstrap/ng-bootstrap/index.d.ts, found version 4, expected 3
# resolved: check @ng-bootstrap/ng-bootstrap version in  
# /node_modules/@ng-bootstrap/ng-bootstrap/package.json shall be "version": "1.0.0-beta.6"
# if different version found, delete /node_modules/@ng-bootstrap/ng-bootstrap/
run npm install --save-dev @ng-bootstrap/ng-bootstrap@1.0.0-beta.6  

# error: Metadata version mismatch for module /node_modules/css-animator/index.d.ts, found version 4, expected 3
# resolved: check  /node_modules/css-animator/ackage.json shall be  "version": "2.2.1"
# if different version found, delete /node_modules/css-animator/
run npm install --save-dev css-animator@2.2.1

# error
# npm ERR! Error extracting C:\Users\USERNAME\AppData\Roaming\npm-cache\ngx-chips\1.6.2\package.tgz archive: ENOENT: no such file or # directory, open 'C:\Users\USERNAME\AppData\Roaming\npm-cache\ngx-chips\1.6.2\package.tgz'
# check version exist in C:\Users\USERNAME\AppData\Roaming\npm-cache\ngx-chips\ 
# Modified package.json for project according to version in C:\Users\USERNAME\AppData\Roaming\npm-cache\ngx-chips\ 
# example: "ngx-chips": "^1.6.3" (line 67) 

===========================================================
or before run npm istall edited  original package.json :
"@ng-bootstrap/ng-bootstrap": "1.0.0-beta.6", (line 25) 
"css-animator": "2.2.1", (line 46)
"ngx-chips": "^1.6.3", (line 67) 