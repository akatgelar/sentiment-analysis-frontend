import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login.component';
import {SharedModule} from '../../shared/shared.module';

export const LoginRoutes: Routes = [
  {
    path: '',
    component: LoginComponent,
    data: {
      breadcrumb: 'Login'
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(LoginRoutes),
    SharedModule
  ],
  declarations: [LoginComponent]
})
export class LoginModule { }
