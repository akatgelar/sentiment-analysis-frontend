webpackJsonp(["sentiment.module"],{

/***/ "../../../../../src/app/data/sentiment/sentiment.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\" style=\"justify-content: center;\">\r\n\r\n  <!-- counter-card-1 start-->   \r\n  <div class=\"col-md-12 col-xl-4\">\r\n    <div class=\"card social-widget-card\">\r\n      <div class=\"card-block-big bg-success\">\r\n        <i class=\"icofont icofont-emo-simple-smile\"  style=\"left: 40px;font-size: 72px;top: 40px;\"></i>\r\n        <h1 style=\"text-align:right\" *ngIf=\"_dataLoaded\">{{_dataGetTotalSentiment.data.percentage_positif}}%</h1>\r\n        <h6 style=\"text-align:right\" *ngIf=\"_dataLoaded\">{{numberWithCommas(_dataGetTotalSentiment.data.sentiment_positif)}} tweet</h6>\r\n        <h6 style=\"text-align:right\" class=\"m-t-10\">Sentimen Positif</h6>\r\n      </div> \r\n    </div> \r\n  </div>   \r\n  <div class=\"col-md-12 col-xl-4\">\r\n    <div class=\"card social-widget-card\">\r\n      <div class=\"card-block-big bg-warning\">\r\n        <i class=\"icofont icofont-emo-expressionless\"  style=\"left: 40px;font-size: 72px;top: 40px;\"></i>\r\n        <h1 style=\"text-align:right\" *ngIf=\"_dataLoaded\">{{_dataGetTotalSentiment.data.percentage_netral}}%</h1>\r\n        <h6 style=\"text-align:right\" *ngIf=\"_dataLoaded\">{{numberWithCommas(_dataGetTotalSentiment.data.sentiment_netral)}} tweet</h6>\r\n        <h6 style=\"text-align:right\" class=\"m-t-10\">Sentimen Netral</h6> \r\n      </div>\r\n    </div>\r\n  </div>  \r\n  <div class=\"col-md-12 col-xl-4\">\r\n    <div class=\"card social-widget-card\">\r\n      <div class=\"card-block-big bg-danger\">\r\n        <i class=\"icofont icofont-emo-worried\"  style=\"left: 40px;font-size: 72px;top: 40px;\"></i>\r\n        <h1 style=\"text-align:right\" *ngIf=\"_dataLoaded\">{{_dataGetTotalSentiment.data.percentage_negatif}}%</h1>\r\n        <h6 style=\"text-align:right\" *ngIf=\"_dataLoaded\">{{numberWithCommas(_dataGetTotalSentiment.data.sentiment_negatif)}} tweet</h6>\r\n        <h6 style=\"text-align:right\" class=\"m-t-10\">Sentimen Negatif</h6> \r\n      </div>\r\n    </div>  \r\n  </div>  \r\n  <!-- counter-card-1 end-->\r\n </div>\r\n\r\n <div class=\"row\">\r\n\r\n  <!-- DISCRETE BAR CHART start -->\r\n  <div class=\"col-md-12 col-lg-12\">\r\n    <app-card [title]=\"'Sentiment'\" [headerContent]=\"'Perbandingan Sentiment Masing Masing #hashtag atau keyword'\"> \r\n      <div class=\"row\"> \r\n        \r\n        <div class=\"col-md-12 col-lg-1\" style=\"text-align:center\"></div>\r\n        <div class=\"col-md-12 col-lg-5\" style=\"text-align:center\">\r\n          <h3>#2019GantiPresiden</h3> \r\n          <google-chart [data]=\"donutChartData1\"></google-chart>\r\n        </div>  \r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <br/><br/><br/><br/><br/><br/>\r\n          <label class=\"badge badge-lg bg-success\"><i class=\"icofont icofont-emo-simple-smile\"></i></label>  \r\n          <h5 *ngIf=\"_dataLoaded\">&nbsp;&nbsp;Sentimen Positif {{_dataGetTotalSentiment.data.topics[0].percentage_positif}} % ({{numberWithCommas(_dataGetTotalSentiment.data.topics[0].sentiment_positif)}} tweet) </h5>\r\n          <br>\r\n          <label class=\"badge badge-lg bg-warning\"><i class=\"icofont icofont-emo-expressionless\"></i></label>  \r\n          <h5 *ngIf=\"_dataLoaded\">&nbsp;&nbsp;Sentimen Netral {{_dataGetTotalSentiment.data.topics[0].percentage_netral}} % ({{numberWithCommas(_dataGetTotalSentiment.data.topics[0].sentiment_netral)}} tweet) </h5>\r\n          <br>\r\n          <label class=\"badge badge-lg bg-danger\"><i class=\"icofont icofont-emo-worried\"></i></label>  \r\n          <h5 *ngIf=\"_dataLoaded\">&nbsp;&nbsp;Sentimen Negatif {{_dataGetTotalSentiment.data.topics[0].percentage_negatif}} % ({{numberWithCommas(_dataGetTotalSentiment.data.topics[0].sentiment_negatif)}} tweet) </h5>\r\n        </div> \r\n        <div class=\"col-md-12 col-lg-12\">\r\n          <hr/>\r\n          <br/>\r\n          <br/>\r\n        </div>\r\n        \r\n\r\n        <div class=\"col-md-12 col-lg-1\" style=\"text-align:center\"></div>\r\n        <div class=\"col-md-12 col-lg-5\" style=\"text-align:center\"> \r\n          <h3>#2019TetapJokowi</h3> \r\n          <google-chart [data]=\"donutChartData2\"></google-chart>\r\n        </div>   \r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <br/><br/><br/><br/><br/><br/>\r\n          <label class=\"badge badge-lg bg-success\"><i class=\"icofont icofont-emo-simple-smile\"></i></label>  \r\n          <h5 *ngIf=\"_dataLoaded\">&nbsp;&nbsp;Sentimen Positif {{_dataGetTotalSentiment.data.topics[1].percentage_positif}} % ({{numberWithCommas(_dataGetTotalSentiment.data.topics[1].sentiment_positif)}} tweet) </h5>\r\n          <br>\r\n          <label class=\"badge badge-lg bg-warning\"><i class=\"icofont icofont-emo-expressionless\"></i></label>  \r\n          <h5 *ngIf=\"_dataLoaded\">&nbsp;&nbsp;Sentimen Netral {{_dataGetTotalSentiment.data.topics[1].percentage_netral}} % ({{numberWithCommas(_dataGetTotalSentiment.data.topics[1].sentiment_netral)}} tweet) </h5>\r\n          <br>\r\n          <label class=\"badge badge-lg bg-danger\"><i class=\"icofont icofont-emo-worried\"></i></label>  \r\n          <h5 *ngIf=\"_dataLoaded\">&nbsp;&nbsp;Sentimen Negatif {{_dataGetTotalSentiment.data.topics[1].percentage_negatif}} % ({{numberWithCommas(_dataGetTotalSentiment.data.topics[1].sentiment_negatif)}} tweet) </h5>\r\n        </div> \r\n        <div class=\"col-md-12 col-lg-12\">\r\n          <hr/>\r\n          <br/>\r\n          <br/>\r\n        </div>\r\n\r\n\r\n        <div class=\"col-md-12 col-lg-1\" style=\"text-align:center\"></div>\r\n        <div class=\"col-md-12 col-lg-5\" style=\"text-align:center\"> \r\n          <h3>Jokowi</h3> \r\n          <google-chart [data]=\"donutChartData3\"></google-chart>\r\n        </div>   \r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <br/><br/><br/><br/><br/><br/>\r\n          <label class=\"badge badge-lg bg-success\"><i class=\"icofont icofont-emo-simple-smile\"></i></label>  \r\n          <h5 *ngIf=\"_dataLoaded\">&nbsp;&nbsp;Sentimen Positif {{_dataGetTotalSentiment.data.topics[2].percentage_positif}} % ({{numberWithCommas(_dataGetTotalSentiment.data.topics[2].sentiment_positif)}} tweet) </h5>\r\n          <br>\r\n          <label class=\"badge badge-lg bg-warning\"><i class=\"icofont icofont-emo-expressionless\"></i></label>  \r\n          <h5 *ngIf=\"_dataLoaded\">&nbsp;&nbsp;Sentimen Netral {{_dataGetTotalSentiment.data.topics[2].percentage_netral}} % ({{numberWithCommas(_dataGetTotalSentiment.data.topics[2].sentiment_netral)}} tweet) </h5>\r\n          <br>\r\n          <label class=\"badge badge-lg bg-danger\"><i class=\"icofont icofont-emo-worried\"></i></label>  \r\n          <h5 *ngIf=\"_dataLoaded\">&nbsp;&nbsp;Sentimen Negatif {{_dataGetTotalSentiment.data.topics[2].percentage_negatif}} % ({{numberWithCommas(_dataGetTotalSentiment.data.topics[2].sentiment_negatif)}} tweet) </h5>\r\n        </div> \r\n        <div class=\"col-md-12 col-lg-12\">\r\n          <hr/>\r\n          <br/>\r\n          <br/>\r\n        </div>\r\n\r\n\r\n        <div class=\"col-md-12 col-lg-1\" style=\"text-align:center\"></div>\r\n        <div class=\"col-md-12 col-lg-5\" style=\"text-align:center\"> \r\n          <h3>Prabowo</h3> \r\n          <google-chart [data]=\"donutChartData4\"></google-chart>\r\n        </div>   \r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <br/><br/><br/><br/><br/><br/>\r\n          <label class=\"badge badge-lg bg-success\"><i class=\"icofont icofont-emo-simple-smile\"></i></label>  \r\n          <h5 *ngIf=\"_dataLoaded\">&nbsp;&nbsp;Sentimen Positif {{_dataGetTotalSentiment.data.topics[3].percentage_positif}} % ({{numberWithCommas(_dataGetTotalSentiment.data.topics[3].sentiment_positif)}} tweet) </h5>\r\n          <br>\r\n          <label class=\"badge badge-lg bg-warning\"><i class=\"icofont icofont-emo-expressionless\"></i></label>  \r\n          <h5 *ngIf=\"_dataLoaded\">&nbsp;&nbsp;Sentimen Netral {{_dataGetTotalSentiment.data.topics[3].percentage_netral}} % ({{numberWithCommas(_dataGetTotalSentiment.data.topics[3].sentiment_netral)}} tweet) </h5>\r\n          <br>\r\n          <label class=\"badge badge-lg bg-danger\"><i class=\"icofont icofont-emo-worried\"></i></label>  \r\n          <h5 *ngIf=\"_dataLoaded\">&nbsp;&nbsp;Sentimen Negatif {{_dataGetTotalSentiment.data.topics[3].percentage_negatif}} % ({{numberWithCommas(_dataGetTotalSentiment.data.topics[3].sentiment_negatif)}} tweet) </h5>\r\n        </div> \r\n        <div class=\"col-md-12 col-lg-12\">\r\n          <hr/>\r\n          <br/>\r\n          <br/>\r\n        </div>\r\n\r\n\r\n      </div>\r\n    </app-card>\r\n  </div>\r\n  <!-- DISCRETE BAR CHART Ends -->\r\n\r\n\r\n\r\n    \r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/data/sentiment/sentiment.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SentimentComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_d3__ = __webpack_require__("../../../../d3/d3.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_d3___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_d3__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_elements_animation__ = __webpack_require__("../../../../../src/app/shared/elements/animation.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_sweetalert2__ = __webpack_require__("../../../../sweetalert2/dist/sweetalert2.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_sweetalert2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_sweetalert2__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SentimentComponent = (function () {
    function SentimentComponent(httpClient) {
        this.httpClient = httpClient;
        this._apiURLGetTotalSentiment = '/api/getTotalSentiment';
    }
    SentimentComponent.prototype.ngOnInit = function () {
        this.getTotalSentiment();
    };
    SentimentComponent.prototype.getTotalSentiment = function () {
        var _this = this;
        try {
            this.httpClient.get(this._apiURLGetTotalSentiment)
                .subscribe(function (result) {
                _this._dataGetTotalSentiment = result;
                console.log(_this._dataGetTotalSentiment.data.sentiment_negatif);
                _this.donutChartData1 = {
                    chartType: 'PieChart',
                    dataTable: [
                        ['Keterangan', 'Jumlah Persentase', { role: 'annotation' }],
                        ['Positif', _this._dataGetTotalSentiment.data.topics[0].sentiment_positif,
                            _this.numberWithCommas(_this._dataGetTotalSentiment.data.topics[0].sentiment_positif)],
                        ['Netral', _this._dataGetTotalSentiment.data.topics[0].sentiment_netral,
                            _this.numberWithCommas(_this._dataGetTotalSentiment.data.topics[0].sentiment_netral)],
                        ['Negatif', _this._dataGetTotalSentiment.data.topics[0].sentiment_negatif,
                            _this.numberWithCommas(_this._dataGetTotalSentiment.data.topics[0].sentiment_negatif)],
                    ],
                    options: {
                        height: 300,
                        title: '',
                        pieHole: 0.3,
                        colors: ['#2ecc71', '#f1c40f', '#e74c3c'],
                        chartArea: {
                            height: 250,
                            width: 250,
                            left: 50,
                            right: 50,
                        },
                        legend: {
                            position: 'none',
                            maxLines: 1,
                            textStyle: {
                                fontSize: 25
                            }
                        },
                    },
                };
                _this.donutChartData2 = {
                    chartType: 'PieChart',
                    dataTable: [
                        ['Keterangan', 'Jumlah Persentase', { role: 'annotation' }],
                        ['Positif', _this._dataGetTotalSentiment.data.topics[1].sentiment_positif,
                            _this.numberWithCommas(_this._dataGetTotalSentiment.data.topics[1].sentiment_positif)],
                        ['Netral', _this._dataGetTotalSentiment.data.topics[1].sentiment_netral,
                            _this.numberWithCommas(_this._dataGetTotalSentiment.data.topics[1].sentiment_netral)],
                        ['Negatif', _this._dataGetTotalSentiment.data.topics[1].sentiment_negatif,
                            _this.numberWithCommas(_this._dataGetTotalSentiment.data.topics[1].sentiment_negatif)],
                    ],
                    options: {
                        height: 300,
                        title: '',
                        pieHole: 0.3,
                        colors: ['#2ecc71', '#f1c40f', '#e74c3c'],
                        chartArea: {
                            height: 250,
                            width: 250,
                            left: 50,
                            right: 50,
                        },
                        legend: {
                            position: 'none',
                            maxLines: 1,
                            textStyle: {
                                fontSize: 25
                            }
                        },
                    },
                };
                _this.donutChartData3 = {
                    chartType: 'PieChart',
                    dataTable: [
                        ['Keterangan', 'Jumlah Persentase', { role: 'annotation' }],
                        ['Positif', _this._dataGetTotalSentiment.data.topics[2].sentiment_positif,
                            _this.numberWithCommas(_this._dataGetTotalSentiment.data.topics[2].sentiment_positif)],
                        ['Netral', _this._dataGetTotalSentiment.data.topics[2].sentiment_netral,
                            _this.numberWithCommas(_this._dataGetTotalSentiment.data.topics[2].sentiment_netral)],
                        ['Negatif', _this._dataGetTotalSentiment.data.topics[2].sentiment_negatif,
                            _this.numberWithCommas(_this._dataGetTotalSentiment.data.topics[2].sentiment_negatif)],
                    ],
                    options: {
                        height: 300,
                        title: '',
                        pieHole: 0.3,
                        colors: ['#2ecc71', '#f1c40f', '#e74c3c'],
                        chartArea: {
                            height: 250,
                            width: 250,
                            left: 50,
                            right: 50,
                        },
                        legend: {
                            position: 'none',
                            maxLines: 1,
                            textStyle: {
                                fontSize: 25
                            }
                        },
                    },
                };
                _this.donutChartData4 = {
                    chartType: 'PieChart',
                    dataTable: [
                        ['Keterangan', 'Jumlah Persentase', { role: 'annotation' }],
                        ['Positif', _this._dataGetTotalSentiment.data.topics[3].sentiment_positif,
                            _this.numberWithCommas(_this._dataGetTotalSentiment.data.topics[3].sentiment_positif)],
                        ['Netral', _this._dataGetTotalSentiment.data.topics[3].sentiment_netral,
                            _this.numberWithCommas(_this._dataGetTotalSentiment.data.topics[3].sentiment_netral)],
                        ['Negatif', _this._dataGetTotalSentiment.data.topics[3].sentiment_negatif,
                            _this.numberWithCommas(_this._dataGetTotalSentiment.data.topics[3].sentiment_negatif)],
                    ],
                    options: {
                        height: 300,
                        title: '',
                        pieHole: 0.3,
                        colors: ['#2ecc71', '#f1c40f', '#e74c3c'],
                        chartArea: {
                            height: 250,
                            width: 250,
                            left: 50,
                            right: 50,
                        },
                        legend: {
                            position: 'none',
                            maxLines: 1,
                            textStyle: {
                                fontSize: 25
                            }
                        },
                    },
                };
                _this._dataLoaded = true;
            }, function (error) {
                console.log(error);
            });
        }
        catch (error) {
            __WEBPACK_IMPORTED_MODULE_4_sweetalert2___default()('Infomation!', 'Can\'t connect to server.', 'error');
        }
    };
    SentimentComponent.prototype.numberWithCommas = function (x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
    };
    return SentimentComponent;
}());
SentimentComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-sentiment',
        template: __webpack_require__("../../../../../src/app/data/sentiment/sentiment.component.html"),
        styles: [__webpack_require__("../../../../c3/c3.min.css"), __webpack_require__("../../../../../src/assets/icon/SVG-animated/svg-weather.css"), __webpack_require__("../../../../../src/assets/css/chartist.css"), __webpack_require__("../../../../nvd3/build/nv.d3.css")],
        encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewEncapsulation"].None,
        animations: [__WEBPACK_IMPORTED_MODULE_2__shared_elements_animation__["a" /* fadeInOutTranslate */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */]) === "function" && _a || Object])
], SentimentComponent);

var _a;
//# sourceMappingURL=sentiment.component.js.map

/***/ }),

/***/ "../../../../../src/app/data/sentiment/sentiment.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SentimentModule", function() { return SentimentModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__sentiment_component__ = __webpack_require__("../../../../../src/app/data/sentiment/sentiment.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__sentiment_routing__ = __webpack_require__("../../../../../src/app/data/sentiment/sentiment.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var SentimentModule = (function () {
    function SentimentModule() {
    }
    return SentimentModule;
}());
SentimentModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["g" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_4__sentiment_routing__["a" /* SentimentRoutes */]),
            __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__["a" /* SharedModule */],
            __WEBPACK_IMPORTED_MODULE_6__angular_common_http__["b" /* HttpClientModule */]
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_3__sentiment_component__["a" /* SentimentComponent */]]
    })
], SentimentModule);

//# sourceMappingURL=sentiment.module.js.map

/***/ }),

/***/ "../../../../../src/app/data/sentiment/sentiment.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SentimentRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__sentiment_component__ = __webpack_require__("../../../../../src/app/data/sentiment/sentiment.component.ts");

var SentimentRoutes = [{
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__sentiment_component__["a" /* SentimentComponent */],
        data: {
            breadcrumb: "Sentiment"
        }
    }];
//# sourceMappingURL=sentiment.routing.js.map

/***/ })

});
//# sourceMappingURL=sentiment.module.chunk.js.map