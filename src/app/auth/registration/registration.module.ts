import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {RegistrationComponent} from './registration.component';
import {SharedModule} from '../../shared/shared.module';

export const RegistrationRoutes: Routes = [
  {
    path: '',
    component: RegistrationComponent,
    data: {
      breadcrumb: 'Login'
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(RegistrationRoutes),
    SharedModule
  ],
  declarations: [RegistrationComponent]
})
export class RegistrationModule { }
